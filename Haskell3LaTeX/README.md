Description
===========

Haskell3LaTeX (which is a fork of Haskell2LaTeX: see below) is a pretty-printer for
Literate Haskell. Literate Haskell files (with .lhs extension) are Haskell source files
which are meant to be typeset. Canonically, Literate Haskell files are typeset using
LaTeX.

While a separate tool for typesetting is strictly not necessary (one can just print
code using the verbatim package), nice formatting and indentation and syntax
highlighting and whatnot are very nice features, which Haskell3LaTeX does superbly.

History
=======

Haskell3LaTeX is a fork of Haskell2LaTeX. I emailed Ian Lynagh (the author of
Haskell2LaTeX) and didn't get a response. There were some limitations in
Haskell2LaTeX 0.3.0 (the last public release as of the time of this writing) that I
needed to get around, so I'm forking it and providing it here.

I want to simultaneously promote Haskell2LaTeX and still not sully the good work that
Ian has done. I'm calling this fork Haskell3LaTeX. If you want the actual
Haskell2LaTeX (the stuff Ian did), it is available at the following URL:
http://www.comlab.ox.ac.uk/people/ian.lynagh/Haskell2LaTeX/

Specifically, the issues I wanted to address were:

* Some problems with the automaton that didn't work with my code. I had to write a
few new rules, etc.
* The build system was very Linux-specific
* Update it for modern versions of ghc
* It didn't work with Literate Happy (the parser generator grammar files)

Building
========

Just run `make`

It requires ghc
