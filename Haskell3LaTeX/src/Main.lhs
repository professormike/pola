
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Main where

> import System.Environment
> import System.IO
> import Haskell2LaTeX.Either
> import Haskell2LaTeX.Args
> import Common.Tokens
> import Haskell2LaTeX.Positionise
> import Haskell2LaTeX.Scanner
> import Haskell2LaTeX.Offside
> import Haskell2LaTeX.Mangle
> import Haskell2LaTeX.Parser
> import Haskell2LaTeX.PPTeX
> import Haskell2LaTeX.Tree

> name,progname,bugs,desc,version :: String
> name      = "Haskell3LaTeX"
> progname  = "haskell3latex"
> bugs      = "Report bugs to <mburrel@uwo.ca>"
> desc      = "prettyprints a Haskell script in LaTeX using Haskell2LaTeX.sty."
> version   = "0.1"

> main :: IO()
> main = do raw_args <- getArgs
>           let (args, input_file) = parse_commandline raw_args
>           if want_help args then show_help
>                             else if want_version args
>                                  then show_version
>                                  else pp args input_file

> show_help :: IO()
> show_help = do putStrLn ("Usage: " ++ progname ++ " [OPTION]... [FILE]...")
>                putStrLn (name ++ " " ++ desc)
>                putStr usage_info
>                putStrLn ""
>                putStrLn bugs

> show_version :: IO()
> show_version = putStrLn (name ++ " " ++ version)

> pp :: Args -> FilePath -> IO()
> pp args input_file = do raw_input <- if input_file == "-"
>                                      then getContents
>                                      else readFile input_file
>                         let input = positionise (do_unlit args) raw_input
>                             output_file = get_output_file args
>                             output = if output_file == "-"
>                                      then putStrLn
>                                      else writeFile output_file
>                         output =<< (choose_output (outputs args input) args)

> choose_output :: [((Args -> Bool), String, [String])] -> Args -> IO String
> choose_output [] _ = error "doit: empty list - this can't happen!"
> choose_output ((f, s, es):fss) args = if f args
>                                       then do mapM_ (hPutStrLn stderr) es
>                                               return s
>                                       else choose_output fss args

> outputs :: Args
>         -> Input [(Char, Position)]
>         -> [((Args -> Bool), String, [String])]
> outputs args input =
>  [(stop_after_unlit,      show input,                 []),
>   (stop_after_lex,        pp_tokens' lexer_ts,        lexer_errors),
>   (stop_after_indentmark, pp_tokens' indentmark_ts,   indentmark_errors'),
>   (stop_after_offside,    pp_tokens' offsideified_ts, offsideified_errors'),
>   (stop_after_fixity,     pp_tokens' mangled_ts,      mangled_errors'),
>   (stop_after_parseable,  pp_tokens' parseable_ts,    mangled_errors'),
>   (stop_after_parse,      pp_tree' tree,              parsed_errors')]
>  where pp_tokens' = if print_list_text args
>                     then pp_tokens_comments
>                     else show
>        pp_tree' = if print_tree_latex args
>                   then pp_treelets_comments
>                   else if print_tree_text args
>                        then error "print_tree_text XXX" -- XXX
>                        else show
>        (lexer_ts, lexer_errors) = scan input
>        (indentmark_ts, indentmark_errors) = indentmarkify lexer_ts
>        indentmark_errors' = lexer_errors ++ indentmark_errors
>        (offsideified_ts, offsideified_errors) = offsideify indentmark_ts
>        offsideified_errors' = indentmark_errors' ++ offsideified_errors
>        topdecls_fixities = get_topdecls_fixities offsideified_ts
>        (mangled_ts, mangled_errors)
>         = do_rights (mangle topdecls_fixities) offsideified_ts
>        mangled_errors' = offsideified_errors' ++ mangled_errors
>        parseable_ts = fst (do_rights (Right . filter (is_parseable_token . fst)) mangled_ts)
>        (parsed, parsed_errors) = parse parseable_ts
>        parsed_errors' = mangled_errors' ++ parsed_errors
>        tree = flat parsed

> flat :: Input [a] -> [Either String a]
> flat [] = []
> flat (Left l:xs) = Left l:flat xs
> flat (Right (_, rs, _):xs) = map Right rs ++ flat xs

> pp_tokens_comments :: Input [(Token, Position)] -> String
> pp_tokens_comments [] = ""
> pp_tokens_comments (Left s:es) = s ++ pp_tokens_comments es
> pp_tokens_comments (Right (_, cps, _):es) = (pp_tokens . map fst) cps
>                                          ++ pp_tokens_comments es

> pp_treelets_comments :: [Either String Parsed] -> String
> pp_treelets_comments [] = ""
> pp_treelets_comments (Left s:es) = s ++ pp_treelets_comments es
> pp_treelets_comments (Right r:es) = this ++ pp_treelets_comments es
>  where this = case r of
>                   ParsedHead h -> pp_ModuleHead h
>                   ParsedImpD i -> pp_ModuleImpdecls i
>                   ParsedTopD t -> pp_ModuleTopdecls t
>                   ParsedTail t -> pp_ModuleTail t

