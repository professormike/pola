
> module Common.Positionise (Position, end_of_file, get_pos_char,
>                     furthest_pos, positionise, show_pos) where

> import Common.Position

> positionise :: String -> [(Char, Position)]
> positionise inp = concat (map add_pos inp_lines)
>  where inp_lines = zip (lineify inp) [1..]

> add_pos :: (String, Offset) -> [(Char, Position)]
> add_pos (xs, line) = zipWith (\x c -> (x, Position line c)) xs [1..]

> lineify :: String -> [String]
> lineify "" = []
> lineify xs = ys:lineify zs
>  where (ys, zs) = takeDropUntil ((==) '\n') xs

> takeDropUntil :: (a -> Bool) -> [a] -> ([a], [a])
> takeDropUntil _ [] = ([], [])
> takeDropUntil p (x:xs) = if p x then ([x], xs)
>                                 else (x:ys, zs)
>  where (ys, zs) = takeDropUntil p xs

