
> module Common.Position (Position(..), Offset, Line, Char, end_of_file, get_pos_char,
>                  furthest_pos, show_pos) where

> import Data.Char

> type Offset = Int
> type Line = Offset
> type Character = Offset
> data Position = Position !Line !Character
>               | End_Of_File
>  deriving (Show, Eq)

> data Classified = Comment String Line Bool -- Bool True = comment is white
>                 | BirdTracks String [(Char, Position)] Position
>                 | CodeBlock String [(Char, Position)] Position
>   deriving Show

> get_pos_char :: Position -> Character
> get_pos_char (Position _ c) = c
> get_pos_char _ = 1 -- Wrong!

> end_of_file :: Position
> end_of_file = End_Of_File

> instance Ord Position where
>   (Position l1 c1) <= (Position l2 c2) = (l1, c1) <= (l2, c2)
>   _ <= End_Of_File = True
>   End_Of_File <= _ = False

> furthest_pos :: Position -> Position -> Position
> furthest_pos = max

> show_pos :: Position -> String
> show_pos (Position line char) = "line " ++ show line
>                              ++ ", character " ++ show char
> show_pos End_Of_File = "end of file"

\end{document}

