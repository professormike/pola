
> module Common.Utils (wrap, cons_2_2) where

> wrap :: a -> [a]
> wrap x = [x]

> cons_2_2 :: b -> (a, [b]) -> (a, [b])
> cons_2_2 y (x, ys) = (x, y:ys)

