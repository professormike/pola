
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Common.PCbase (Parser, pSucceed, pFail, (Common.PCbase.<*>), (<|>), (<|), (<!>),
>                pPred, pList0, keep_pos,
>                parse_succeeded, consumed_length, ret_val,
>                rest_of_input, fail_pos, pNotAcceptsEmpty)
>   where

Take a string and return a b, the number of characters consumed and the
rest of the input

> import Common.Position

> type SParsed a b = (b, Int, [(a, Position)], Position)
> data Parsed a b = Success (SParsed a b)
>                 | Failure Position
>   deriving Show -- XXX temporary
> type SParser a b = [(a, Position)] -> SParsed a b
> type Parser a b = [(a, Position)] -> Parsed a b

> parse_succeeded :: Parsed a b -> Bool
> parse_succeeded (Success _) = True
> parse_succeeded (Failure _) = False
> consumed_length :: Parsed a b -> Int
> consumed_length (Success (_, len, _, _)) = len
> consumed_length (Failure _)
>   = error "Tried to take consumed_length of a failed parse"
> ret_val :: Parsed a b -> b
> ret_val (Success (ret, _, _, _)) = ret
> ret_val (Failure _)
>   = error "Tried to take ret_val of a failed parse"
> rest_of_input :: Parsed a b -> [(a, Position)]
> rest_of_input (Success (_, _, rest, _)) = rest
> rest_of_input (Failure _)
>   = error "Tried to take rest_of_input of a failed parse"
> fail_pos :: Parsed a b -> Position
> fail_pos (Failure p) = p
> fail_pos (Success (_, _, _, p)) = p

> infixl 7 <*>
> infixl 2 <!>
> infixl 3 <|>
> infixl 3 <|

> get_pos :: [(a, Position)] -> Position
> get_pos [] = end_of_file
> get_pos ((_, p):_) = p

> keep_pos :: Parser a b -> Parser a (b, Position)
> keep_pos p
>  = \inp -> case p inp of
>                Failure pos -> Failure pos
>                Success (v, n, inp', pos')
>                    -> Success ((v, get_pos inp), n, inp', pos')

> pFail :: Parser a b
> pFail = \inp -> Failure (get_pos inp)

> pList0 :: Parser a b -> Parser a [b]
> pList0 p = \inp -> Success (pListS p inp)
> pListS :: Parser a b -> SParser a [b]
> pListS p = \inp -> case p inp of
>                        Success (v1, n1, inp1, pos1) ->
>                            fp `seq` (v1:v2, n1 + n2, inp2, fp)
>                            where (v2, n2, inp2, pos2) = pListS p inp1
>                                  fp = furthest_pos pos1 pos2
>                        Failure pos -> ([], 0, inp, pos)

> pSucceed :: b -> Parser a b
> pSucceed v = \inp -> Success (v, 0, inp, get_pos inp)

> pNotAcceptsEmpty :: Parser a b -> c -> Parser a c
> pNotAcceptsEmpty p v = \inp -> case p inp of
>                                    Failure _ -> pSucceed v inp
>                                    _ -> Failure (get_pos inp)

> (<*>) :: Parser a (b -> c) -> Parser a b -> Parser a c
> p <*> q
>  = \inp -> case p inp of
>                Success (v1, n1, inp1, pos1) ->
>                    case q inp1 of
>                        Success (v2, n2, inp2, pos2) ->
>                            fp `seq` Success (v1 v2, n1 + n2, inp2, fp)
>                                where fp = furthest_pos pos1 pos2
>                        Failure pos2 ->
>                            fp `seq` Failure fp
>                                where fp = furthest_pos pos1 pos2
>                Failure pos -> Failure pos

> (<|>) :: Parser a b -> Parser a b -> Parser a b
> p <|> q
>  = \inp -> case (p inp, q inp) of
>                (Success (v1, n1, inp1, pos1), Success (v2, n2, inp2, pos2))
>                    -> case n1 `compare` n2 of
>                           GT -> succeed v1 n1 inp1 pos1 pos2
>                           LT -> succeed v2 n2 inp2 pos1 pos2
>                           EQ -> error ("Equal parses at " ++ show_pos (get_pos inp))
>                (Success (v1, n1, inp1, pos1), Failure pos2)
>                    -> succeed v1 n1 inp1 pos1 pos2
>                (Failure pos1, Success (v2, n2, inp2, pos2))
>                    -> succeed v2 n2 inp2 pos1 pos2
>                (Failure pos1, Failure pos2) -> fp `seq` Failure fp
>                    where fp = furthest_pos pos1 pos2
>  where succeed v n inp pos1 pos2 = fp `seq` Success (v, n, inp, fp)
>            where fp = furthest_pos pos1 pos2

> (<|) :: Parser a b -> Parser a b -> Parser a b
> p <| q = \inp -> case p inp of
>                      Failure pos1 ->
>                          case q inp of
>                              Success (v2, n2, inp2, pos2) ->
>                                  fp `seq` Success (v2, n2, inp2, fp)
>                                where fp = furthest_pos pos1 pos2
>                              Failure pos2 ->
>                                  fp `seq` Failure fp
>                                where fp = furthest_pos pos1 pos2
>                      success -> success

p <!> q = p if q fails or q eats less input than p

> (<!>) :: Parser a b -> Parser a c -> Parser a b
> p <!> q = \inp -> case q inp of
>                       Failure _ -> p inp
>                       Success (_, nq, _, _) ->
>                           case p inp of
>                               Failure pos -> Failure pos
>                               r@(Success (_, np, _, _)) ->
>                                   if nq >= np
>                                   then Failure (get_pos inp)
>                                   else r

> pPred :: (a -> Bool) -> Parser a a
> pPred f = \inp -> case inp of
>                          [] -> Failure (get_pos inp)
>                          ((t, p):ts) -> if f t
>                                         then Success (t, 1, ts, get_pos ts)
>                                         else Failure p

