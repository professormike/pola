
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Common.Tokens (Token(..), StringContents(..), StringContent(..), Infix(..),
>                fromInteger', fromFloat,
>                is_integer, is_float, is_literal,
>                is_VarID,   is_VarID_name,    fromVarID,
>                is_VarSym,                    fromVarSym,
>                is_ConID,                     fromConID,
>                is_ConSym,                    fromConSym,
>                is_QVarID,  is_QVarID_infix,  fromQVarID,  tupleQVarID,
>                is_QVarSym, is_QVarSym_infix, fromQVarSym, tupleQVarSym,
>                is_QConID,  is_QConID_infix,  fromQConID,  tupleQConID,
>                is_QConSym, is_QConSym_infix, fromQConSym, tupleQConSym,
>                is_white_space, is_parseable_token,
>                pp_tokens,
>                is_bang, is_plus, is_negative) where

> data Token =
> -- The "real" tokens:
>              Special Char
>            | WhiteChar Char
>            | Newline String
>            | Comment Int String
>            | NCommentOpen
>            | NCommentClose
>            | ReservedID String
>            | ReservedOp String
>            | QVarID Infix (Maybe String) String
>            | QConID Infix (Maybe String) String
>            | QVarSym Infix (Maybe String) String
>            | QConSym Infix (Maybe String) String
>            | Integer String
>            | Float String
>            | String StringContents
>            | Char String
> -- Implicit '{', '}' and ';' (for the layout rule)
>            | ImplicitSpecial Char
> -- The indentation of the following token and current line
> -- (for the layout rule)
>            | IndentToken Int
>            | IndentLine Int
>               deriving (Show, Eq, Read, Ord)

> data StringContents = StringContents [StringContent]
>   deriving (Show, Eq, Read, Ord)
> data StringContent = GapStart
>                    | GapEnd
>                    | StringChar String
>                    | StringWhiteChar String
>   deriving (Show, Eq, Read, Ord)

> data Infix = InfixN Int | InfixL Int | InfixR Int
>   deriving (Show, Eq, Read, Ord)

> get_name :: Maybe String -> String -> String
> get_name Nothing s = s
> get_name (Just c) s = c ++ "." ++ s

> is_integer :: Token -> Bool
> is_integer (Integer _) = True
> is_integer _ = False

> is_float :: Token -> Bool
> is_float (Float _) = True
> is_float _ = False

> is_literal :: Token -> Bool
> is_literal (Char _) = True
> is_literal (String _) = True
> is_literal t = is_integer t || is_float t

> is_bang :: Token -> Bool
> is_bang (QVarSym _ Nothing "!") = True
> is_bang _ = False

> is_plus :: Token -> Bool
> is_plus (QVarSym _ Nothing "+") = True
> is_plus _ = False

> is_negative :: Token -> Bool
> is_negative (QVarSym _ Nothing "-") = True
> is_negative _ = False

> fromInteger' :: Token -> String
> fromInteger' (Integer s) = s
> fromInteger' t = error ("fromInteger: " ++ show t)

> fromFloat :: Token -> String
> fromFloat (Float s) = s
> fromFloat t = error ("fromFloat: " ++ show t)

> is_VarID :: Token -> Bool
> is_VarID (QVarID _ Nothing _) = True
> is_VarID _ = False
> is_VarID_name :: String -> Token -> Bool
> is_VarID_name n (QVarID _ Nothing s) = n == s
> is_VarID_name _ _ = False
> fromVarID :: Token -> String
> fromVarID (QVarID _ Nothing s) = s
> fromVarID t = error ("fromVarID: " ++ show t)

> is_VarSym :: Token -> Bool
> is_VarSym (QVarSym _ Nothing _) = True
> is_VarSym _ = False
> fromVarSym :: Token -> String
> fromVarSym (QVarSym _ Nothing s) = s
> fromVarSym t = error ("fromVarSym: " ++ show t)

> is_ConID :: Token -> Bool
> is_ConID (QConID _ Nothing _) = True
> is_ConID _ = False
> fromConID :: Token -> String
> fromConID (QConID _ Nothing s) = s
> fromConID t = error ("fromConID: " ++ show t)

> is_ConSym :: Token -> Bool
> is_ConSym (QConSym _ Nothing _) = True
> is_ConSym _ = False
> fromConSym :: Token -> String
> fromConSym (QConSym _ Nothing s) = s
> fromConSym t = error ("fromConSym: " ++ show t)

> is_QVarID_infix :: Infix -> Token -> Bool
> is_QVarID_infix i (QVarID j _ _) = i == j
> is_QVarID_infix _ _ = False
> is_QVarID :: Token -> Bool
> is_QVarID (QVarID _ Nothing _) = True
> is_QVarID _ = False
> fromQVarID :: Token -> String
> fromQVarID (QVarID _ c s) = get_name c s
> fromQVarID t = error ("fromQVarID: " ++ show t)
> tupleQVarID :: Token -> (Maybe String, String)
> tupleQVarID (QVarID _ c s) = (c, s)
> tupleQVarID t = error ("tupleQVarID: " ++ show t)

> is_QVarSym :: Token -> Bool
> is_QVarSym (QVarSym _ _ _) = True
> is_QVarSym _ = False
> is_QVarSym_infix :: Infix -> Token -> Bool
> is_QVarSym_infix i (QVarSym j _ _) = i == j
> is_QVarSym_infix _ _ = False
> fromQVarSym :: Token -> String
> fromQVarSym (QVarSym _ c s) = get_name c s
> fromQVarSym t = error ("fromQVarSym: " ++ show t)
> tupleQVarSym :: Token -> (Maybe String, String)
> tupleQVarSym (QVarSym _ c s) = (c, s)
> tupleQVarSym t = error ("tupleQVarSym: " ++ show t)

> is_QConID_infix :: Infix -> Token -> Bool
> is_QConID_infix i (QConID j _ _) = i == j
> is_QConID_infix _ _ = False
> is_QConID :: Token -> Bool
> is_QConID (QConID _ _ _) = True
> is_QConID _ = False
> fromQConID :: Token -> String
> fromQConID (QConID _ c s) = get_name c s
> fromQConID t = error ("fromQConID: " ++ show t)
> tupleQConID :: Token -> (Maybe String, String)
> tupleQConID (QConID _ c s) = (c, s)
> tupleQConID t = error ("tupleQConID: " ++ show t)

> is_QConSym :: Token -> Bool
> is_QConSym (QConSym _ _ _) = True
> is_QConSym _ = False
> is_QConSym_infix :: Infix -> Token -> Bool
> is_QConSym_infix i (QConSym j _ _) = i == j
> is_QConSym_infix _ _ = False
> fromQConSym :: Token -> String
> fromQConSym (QConSym _ c s) = get_name c s
> fromQConSym t = error ("fromQConSym: " ++ show t)
> tupleQConSym :: Token -> (Maybe String, String)
> tupleQConSym (QConSym _ c s) = (c, s)
> tupleQConSym t = error ("tupleQConSym: " ++ show t)

is_white_space t returns True if and only if t is a white space
character, which includes comments, tabs and newlines. It is used to
determine what is and isn't passed to the automaton.

> is_white_space :: Token -> Bool
> is_white_space NCommentOpen = True
> is_white_space NCommentClose = True
> is_white_space (Comment _ _) = True
> is_white_space (WhiteChar _) = True
> is_white_space (Newline _) = True
> is_white_space _ = False

is_parseable_token returns True iff the token should be passed to the
parser.

> is_parseable_token :: Token -> Bool
> is_parseable_token (WhiteChar _) = False
> is_parseable_token (Newline _) = False
> is_parseable_token (IndentToken _) = False
> is_parseable_token (IndentLine _) = False
> is_parseable_token NCommentOpen = False
> is_parseable_token NCommentClose = False
> is_parseable_token (Comment _ _) = False
> is_parseable_token _ = True

pp_tokens prints a list of tokens (roughly) as it would have appeared.
Implicit characters are also printed.
Printing Indent* may be useful and they'll be gone by the end anyway.

> pp_tokens :: [Token] -> String
> pp_tokens = concat . map pp_token

> pp_token :: Token -> String
> pp_token (Special c) = [c]
> pp_token (WhiteChar c) = [c]
> pp_token (Newline s) = s
> pp_token (Comment i s) = replicate i '-' ++ s
> pp_token NCommentOpen = "{-"
> pp_token NCommentClose = "-}"
> pp_token (ReservedID s) = s
> pp_token (ReservedOp s) = s
> pp_token (QVarID _ c s) = get_name c s
> pp_token (QVarSym _ c s) = get_name c s
> pp_token (QConID _ c s) = get_name c s
> pp_token (QConSym _ c s) = get_name c s
> pp_token (Integer s) = s
> pp_token (Float s) = s
> pp_token (String s) = pp_string s
> pp_token (Char s) = s
> pp_token (ImplicitSpecial c) = [c]
> pp_token (IndentToken n) = "{" ++ show n ++ "}"
> pp_token (IndentLine n) = "<" ++ show n ++ ">"

> pp_string :: StringContents -> String
> pp_string (StringContents scs) = "\"" ++ pp_scs scs ++ "\""

> pp_scs :: [StringContent] -> String
> pp_scs = concat . map pp_sc

> pp_sc :: StringContent -> String
> pp_sc GapStart = "\\"
> pp_sc GapEnd = "\\"
> pp_sc (StringChar s) = s
> pp_sc (StringWhiteChar s) = s

