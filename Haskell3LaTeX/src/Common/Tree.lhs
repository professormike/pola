
> module Common.Tree (Tree(..), LitTrans(..), Def(..), Expr(..), Func) where

> import Common.Tokens
> import Data.List

> data Tree = Tree [LitTrans] [Def]
> data LitTrans = TokEq String Token
>               | TokF String Func -- Name of a functions :; Token -> Bool
> data Def = Def Bool String Expr
>   deriving Show
> data Expr = LiteralE String
>           | LinkE String
>           | Seqs [Expr]
>           | Choices [Expr]
>           | List Expr Integer
>           | SepList Expr Expr Integer
>           | Optional Expr
>   deriving Show

> type Func = String

