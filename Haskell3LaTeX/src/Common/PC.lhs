
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Common.PC (module Common.PCbase,
>            (<*), (<$>), (<&>), (<&), (<$), (|>), pSym, pSListEnd, accepts_empty,
>            pPredHolds, pList, pSList, pMaybe, pEither, pSyms, opt) where

> import Prelude hiding ((<$>), (<*>), (<$), (<*))
> import Common.Utils
> import Common.PCbase
> import Common.Position

> accepts_empty :: Parser a b -> Bool
> accepts_empty p = parse_succeeded (p [])

> pSListEnd :: Parser a (b -> c)          -- Give it a seperator and it will
>                                         -- give you a thing
>           -> Parser a d                 -- A thing that might be an end
>           -> Parser a b                 -- A seperator
>           -> (d -> b -> c)              -- Convert something that might
>                                         -- be an end to a thing
>           -> Parser a ([c], d, Maybe b)
> pSListEnd p_r p_e p_s conv =
>          flip ($)
>      <$> p_e
>      <*> (    (\s rest e -> cons_thing (conv e) s rest) <$> p_s <*> p_rest
>           <|  (\ms e -> ([], e, ms)) <$> pMaybe p_s
>          )
>  <|>     cons_thing <$> p_r <*> p_s <*> p_rest
>   where cons_thing thing sep (rest, e, s) = (thing sep:rest, e, s)
>         p_rest = pSListEnd p_r p_e p_s conv

Take a string and return a b, the number of characters consumed and the
rest of the input

> infix  4 `opt`
> infixl 7 <$>
> infixl 7 <$
> infixl 7 <*
> infixl 7 <&>
> infixl 7 <&
> infixl 3 |>

> (|>) :: Parser a b -> Parser a b -> Parser a b
> p  |> q = q <|  p

> (<*) :: Parser a b -> Parser a c -> Parser a b
> p <* q = const <$> p <*> q

> (<$>) :: (b -> c) -> Parser a b -> Parser a c
> f <$> p = pSucceed f <*> p

> (<&>) :: (b -> c) -> Parser a b -> Parser a (c, Position)
> f <&> p = keep_pos (f <$> p)

> (<&) :: b -> Parser a c -> Parser a (b, Position)
> f <& p = const f <&> p

> (<$) :: b -> Parser a c -> Parser a b
> p <$  q = const p <$> q

> pSym :: (Eq a) => a -> Parser a a
> pSym = pPred . (==)

> pSyms :: (Eq a) => [a] -> Parser a [a]
> pSyms = foldr (\c p -> (:) <$> pSym c <*> p) (pSucceed [])

> pPredHolds :: (a -> Bool) -> Parser a Bool
> pPredHolds f = True <$  pPred f `opt` False

Parser for a seperated list, optionally with a trailing seperator

> pList :: Int -> Parser a b -> Parser a [b]
> pList i p	| i == 0 = pList0 p
> 		| otherwise = (:) <$> p <*> pList (i - 1) p

pSList :: Bool                    -- Accept a seperator at the end?
       -> Int                     -- Minimum number of things
       -> Parser a b              -- Parser for the seperator
       -> Parser a c              -- Parser for the thing
       -> Parser a ([Either b c])

> pSList :: Bool -> Int -> Parser a b -> Parser a c -> Parser a ([Either b c])
> pSList _ _ p_s p_t
>  | accepts_empty p_s && accepts_empty p_t
>   = error "p_s and p_t can't both accept empty in pSList"
> pSList end i p_s p_t	| i == 0 = psList0
> 			| otherwise = psListI where
> 	psList0 = pSList end 1 p_s p_t
>                    <|> (if end then wrap . Left <$> p_s else pFail)
>                    <|> if accepts_empty p_t
>                        then pFail
>                        else pSucceed []
> 	psListI = (\h mid t -> h ++ concat mid ++ t)
>    		<$> (wrap . Right <$> p_t)
>    		<*> pList (i - 1) ((\s t -> [Left s, Right t]) <$> p_s <*> p_t)
>    		<*> if end then wrap . Left <$> p_s `opt` []
>               	else pSucceed []

> pMaybe :: Parser a b -> Parser a (Maybe b)
> pMaybe p = Just <$> p `opt` Nothing

> pEither :: Parser a b -> Parser a c -> Parser a (Either b c)
> pEither p1 p2 = Left <$> p1 <|> Right <$> p2

> opt :: Parser a b -> b -> Parser a b
> p `opt` v = p <|  pSucceed v

