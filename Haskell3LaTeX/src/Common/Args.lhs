
> module Common.Args (Config(..), parse_args) where

> data Config = Code { header_file :: String, out_file :: String }
>             | Diags { out_dir :: String, prefix :: String }

> default_code_config :: Config
> default_code_config = Code { header_file = "IA.header", out_file="IA.lhs" }

> default_diags_config :: Config
> default_diags_config = Diags { out_dir = ".", prefix = "diag" }

> parse_args :: [String] -> Config
> parse_args ("--code":xs) = parse_code_args default_code_config xs
> parse_args ("--diags":xs) = parse_diags_args default_diags_config xs
> parse_args _ = error "Must specify --code or --diags first" -- XXX

> parse_code_args :: Config -> [String] -> Config
> parse_code_args c [] = c
> parse_code_args c ("-o":x:xs) = parse_code_args (c { out_file = x }) xs
> parse_code_args c ("-H":x:xs) = parse_code_args (c { header_file = x }) xs
> parse_code_args _ _ = error "Bad args" -- XXX

> parse_diags_args :: Config -> [String] -> Config
> parse_diags_args c [] = c
> parse_diags_args c ("-o":x:xs) = parse_diags_args (c { out_dir = x }) xs
> parse_diags_args c ("-p":x:xs) = parse_diags_args (c { prefix = x }) xs
> parse_diags_args _ _ = error "Bad args" -- XXX

