
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

"" should be Nothing?

> module Haskell2LaTeX.Subject (get_subject_Topdecl, get_subject_Decl,
>                 get_subject_Cdecl, get_subject_Idecl)  where

> import Haskell2LaTeX.Tree

> get_subject_Topdecl :: Topdecl -> Maybe String
> get_subject_Topdecl (Topdecl_decl decl) = get_subject_Decl decl
> get_subject_Topdecl _ = Nothing

> get_subject_Decl :: Decl -> Maybe String
> get_subject_Decl (Decl_gendecl gendecl) = get_subject_Gendecl gendecl
> get_subject_Decl (Decl_fun funlhs _) = Just (get_subject_Funlhs funlhs)
> get_subject_Decl (Decl_pat0 pat_i _) = Just (get_subject_Pat_i pat_i)

> get_subject_Cdecl :: Cdecl -> Maybe String
> get_subject_Cdecl (Cdecl_gendecl gendecl) = get_subject_Gendecl gendecl
> get_subject_Cdecl (Cdecl_fun funlhs _) = Just (get_subject_Funlhs funlhs)
> get_subject_Cdecl (Cdecl_var var _) = Just (get_subject_Var var)

> get_subject_Idecl :: Idecl -> Maybe String
> get_subject_Idecl (Idecl_fun funlhs _) = Just (get_subject_Funlhs funlhs)
> get_subject_Idecl (Idecl_var var _) = Just (get_subject_Var var)
> get_subject_Idecl Idecl_empty = Nothing

> get_subject_Gendecl :: Gendecl -> Maybe String
> get_subject_Gendecl (Gendecl_type vars _ _) = Just (get_subject_Vars vars)
> get_subject_Gendecl _ = Nothing

> get_subject_Funlhs :: Funlhs -> String
> get_subject_Funlhs (Funlhs_var var _) = get_subject_Var var
> get_subject_Funlhs (Funlhs_pat _ varop _) = get_subject_Varop varop
> get_subject_Funlhs (Funlhs_lpat _ varop _) = get_subject_Varop varop
> get_subject_Funlhs (Funlhs_rpat _ varop _) = get_subject_Varop varop
> get_subject_Funlhs (Funlhs_apats funlhs _) = get_subject_Funlhs funlhs

> get_subject_Pat_i :: Pat_i -> String
> get_subject_Pat_i (Pat_i_pat p _) = get_subject_Pat_i p
> get_subject_Pat_i (Pat_i_10 (Pat_10_apat (Apat_var v _))) = get_subject_Var v
> get_subject_Pat_i _ = ""

> get_subject_Vars :: Vars -> String
> get_subject_Vars (Vars vars) = comma_sep (map get_subject_Var vars)

> get_subject_Var :: Var -> String
> get_subject_Var (Var_varid varid) = varid
> get_subject_Var (Var_varsym varsym) = varsym

> get_subject_Varop :: Varop -> String
> get_subject_Varop (Varop_varid varid) = varid
> get_subject_Varop (Varop_varsym varsym) = varsym

> comma_sep :: [String] -> String
> comma_sep [] = ""
> comma_sep [s] = s
> comma_sep (s:ss) = s ++ "," ++ comma_sep ss

