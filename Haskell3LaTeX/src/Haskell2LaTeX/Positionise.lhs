
> module Haskell2LaTeX.Positionise (Position, end_of_file, get_pos_char,
>                     furthest_pos, positionise, show_pos) where

> import Common.Position

> import Data.Char

> data Classified = Comment String Line Bool -- Bool True = comment is white
>                 | BirdTracks String [(Char, Position)] Position
>                 | CodeBlock String [(Char, Position)] Position
>   deriving Show

> positionise :: Bool
>             -> String
>             -> [Either String (String, [(Char, Position)], Position)]
> positionise unlit inp
>  | unlit     = (collapse . check . classify) inp_lines
>  | otherwise = [Right (inp, concat (map add_pos inp_lines), End_Of_File)]
>  where inp_lines = zip (lineify inp) [1..]

> add_pos :: (String, Line) -> [(Char, Position)]
> add_pos (xs, l) = zip xs (map (Position l) [1..])

=====

> classify :: [(String, Line)] -> [Classified]
> classify [] = []
> classify sos@(('>':_, _):_) = this:classify rest
>  where (block, rest) = span (is_bird_track . fst) sos
>        block_string = concat $ map fst block
>        block_with_pos = concat $ map (detrack . add_pos) block
>        last_pos = snd $ last block_with_pos
>        this = BirdTracks block_string block_with_pos last_pos
> classify ((first_line, l):sos)
>  | is_line "\\begin{code}" first_line = this:classify rest
>  where (block, end_rest) = break (is_line "\\end{code}" . fst) sos
>        block_string = concat $ map fst block
>        block_with_pos = concat $ map add_pos block
>        (rest, end_line) = case end_rest of
>                               [] -> error $ unfinished_block "code" l
>                               (_, el):rs -> (rs, el)
>        this = CodeBlock block_string block_with_pos (Position end_line 1)
> classify (fl@(first_line, l):sos)
>  | is_line "\\begin{verbatim}" first_line = this:classify rest
>  where (block, end_rest) = break (is_line "\\end{verbatim}" . fst) sos
>        (last_line, rest) = case end_rest of
>                                [] -> error $ unfinished_block "verbatim" l
>                                ll:r -> (ll, r)
>        block_string = concat $ map fst $ [fl] ++ block ++ [last_line]
>        this = Comment block_string l False
> classify ((s, l):sos) = Comment s l (all isSpace s):classify sos

> is_line :: String -> String -> Bool
> is_line s l = s == s' && all isSpace white
>   where (s', white) = splitAt (length s) l

> unfinished_block :: String -> Line -> String
> unfinished_block b l = "EOF in " ++ b ++ " block started on line " ++ show l

> is_bird_track :: String -> Bool
> is_bird_track ('>':_) = True
> is_bird_track _ = False

> detrack :: [(Char, Position)] -> [(Char, Position)]
> detrack (('>', p):cps) = (' ', p):cps
> detrack _ = error "detrack: Line beginning with '>' has no first character?!"

=====

> check :: [Classified] -> [Classified]
> check [] = []
> check (Comment _ line False:BirdTracks _ _ _:_)
>  = error $ "Program line next to comment at line " ++ show line
> check (BirdTracks _ _ _:Comment _ line False:_)
>  = error $ "Program line next to comment at line " ++ show line
> check (x:xs) = x:check xs

> collapse :: [Classified]
>          -> [Either String (String, [(Char, Position)], Position)]
> collapse [] = []
> collapse (Comment s1 l _:Comment s2 _ _:cs)
>  = collapse (Comment (s1 ++ s2) l False:cs)
> collapse (Comment s _ _:cs) = Left s:collapse cs
> collapse (BirdTracks s cps p:cs) = Right (s, cps, p):collapse cs
> collapse (CodeBlock  s cps p:cs) = Right (s, cps, p):collapse cs

> lineify :: String -> [String]
> lineify "" = []
> lineify xs = ys:lineify zs
>  where (ys, zs) = takeDropUntil ((==) '\n') xs

> takeDropUntil :: (a -> Bool) -> [a] -> ([a], [a])
> takeDropUntil _ [] = ([], [])
> takeDropUntil p (x:xs) = if p x then ([x], xs)
>                                 else (x:ys, zs)
>  where (ys, zs) = takeDropUntil p xs

\end{document}

