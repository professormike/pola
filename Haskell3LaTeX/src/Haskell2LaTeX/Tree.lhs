
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Haskell2LaTeX.Tree (
>              Parsed(..),
>              ModuleHead(..),
>              ModuleTail(..),
>              Literal(..),
>              Module(..),
>              Body(..),
>              Impdecls(..),
>              Exports(..),
>              Export(..),
>              Impdecl(..),
>              Impspec(..),
>              Import(..),
>              Topdecls(..),
>              Topdecl(..),
>              Decls(..),
>              Decl(..),
>              Cdecls(..),
>              Cdecl(..),
>              Idecls(..),
>              Idecl(..),
>              Gendecl(..),
>              Ops(..),
>              Vars(..),
>              Fixity(..),
>              Type(..),
>              Btype(..),
>              Atype(..),
>              Gtycon(..),
>              Context(..),
>              Class(..),
>              Scontext(..),
>              Simpleclass(..),
>              Simpletype(..),
>              Constrs(..),
>              Constr(..),
>              Newconstr(..),
>              Fielddecl(..),
>              Deriving(..),
>              Dclass(..),
>              Inst(..),
>              Funlhs(..),
>              Rhs(..),
>              Gdrhs(..),
>              Gd(..),
>              Exp(..),
>              Exp_i(..),
>              Lexp_i(..),
>              Rexp_i(..),
>              Exp_10(..),
>              Fexp(..),
>              Aexp(..),
>              Qual(..),
>              Alts(..),
>              Alt(..),
>              Gdpat(..),
>              Stmts(..),
>              Stmt(..),
>              Fbind(..),
>              Pat(..),
>              Pat_i(..),
>              Lpat_i(..),
>              Rpat_i(..),
>              Pat_10(..),
>              Apat(..),
>              Fpat(..),
>              Gcon(..),
>              Var(..),
>              Qvar(..),
>              Con(..),
>              Qcon(..),
>              Varop(..),
>              Qvarop(..),
>              Conop(..),
>              Qconop(..),
>              Op(..),
>              Qop(..),
>              Gconsym(..),
>              VarID,
>              ConID,
>              VarSym,
>              ConSym,
>              Tyvar,
>              Tycon,
>              Tycls,
>              Tycon_Tycls,
>              Modid,
>              QVarID,
>              QConID,
>              Qtycon,
>              Qtycls,
>              Qtycon_Qtycls,
>              QVarSym,
>              QConSym) where
> import Common.Tokens

> data Parsed = ParsedHead ModuleHead
>             | ParsedImpD Impdecls
>             | ParsedTopD Topdecls
>             | ParsedTail ModuleTail
>   deriving Show

> data Literal = PLiteral Token
>   deriving Show

> type VarID = String
> type ConID = String
> type VarSym = String
> type ConSym = String
> type Tyvar = VarID
> type Tycon = ConID
> type Tycls = ConID
> type Tycon_Tycls = ConID
> type Modid = ConID

> type QVarID = (Maybe String, VarID)
> type QConID = (Maybe String, ConID)
> type QVarSym = (Maybe String, VarSym)
> type QConSym = (Maybe String, ConSym)
> type Qtycon = QConID
> type Qtycls = QConID
> type Qtycon_Qtycls = QConID

---------------

> data Module = Module_with_id Modid (Maybe Exports) Body
>             | Module_no_id Body
>   deriving Show

> data ModuleHead = ModuleHead_with_id Modid (Maybe Exports) Bool
>                 | ModuleHead_no_id Bool
>   deriving Show

> data ModuleTail = ModuleTail Bool
>   deriving Show

> data Body = Body_both Bool Impdecls Bool Topdecls
>           | Body_imp Bool Impdecls
>           | Body_top Bool Topdecls
>   deriving Show

> data Impdecls = Impdecls [Either Bool Impdecl]
>   deriving Show

> data Exports = Exports [Export] Bool
>   deriving Show

> data Export = Export_qvar Qvar
>             | Export_qtycon_qtycls Qtycon_Qtycls (Maybe (Maybe (Either [Con] [Qvar])))
>             | Export_module Modid
>   deriving Show

> data Impdecl = Impdecl Bool Modid (Maybe Modid) (Maybe Impspec)
>              | Impdecl_empty
>   deriving Show

> data Impspec = Impspec Bool [Import] Bool
>   deriving Show

> data Import = Import_var Var
>             | Import_tycon_tycls Tycon_Tycls (Maybe (Maybe (Either [Con] [Var])))
>   deriving Show

> data Topdecls = Topdecls [Either Bool Topdecl]
>   deriving Show

> data Topdecl = Topdecl_type Simpletype Type
>              | Topdecl_data (Maybe Context)
>                             Simpletype
>                             Constrs
>                             (Maybe Deriving)
>              | Topdecl_newtype (Maybe Context)
>                                Simpletype
>                                Newconstr
>                                (Maybe Deriving)
>              | Topdecl_class (Maybe Scontext) Tycls Tyvar (Maybe Cdecls)
>              | Topdecl_instance (Maybe Scontext) Qtycls Inst (Maybe Idecls)
>              | Topdecl_default [Type]
>              | Topdecl_decl Decl
>   deriving Show

> data Decls = Decls Bool [Either Bool Decl]
>   deriving Show

> data Decl = Decl_gendecl Gendecl
>           | Decl_fun Funlhs Rhs
>           | Decl_pat0 Pat_i Rhs
>   deriving Show

> data Cdecls = Cdecls Bool [Either Bool Cdecl]
>   deriving Show

> data Cdecl = Cdecl_gendecl Gendecl
>            | Cdecl_fun Funlhs Rhs
>            | Cdecl_var Var Rhs
>   deriving Show

> data Idecls = Idecls Bool [Either Bool Idecl]
>   deriving Show

> data Idecl = Idecl_fun Funlhs Rhs
>            | Idecl_var Var Rhs
>            | Idecl_empty
>   deriving Show

> data Gendecl = Gendecl_type Vars (Maybe Context) Type
>              | Gendecl_fixity Fixity (Maybe String) Ops
>              | Gendecl_empty
>   deriving Show

> data Ops = Ops [Op]
>   deriving Show

> data Vars = Vars [Var]
>   deriving Show

> data Fixity = Fixity_infixl | Fixity_infixr | Fixity_infix
>   deriving Show

> data Type = Type Btype (Maybe Type)
>   deriving Show

> data Btype = Btype [Atype]
>   deriving Show

> data Atype = Atype_gtycon Gtycon
>            | Atype_tyvar Tyvar
>            | Atype_tuple [Type]
>            | Atype_list Type
>            | Atype_paren Type
>   deriving Show

> data Gtycon = Gtycon_qtycon Qtycon
>             | Gtycon_unit
>             | Gtycon_list
>             | Gtycon_function
>             | Gtycon_tupling Int
>   deriving Show

> data Context = Context Class
>              | Context_list [Class]
>   deriving Show

> data Class = Class Qtycls Tyvar
>            | Class_list Qtycls Tyvar [Atype]
>   deriving Show

> data Scontext = Scontext Simpleclass
>               | Scontext_list [Simpleclass]
>   deriving Show

> data Simpleclass = Simpleclass Qtycls Tyvar
>   deriving Show

> data Simpletype = Simpletype Tycon [Tyvar]
>   deriving Show

> data Constrs = Constrs [Constr]
>   deriving Show

> data Constr = Constr_atype Con [(Bool, Atype)]
>             | Constr_infix (Either Btype Atype) Conop (Either Btype Atype)
>             | Constr_field Con [Fielddecl]
>   deriving Show

> data Newconstr = Newconstr_atype Con Atype
>                | Newconstr_var_type Con Var Type
>   deriving Show

> data Fielddecl = Fielddecl Vars (Either Type Atype)
>   deriving Show

> data Deriving = Deriving Dclass
>               | Deriving_list [Dclass]
>   deriving Show

> data Dclass = Dclass Qtycls
>   deriving Show

> data Inst = Inst_gtycon Gtycon
>           | Inst_gtycon_concr_list Gtycon [Gtycon]
>           | Inst_gtycon_list Gtycon [Tyvar]
>           | Inst_tuple [Tyvar]
>           | Inst_list Tyvar
>           | Inst_function Tyvar Tyvar
>   deriving Show

> data Funlhs = Funlhs_var Var [Apat]
>             | Funlhs_pat Pat_i Varop Pat_i
>             | Funlhs_lpat Lpat_i Varop Pat_i
>             | Funlhs_rpat Pat_i Varop Rpat_i
>             | Funlhs_apats Funlhs [Apat]
>   deriving Show

> data Rhs = Rhs_exp Exp (Maybe Decls)
>          | Rhs_gdrhs Gdrhs (Maybe Decls)
>   deriving Show

> data Gdrhs = Gdrhs Gd Exp (Maybe Gdrhs)
>   deriving Show

> data Gd = Gd Exp_i
>   deriving Show

> data Exp = Exp_sig Exp_i (Maybe Context) Type
>          | Exp Exp_i
>   deriving Show

> data Exp_i = Exp_i_exp Exp_i (Maybe (Qop, Exp_i))
>            | Exp_i_lexp Lexp_i
>            | Exp_i_rexp Rexp_i
>            | Exp_i_10 Exp_10
>   deriving Show

> data Lexp_i = Lexp_i_l Lexp_i Qop Exp_i
>             | Lexp_i_e Exp_i Qop Exp_i
>             | Lexp_i_neg Exp_i
>   deriving Show

> data Rexp_i = Rexp_i_e Exp_i Qop Exp_i
>             | Rexp_i_r Exp_i Qop Rexp_i
>   deriving Show

> data Exp_10 = Exp_10_lambda [Apat] Exp
>             | Exp_10_let Decls Exp
>             | Exp_10_if Exp Exp Exp
>             | Exp_10_case Exp Bool Alts
>             | Exp_10_do Bool Stmts
>             | Exp_10_fexp Fexp
>   deriving Show

> data Fexp = Fexp [Aexp]
>   deriving Show

> data Aexp = Aexp_qvar Qvar
>           | Aexp_gcon Gcon
>           | Aexp_literal Literal
>           | Aexp_paren Exp
>           | Aexp_tuple [Exp]
>           | Aexp_list [Exp]
>           | Aexp_arith Exp (Maybe Exp) (Maybe Exp)
>           | Aexp_listcomp Exp [Qual]
>           | Aexp_left Exp_i Qop
>           | Aexp_right Qop Exp_i
>           | Aexp_qcon Qcon [Fbind]
>           | Aexp_update Aexp [[Fbind]]
>   deriving Show

> data Qual = Qual_generator Pat Exp
>           | Qual_let Decls
>           | Qual_exp Exp
>   deriving Show

> data Alts = Alts [Either Bool Alt]
>   deriving Show

> data Alt = Alt_exp Pat Exp (Maybe Decls)
>          | Alt_gdpat Pat Gdpat (Maybe Decls)
>          | Alt_empty
>   deriving Show

> data Gdpat = Gdpat Gd Exp (Maybe Gdpat)
>   deriving Show

> data Stmts = Stmts [Stmt] Exp (Maybe Bool)
>   deriving Show

> data Stmt = Stmt_exp Exp Bool
>           | Stmt_pat Pat Exp Bool
>           | Stmt_let Decls Bool
>           | Stmt_empty Bool
>   deriving Show

> data Fbind = Fbind Qvar Exp
>   deriving Show

> data Pat = Pat_succ Var String
>          | Pat Pat_i
>   deriving Show

> data Pat_i = Pat_i_pat Pat_i (Maybe (Qconop, Pat_i))
>            | Pat_i_lpat Lpat_i
>            | Pat_i_rpat Rpat_i
>            | Pat_i_10 Pat_10
>   deriving Show

> data Lpat_i = Lpat_i_l Lpat_i Qconop Pat_i
>             | Lpat_i_p Pat_i Qconop Pat_i
>             | Lpat_i_6_integer String
>             | Lpat_i_6_float String
>   deriving Show

> data Rpat_i = Rpat_i_p Pat_i Qconop Pat_i
>             | Rpat_i_r Pat_i Qconop Rpat_i
>   deriving Show

> data Pat_10 = Pat_10_apat Apat
>             | Pat_10_gcon Gcon [Apat]
>   deriving Show

> data Apat = Apat_var Var (Maybe Apat)
>           | Apat_gcon Gcon
>           | Apat_qcon Qcon [Fpat]
>           | Apat_literal Literal
>           | Apat_wildcard
>           | Apat_paren Pat
>           | Apat_tuple [Pat]
>           | Apat_list [Pat]
>           | Apat_irrefutable Apat
>   deriving Show

> data Fpat = Fpat Qvar Pat
>   deriving Show

> data Gcon = Gcon_unit
>           | Gcon_list
>           | Gcon_tuple Int
>           | Gcon_qcon Qcon
>   deriving Show

> data Var = Var_varid VarID
>          | Var_varsym VarSym
>   deriving Show

> data Qvar = Qvar_qvarid QVarID
>           | Qvar_qvarsym QVarSym
>   deriving Show

> data Con = Con_conid ConID
>          | Con_consym ConSym
>   deriving Show

> data Qcon = Qcon_qconid QConID
>           | Qcon_gconsym Gconsym
>   deriving Show

> data Varop = Varop_varsym VarSym
>            | Varop_varid VarID
>   deriving Show

> data Qvarop = Qvarop_qvarsym QVarSym
>             | Qvarop_qvarid QVarID
>   deriving Show

> data Conop = Conop_consym ConSym
>            | Conop_conid ConID
>   deriving Show

> data Qconop = Qconop_gconsym Gconsym
>             | Qconop_qconid QConID
>   deriving Show

> data Op = Op_varop Varop
>         | Op_conop Conop
>   deriving Show

> data Qop = Qop_qvarop Qvarop
>          | Qop_qconop Qconop
>   deriving Show

> data Gconsym = Gconsym_cons
>              | Gconsym_qconsym QConSym
>   deriving Show

