
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Haskell2LaTeX.Mangle (get_topdecls_fixities, mangle) where

> import Haskell2LaTeX.Either
> import Common.Tokens
> import Data.Char
> import Common.Position

> get_topdecls_fixities :: Input [(Token, Position)] -> [Token]
> get_topdecls_fixities input = get_end_state f e input
>  where module_name = (get_module_name . map fst . get_merge_rights) input
>        f = get_infixes module_name
>        e = []

\begin{verbatim}
 get_topdecls_fixities :: [(Token, Position)] -> [Token]
 get_topdecls_fixities tps = get_infixes module_name [] block
  where block = (fst . get_block . snd . get_till_block) tps
        module_name = get_module_name $ map fst tps
\end{verbatim}

> mangle :: [Token]
>        -> [(Token, Position)]
>        -> Either String [(Token, Position)]
> mangle ts tps = Right (do_module ts tps)

> do_module :: [Token] -> [(Token, Position)] -> [(Token, Position)]
> do_module prev_infixes tps
>  = fst (do_surrounded_decls_block module_name prev_infixes tps)
>  where module_name = get_module_name $ map fst tps


\verb!get_module_name! takes a list of tokens comprising a Haskell script
and returns the module name of the script.

> get_module_name :: [Token] -> String
> get_module_name ts = case take 2 (filter is_parseable_token ts) of
>                          [ReservedID "module", QConID _ Nothing c] -> c
>                          _ -> "Main"


> do_surrounded_decls_block :: String
>                           -> [Token]
>                           -> [(Token, Position)]
>                           -> ([(Token, Position)], [Token])
> do_surrounded_decls_block m prev_infixes tps = (tps' ++ suffix, new_infixes)
>  where (tps', suffix, new_infixes)
>         = do_surrounded_decls_block_ret_tail m prev_infixes tps


\verb!do_decls_block! takes
    a module name
    A list of fixity information we already know applies to this block
        unless overruled
    a list of tokens comprising a section of a Haskell script
and returns a tuple of
    the tokens before the first block XXX
    the tokens after the block
    XXX

> do_surrounded_decls_block_ret_tail :: String -> [Token]
>                                    -> [(Token, Position)]
>                                    -> ([(Token, Position)],
>                                        [(Token, Position)],
>                                        [Token])
> do_surrounded_decls_block_ret_tail _ prev_infixes [] = ([], [], prev_infixes)
> do_surrounded_decls_block_ret_tail m prev_infixes tps
>  = (prefix ++ tps', suffix, new_infixes)
>  where (prefix, block_and_suffix) = get_till_block tps
>        (block, suffix) = get_block block_and_suffix
>        (tps', new_infixes) = do_decls_block m prev_infixes block


\verb!do_decls_block! takes
    a module name
    A list of fixity information we already know applies to this block
        unless overruled
    a list of tokens comprising the contents of a block including braces
and returns XXX

> do_decls_block :: String -> [Token] -> [(Token, Position)]
>                                     -> ([(Token, Position)], [Token])
> do_decls_block _ prev_infixes [] = ([], prev_infixes)
> do_decls_block m prev_infixes (tp:tps) = (tp:do_decls m tlf tps, tlf)
>  where tlf = get_infixes m prev_infixes tps


\verb!do_decls! takes
    a module name
    A list of fixity information we already know applies to this block
        unless overruled
    a list of tokens comprising a list of decls including
and returns XXX

> do_decls :: String -> [Token] -> [(Token, Position)] -> [(Token, Position)]
> do_decls _ _ [] = []
> do_decls m prev_infixes tps = do_decl m prev_infixes this
>                            ++ do_decls m prev_infixes rest
>  where (this, rest) = get_next_decl tps


\verb!do_decl! takes
    a module name
    A list of fixity information we already know applies to this block
        unless overruled
    a list of tokens comprising a single decl with a trailing semicolon
        or brace
and returns XXX

> do_decl :: String -> [Token] -> [(Token, Position)] -> [(Token, Position)]
> do_decl m prev_infixes tps = left ++ right
>  where (left, rest) = do_decl_left prev_infixes tps
>        right = do_decl_right m prev_infixes rest

\verb!get_next_decl! takes
    a list of tokens comprising a tail of a block of decls, where thei
        first token non-whitespace is the start of a decl
and returns a tuple of
    the tokens of the first decl, including terminating semi-colon or
        brace
    the remaining tokens

> get_next_decl :: [(Token, Position)]
>               -> ([(Token, Position)], [(Token, Position)])
> get_next_decl = get_next_thing is_semi 0
>  where is_semi = (\t -> t == Special ';' || t == ImplicitSpecial ';')


XXX LHS of a decl? Inc = etc?

> do_decl_left :: [Token] -> [(Token, Position)]
>                         -> ([(Token, Position)], [(Token, Position)])
> do_decl_left prev_infixes tps = (subst_infixes prev_infixes left, right)
>  where (left, right) = split_left_right_decl tps

> split_left_right_decl :: [(Token, Position)]
>                       -> ([(Token, Position)], [(Token, Position)])
> split_left_right_decl = get_next_thing is_middle 0
>  where is_middle = (\t -> t == ReservedOp "=" || t == ReservedOp "|")

> do_decl_right :: String -> [Token] -> [(Token, Position)]
>                                    -> [(Token, Position)]
> do_decl_right m prev_infixes tps
>  = do_decl_right_work m these_infixes before_where_block ++ where_tps
>  where (before_where_block, where_block) = get_where tps
>        (where_tps, these_infixes)
>         = do_surrounded_decls_block m prev_infixes where_block

> do_decl_right_work :: String -> [Token] -> [(Token, Position)]
>                                         -> [(Token, Position)]
> do_decl_right_work m prev_infixes tps = do_guards m prev_infixes tps

> do_guards :: String -> [Token] -> [(Token, Position)] -> [(Token, Position)]
> do_guards _ _ [] = []
> do_guards m prev_infixes tps = do_guard m prev_infixes 0 this
>                             ++ do_guards m prev_infixes rest
>  where (this, rest) = get_next_guard tps

> do_guard :: String -> [Token] -> Int -> [(Token, Position)]
>                                      -> [(Token, Position)]
> do_guard _ _ _ [] = []
> do_guard m prev_infixes 0 (tp@(ReservedID "let", _):tps)
>  = tp:(let_tps ++ do_guard m let_infixes 0 after_let)
>  where (let_tps, after_let, let_infixes)
>         = do_surrounded_decls_block_ret_tail m prev_infixes tps
> do_guard m prev_infixes 0 (tp@(ReservedID "do", _):tps)
>  = tp:block ++ do_guard m prev_infixes 0 after_block
>  where (block, after_block)
>         = do_surrounded_stmts_block_ret_tail m prev_infixes tps
> do_guard m prev_infixes n (tp@(Special '[', _):tps)
>  | is_list_comp tps = tp:list_comp ++ do_guard m prev_infixes n after
>  where (list_comp, after) = do_list_comp m prev_infixes tps
> do_guard m prev_infixes n (tp@(t, _):tps)
>  | is_infixible t     = tp':tps'
>  | is_comment_start t = tp:comment ++ do_guard m prev_infixes n rest
>  | otherwise          = tp:tps'
>  where n' | inc_nest t = n + 1
>           | dec_nest t = n - 1
>           | otherwise  = n
>        tps' = do_guard m prev_infixes n' tps
>        tp' = subst_infix_single prev_infixes tp
>        (comment, rest) = get_till_end_of_line tps

> get_next_guard :: [(Token, Position)]
>                -> ([(Token, Position)], [(Token, Position)])
> get_next_guard = get_next_thing (== ReservedOp "|") 0

> get_where :: [(Token, Position)]
>           -> ([(Token, Position)], [(Token, Position)])
> get_where = get_next_thing (== ReservedID "where") 0





> do_surrounded_stmts_block_ret_tail :: String
>                                    -> [Token]
>                                    -> [(Token, Position)]
>                                    -> ([(Token, Position)],
>                                        [(Token, Position)])
> do_surrounded_stmts_block_ret_tail m prev_infixes tps
>  = (prefix ++ do_stmts_block m prev_infixes block, suffix)
>  where (prefix, block_and_suffix) = get_till_block tps
>        (block, suffix) = get_block block_and_suffix

> do_stmts_block :: String -> [Token] -> [(Token, Position)]
>                                     -> [(Token, Position)]
> do_stmts_block _ _ [] = []
> do_stmts_block m prev_infixes (tp:tps) = tp:do_stmts m prev_infixes tps

> do_stmts :: String -> [Token] -> [(Token, Position)] -> [(Token, Position)]
> do_stmts m prev_infixes tps = fst (do_stmts_quals m prev_infixes 0 tps)


> is_list_comp :: [(Token, Position)] -> Bool
> is_list_comp tps = fst (last xs) == ReservedOp "|"
>  where (xs, _) = get_next_thing is_end 0 tps
>        is_end = (\t -> t == ReservedOp "|" || t == Special ']')

> do_list_comp :: String
>              -> [Token]
>              -> [(Token, Position)]
>              -> ([(Token, Position)], [(Token, Position)])
> do_list_comp m prev_infixes tps = (left_tps ++ right_tps, after)
>  where (left, right_after) = get_next_guard tps
>        (right, after) = get_till_end_of_list_comp right_after
>        (right_tps, right_infixes) = do_stmts_quals m prev_infixes 0 right
>        left_tps = do_guard m right_infixes 0 left




> do_stmts_quals :: String
>                -> [Token]
>                -> Int
>                -> [(Token, Position)]
>                -> ([(Token, Position)], [Token])
> do_stmts_quals _ prev_infixes _ [] = ([], prev_infixes)
> do_stmts_quals m prev_infixes 0 (tp@(ReservedID "let", _):tps)
>  = (tp:let_tps ++ rest, infixes)
>  where (let_tps, after_let, let_infixes)
>         = do_surrounded_decls_block_ret_tail m prev_infixes tps
>        (rest, infixes) = do_stmts_quals m let_infixes 0 after_let
> do_stmts_quals m prev_infixes n (tp@(Special '[', _):tps)
>  | is_list_comp tps = (tp:list_comp ++ rest, infixes)
>  where (list_comp, after) = do_list_comp m prev_infixes tps
>        (rest, infixes) = do_stmts_quals m prev_infixes n after
> do_stmts_quals m prev_infixes n (tp@(t, _):tps)
>  | is_infixible t     = (tp':tps', infixes)
>  | is_comment_start t = (tp:comment ++ rest_tps, infix_tps)
>  | otherwise          = (tp:tps', infixes)
>  where n' | inc_nest t = n + 1
>           | dec_nest t = n - 1
>           | otherwise  = n
>        (tps', infixes) = do_stmts_quals m prev_infixes n' tps
>        tp' = subst_infix_single prev_infixes tp
>        (comment, rest) = get_till_end_of_line tps
>        (rest_tps, infix_tps) = do_stmts_quals m prev_infixes n rest

> get_till_end_of_list_comp :: [(Token, Position)]
>                           -> ([(Token, Position)], [(Token, Position)])
> get_till_end_of_list_comp = get_next_thing (== Special ']') 0


> get_infixes :: String -> [Token] -> [(Token, Position)] -> [Token]
> get_infixes m prev_infixes tps = new_infixes ++ prev_infixes'
>  where (new_infixes, redef_infixes)
>         = get_infixes_work m $ filter (is_parseable_token . fst) tps
>        prev_infixes' = filter (not_redundant redef_infixes) prev_infixes

> not_redundant :: [Token] -> Token -> Bool
> not_redundant [] _ = True
> not_redundant (QVarSym _ Nothing s1:_) (QVarSym _ Nothing s2) | s1 == s2 = False
> not_redundant (QVarID  _ Nothing s1:_) (QVarID  _ Nothing s2) | s1 == s2 = False
> not_redundant (QConSym _ Nothing s1:_) (QConSym _ Nothing s2) | s1 == s2 = False
> not_redundant (QConID _ Nothing s1:_) (QConID  _ Nothing s2) | s1 == s2 = False
> not_redundant (_:is) t = not_redundant is t

> get_infixes_work :: String -> [(Token, Position)] -> ([Token], [Token])
> get_infixes_work _ [] = ([], [])
> get_infixes_work m tps = (this_add ++ rest_add, this_sub ++ rest_sub)
>  where (decl, decls) = get_next_decl tps
>        (this_add, this_sub) = get_infixes_work_single m decl
>        (rest_add, rest_sub) = get_infixes_work m decls

> get_infixes_work_single :: String -> [(Token, Position)]
>                                   -> ([Token], [Token])
> get_infixes_work_single m ((ReservedID "infix", _):tps)
>  = (get_new_infixes m (InfixN 9) $ map fst tps, [])
> get_infixes_work_single m ((ReservedID "infixl", _):tps)
>  = (get_new_infixes m (InfixL 9) $ map fst tps, [])
> get_infixes_work_single m ((ReservedID "infixr", _):tps)
>  = (get_new_infixes m (InfixR 9) $ map fst tps, [])
> get_infixes_work_single m ((ReservedID "class", _):tps)
>  = (get_class_infixes m tps, [])
> get_infixes_work_single m tps = ([], redefined m $ map fst left)
>  where (left, _) = split_left_right_decl tps

> get_class_infixes :: String -> [(Token, Position)] -> [Token]
> get_class_infixes m tps = get_infixes m [] where_block
>  where where_block = snd (get_where tps)

> redefined :: String -> [Token] -> [Token]
> redefined _ [] = []
> redefined m (QVarSym i Nothing s:ts) = [QVarSym i Nothing s, QVarSym i (Just m) s]
>                                     ++ redefined m ts
> redefined m (QVarID  i Nothing s:ts) = [QVarID  i Nothing s, QVarID  i (Just m) s]
>                                     ++ redefined m ts
> redefined m (_:ts) = redefined m ts

> -- Need to allow any integer that ends up in the range 0..9
> get_new_infixes :: String -> Infix -> [Token] -> [Token]
> get_new_infixes _ _ [] = []
> get_new_infixes m (InfixN 9) (Integer i:ts)
>  = get_new_infixes m (InfixN (read i)) ts
> get_new_infixes m (InfixL 9) (Integer i:ts)
>  = get_new_infixes m (InfixL (read i)) ts
> get_new_infixes m (InfixR 9) (Integer i:ts)
>  = get_new_infixes m (InfixR (read i)) ts
> get_new_infixes m inf (QVarSym _ Nothing s:ts)
>  = [QVarSym inf Nothing s, QVarSym inf (Just m) s] ++ get_new_infixes m inf ts
> get_new_infixes m inf (QVarID _ Nothing s:ts)
>  = [QVarID inf Nothing s, QVarID inf (Just m) s] ++ get_new_infixes m inf ts
> get_new_infixes m inf (QConSym _ Nothing s:ts)
>  = [QVarSym inf Nothing s, QConSym inf (Just m) s] ++ get_new_infixes m inf ts
> get_new_infixes m inf (QConID _ Nothing s:ts)
>  = [QVarID inf Nothing s, QConID inf (Just m) s] ++ get_new_infixes m inf ts
> get_new_infixes m inf (_:ts) = get_new_infixes m inf ts

> subst_infixes :: [Token] -> [(Token, Position)] -> [(Token, Position)]
> subst_infixes _ [] = []
> subst_infixes ts (tp@(t, _):tps)
>  | is_comment_start t = tp:ys ++ subst_infixes ts zs
>  | is_infixible t = subst_infix_single ts tp:rest
>  | otherwise = tp:rest
>  where rest = subst_infixes ts tps
>        (ys, zs) = get_till_end_of_line tps

> subst_infix_single :: [Token] -> (Token, Position) -> (Token, Position)
> subst_infix_single [] t = t
> subst_infix_single (QVarSym i m1 s1:_) (QVarSym _ m2 s2, p)
>  | (m1, s1) == (m2, s2) = (QVarSym i m1 s1, p)
> {-
> -- subst_infix_single (QVarSym i s:_) (QVarSym _ t, p)
> --  | s == t = (QVarSym i s, p)
> subst_infix_single (ConSym i s:_) (ConSym _ t, p)
>  | s == t = (ConSym i s, p)
> -- subst_infix_single (QConSym i s:_) (QConSym _ t, p)
> --  | s == t = (QConSym i s, p)
> subst_infix_single (VarID i s:_) (VarID _ t, p)
>  | s == t = (VarID i s, p)
> -- subst_infix_single (QVarID i s:_) (QVarID _ t, p)
> --  | s == t = (QVarID i s, p)
> subst_infix_single (ConID i s:_) (ConID _ t, p)
>  | s == t = (ConID i s, p)
> -- subst_infix_single (QConID i s:_) (QConID _ t, p)
> --  | s == t = (QConID i s, p)
> -}
> subst_infix_single (_:ts) tp = subst_infix_single ts tp
> -- This stuff above should be updated


\verb!get_block! takes
    a list of tokens comprising a section of a Haskell script from the
        token following the opening brace denoting the start of a block
and returns a tuple of
    the initial segment of the list of tokens up to and including the
        closing brace of the block, or all the tokens if there is no
        such token (can't happen)
    the remainder of the tokens

> get_block :: [(Token, Position)]
>           -> ([(Token, Position)], [(Token, Position)])
> get_block = get_next_thing is_close_brace (-1)
>  where is_close_brace = (\t -> t == Special '}' || t == ImplicitSpecial '}')


\verb!get_till_block! takes
    a list of tokens comprising a section of a Haskell script
and returns a tuple of
    the tokens up to but not including the first opening brace (i.e.,
        the beginning of a block), or all the tokens if there is no
        opening brace
    the remainder of the tokens

> get_till_block :: [(Token, Position)] -> ([(Token, Position)], [(Token, Position)])
> get_till_block = break (is_open_brace . fst)
>   where is_open_brace :: Token -> Bool
>         is_open_brace (Special '{') = True
>         is_open_brace (ImplicitSpecial '{') = True
>         is_open_brace _ = False

\begin{verbatim}
 get_till_block [] = ([], [])
 get_till_block (tp@(t, _):tps)
  | t == Special '{' || t == ImplicitSpecial '{' = ([], tp:tps)
 get_till_block (tp:tps) = (tp:ys, zs)
  where (ys, zs) = get_till_block tps
\end{verbatim}




> inc_nest :: Token -> Bool
> inc_nest (Special '{') = True
> inc_nest (ImplicitSpecial '{') = True
> inc_nest (Special '[') = True
> inc_nest (Special '(') = True
> inc_nest NCommentOpen = True
> inc_nest _ = False
> dec_nest :: Token -> Bool
> dec_nest (Special '}') = True
> dec_nest (ImplicitSpecial '}') = True
> dec_nest (Special ']') = True
> dec_nest (Special ')') = True
> dec_nest NCommentClose = True
> dec_nest _ = False

> is_infixible :: Token -> Bool
> is_infixible (QVarSym _ _ _) = True
> is_infixible (QConSym _ _ _) = True
> is_infixible (QVarID  _ _ _) = True
> is_infixible (QConID  _ _ _) = True
> is_infixible _ = False

> get_till_end_of_line :: [(Token, Position)]
>                      -> ([(Token, Position)], [(Token, Position)])
> get_till_end_of_line = get_next_thing is_newline 0

> is_newline :: Token -> Bool
> is_newline (Newline _) = True
> is_newline _ = False


get_next_thing takes
    a predicate on Tokens
    a nesting depth
    a list of tokens
and returns a tuple of
    the initial segment of the list of tokens up to and including the
        first token at nesting depth 0 that the predicate holds for,
        or all the tokens if there is no such token
    the remainder of the tokens

> get_next_thing :: (Token -> Bool)
>                -> Int
>                -> [(Token, Position)]
>                -> ([(Token, Position)], [(Token, Position)])
> get_next_thing _ _ [] = ([], [])
> get_next_thing f 0 (tp@(t, _):tps) | f t = ([tp], tps)
> get_next_thing f n (tp@(t, _):tps) = (tp:ys, zs)
>  where n' | inc_nest t = n + 1
>           | dec_nest t = n - 1
>           | otherwise  = n
>        (ys, zs) = get_next_thing f n' tps

> is_comment_start :: Token -> Bool
> -- XXX fix this: is_comment_start (CommentStart _) = True
> is_comment_start _ = False

\end{document}

