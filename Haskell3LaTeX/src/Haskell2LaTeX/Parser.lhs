
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Haskell2LaTeX.Parser (parse, parse_foo, parser_list) where

> import Prelude hiding ((<$>), (<*>), (<$), (<*))
> import Haskell2LaTeX.Either
> import Common.Tokens
> import Haskell2LaTeX.Tree
> import Common.PC
> import Data.Maybe
> import Data.Char
> import Common.Position

> parse :: Input [(Token,Position)] -> (Input [Parsed], [String])
> parse xs = case parsers of
>                [] -> (output, errors)
>                _ -> error "Couldn't use all parsers"
>  where (parsers, output, errors) = mapAccumL_right parse_foo parser_list xs

> parser_list :: [(Parser Token Parsed, Bool)]
> parser_list = [(ParsedHead <$> p_modulehead, True),
>                (ParsedImpD <$> p_impdecls,   False),
>                (ParsedTopD <$> p_topdecls,   False),
>                (ParsedTail <$> p_moduletail, True)]

> parse_foo :: [(Parser Token Parsed, Bool)]
>           -> ([(Token, Position)], Position)
>           -> Either String ([(Parser Token Parsed, Bool)], [Parsed])
> parse_foo ps (tps, _) = Right (ps', these)
>  where (these, ps') = parse_list ps tps

> -- XXX Should only use a parser once per section
> parse_list :: [(Parser Token Parsed, Bool)]
>            -> [(Token, Position)]
>            -> ([Parsed], [(Parser Token Parsed, Bool)])
> parse_list ps [] = ([], ps)
> parse_list [] ((_, p):_)
>  = error ("Ran out of parsers but need to parse more at " ++ show_pos p)
> parse_list pbs@((p, b):pbs') tps@((_, pos):_)
>  = if parse_succeeded this_parse
>    then if b
>         then let (rest, ret_pbs) = parse_list pbs' (rest_of_input this_parse)
>              in (ret_val this_parse:rest, ret_pbs)
>         else if consumed_length this_parse == 0
>              then parse_list pbs' tps
>              else let (rest, ret_pbs) = parse_list pbs (rest_of_input this_parse)
>                   in (ret_val this_parse:rest, ret_pbs)
>    else if b
>         then error ("Required parse failed at " ++ show_pos pos ++ " (followed by " ++ show (length pbs') ++ " parsers)")
>         else parse_list pbs' tps
>  where this_parse = p tps

-------------------

> p_modulehead :: Parser Token ModuleHead
> p_modulehead = p_modulehead_exists
>            <|  p_modulehead_not_exists
> p_modulehead_exists :: Parser Token ModuleHead
> p_modulehead_exists = ModuleHead_with_id
>                   <$  pSym (ReservedID "module")
>                   <*> p_conid
>                   <*> pMaybe p_exports
>                   <*  pSym (ReservedID "where")
>                   <*> p_curly_open_any
> p_modulehead_not_exists :: Parser Token ModuleHead
> p_modulehead_not_exists = ModuleHead_no_id <$> p_curly_open_any

> p_moduletail :: Parser Token ModuleTail
> p_moduletail = ModuleTail <$> p_curly_close_any

-------------------

> p_impdecls :: Parser Token Impdecls
> p_impdecls = Impdecls <$> pSList False 1 p_semicolon p_impdecl

> p_exports :: Parser Token Exports
> p_exports = (\xs -> Exports (get_rights xs) (last_is_left xs))
>         <$  pSym (Special '(')
>         <*> pSList True 0 p_comma p_export
>         <*  pSym (Special ')')

> p_export :: Parser Token Export
> p_export = p_export_qvar <|  p_export_qtycon_qtycls <|  p_export_module
> p_export_qvar :: Parser Token Export
> p_export_qvar = Export_qvar <$> p_qvar
> p_export_qtycon_qtycls :: Parser Token Export
> p_export_qtycon_qtycls = Export_qtycon_qtycls
>                      <$> p_qconid
>                      <*> pMaybe (    id
>                                  <$  pSym (Special '(')
>                                  <*> (    (    Just
>                                            <$> pEither (get_rights <$> pSList False 0 p_comma p_con)
>                                                        (get_rights <$> pSList False 1 p_comma p_qvar)
>                                           )
>                                       <|> (    Nothing
>                                            <$  pSym (ReservedOp "..")
>                                           )
>                                      )
>                                  <*  pSym (Special ')')
>                                 )
> p_export_module :: Parser Token Export
> p_export_module = Export_module
>               <$  pSym (ReservedID "module")
>               <*> p_conid

> p_impdecl :: Parser Token Impdecl
> p_impdecl = p_impdecl_full <|  p_impdecl_empty
> p_impdecl_full :: Parser Token Impdecl
> p_impdecl_full = Impdecl
>              <$  pSym (ReservedID "import")
>              <*> pPredHolds (is_VarID_name "qualified")
>              <*> p_conid
>              <*> pMaybe (    id
>                          <$  pPred (is_VarID_name "as")
>                          <*> p_conid
>                         )
>              <*> pMaybe p_impspec
> p_impdecl_empty :: Parser Token Impdecl
> p_impdecl_empty = pSucceed Impdecl_empty

> p_impspec :: Parser Token Impspec
> p_impspec = (\h xs -> Impspec h (get_rights xs) (last_is_left xs))
>         <$> pPredHolds (is_VarID_name "hiding")
>         <*  pSym (Special '(')
>         <*> pSList True 0 p_comma p_import
>         <*  pSym (Special ')')

> p_import :: Parser Token Import
> p_import = p_import_var <|  p_import_tycon_tycls
> p_import_var :: Parser Token Import
> p_import_var = Import_var <$> p_var
> p_import_tycon_tycls :: Parser Token Import
> p_import_tycon_tycls = Import_tycon_tycls
>                    <$> p_conid
>                    <*> pMaybe (    id
>                                <$  pSym (Special '(')
>                                <*> (    (    Just
>                                          <$> pEither (get_rights <$> pSList False 1 p_comma p_con)
>                                                      (get_rights <$> pSList False 0 p_comma p_var)
>                                         )
>                                     <|> (    Nothing
>                                          <$  pSym (ReservedOp "..")
>                                         )
>                                    )
>                                <*  pSym (Special ')')
>                               )

> p_topdecls :: Parser Token Topdecls
> p_topdecls = Topdecls <$> pSList False 0 p_semicolon p_topdecl

> p_topdecl :: Parser Token Topdecl
> p_topdecl = p_topdecl_type <|  p_topdecl_data <|  p_topdecl_newtype
>         <|  p_topdecl_class <|  p_topdecl_instance
>         <|  p_topdecl_default <|  p_topdecl_decl
> p_topdecl_type :: Parser Token Topdecl
> p_topdecl_type = Topdecl_type
>              <$  pSym (ReservedID "type")
>              <*> p_simpletype
>              <*  pSym (ReservedOp "=")
>              <*> p_type
> p_topdecl_data :: Parser Token Topdecl
> p_topdecl_data = Topdecl_data
>              <$  pSym (ReservedID "data")
>              <*> pMaybe (    id
>                          <$> p_context
>                          <*  pSym (ReservedOp "=>")
>                         )
>              <*> p_simpletype
>              <*  pSym (ReservedOp "=")
>              <*> p_constrs
>              <*> pMaybe p_deriving
> p_topdecl_newtype :: Parser Token Topdecl
> p_topdecl_newtype = Topdecl_newtype
>              <$  pSym (ReservedID "newtype")
>              <*> pMaybe (    id
>                          <$> p_context
>                          <*  pSym (ReservedOp "=>")
>                         )
>              <*> p_simpletype
>              <*  pSym (ReservedOp "=")
>              <*> p_newconstr
>              <*> pMaybe p_deriving
> p_topdecl_class :: Parser Token Topdecl
> p_topdecl_class = Topdecl_class
>              <$  pSym (ReservedID "class")
>              <*> pMaybe (    id
>                          <$> p_scontext
>                          <*  pSym (ReservedOp "=>")
>                         )
>              <*> p_tycls
>              <*> p_tyvar
>              <*> pMaybe (    id
>                          <$  pSym (ReservedID "where")
>                          <*> p_cdecls
>                         )
> p_topdecl_instance :: Parser Token Topdecl
> p_topdecl_instance = Topdecl_instance
>              <$  pSym (ReservedID "instance")
>              <*> pMaybe (    id
>                          <$> p_scontext
>                          <*  pSym (ReservedOp "=>")
>                         )
>              <*> p_qtycls
>              <*> p_inst
>              <*> pMaybe (    id
>                          <$  pSym (ReservedID "where")
>                          <*> p_idecls
>                         )
> p_topdecl_default :: Parser Token Topdecl
> p_topdecl_default = (Topdecl_default . get_rights)
>                 <$  pSym (ReservedID "default")
>                 <*  pSym (Special '(')
>                 <*> pSList False 0 p_comma p_type
>                 <*  pSym (Special ')')
> p_topdecl_decl :: Parser Token Topdecl
> p_topdecl_decl = Topdecl_decl <$> p_decl

> p_decls :: Parser Token Decls
> p_decls = uncurry Decls
>       <$> p_in_curlies (pSList False 0 p_semicolon p_decl)

> p_decl :: Parser Token Decl
> p_decl = (Decl_gendecl <$> p_gendecl)
>      <|> (Decl_fun <$> p_funlhs <*> p_rhs)
>      <|> (Decl_pat0 <$> p_pat_i 0 <*> p_rhs)

> p_cdecls :: Parser Token Cdecls
> p_cdecls = uncurry Cdecls
>        <$> p_in_curlies (pSList False 0 p_semicolon p_cdecl)

> p_cdecl :: Parser Token Cdecl
> p_cdecl = (Cdecl_gendecl <$> p_gendecl)
>       <|> (Cdecl_fun <$> p_funlhs <*> p_rhs)
>       <|> (Cdecl_var <$> p_var <*> p_rhs)

> p_idecls :: Parser Token Idecls
> p_idecls = uncurry Idecls
>        <$> p_in_curlies (pSList False 0 p_semicolon p_idecl)

> p_idecl :: Parser Token Idecl
> p_idecl = (Idecl_fun <$> p_funlhs <*> p_rhs)
>       <|> (Idecl_var <$> p_var <*> p_rhs)
>       <|> pSucceed Idecl_empty

> p_gendecl :: Parser Token Gendecl
> p_gendecl = p_gendecl_type <|  p_gendecl_fixity <|  p_gendecl_empty
> p_gendecl_type :: Parser Token Gendecl
> p_gendecl_type = Gendecl_type
>              <$> p_vars
>              <*  pSym (ReservedOp "::")
>              <*> pMaybe (    id
>                          <$> p_context
>                          <*  pSym (ReservedOp "=>")
>                         )
>              <*> p_type
> p_gendecl_fixity :: Parser Token Gendecl
> p_gendecl_fixity = Gendecl_fixity
>                <$> p_fixity
>                <*> pMaybe p_integer
>                <*> p_ops
> p_gendecl_empty :: Parser Token Gendecl
> p_gendecl_empty = pSucceed Gendecl_empty

> p_ops :: Parser Token Ops
> p_ops = (Ops . get_rights) <$> pSList False 1 p_comma p_op

> p_vars :: Parser Token Vars
> p_vars = (Vars . get_rights) <$> pSList False 1 p_comma p_var

> p_fixity :: Parser Token Fixity
> p_fixity = Fixity_infixl <$  pSym (ReservedID "infixl")
>        <|  Fixity_infixr <$  pSym (ReservedID "infixr")
>        <|  Fixity_infix  <$  pSym (ReservedID "infix")

> p_type :: Parser Token Type
> p_type = Type
>      <$> p_btype
>      <*> pMaybe (    id
>                  <$  pSym (ReservedOp "->")
>                  <*> p_type
>                 )

> p_btype :: Parser Token Btype
> p_btype = Btype <$> pList 1 p_atype

> p_atype :: Parser Token Atype
> p_atype = p_atype_gtycon <|> p_atype_tyvar {- <|> p_atype_tuple -}
>       <|> p_atype_list {- <|> p_atype_paren -} <|> p_atype_tuple_paren
> p_atype_gtycon :: Parser Token Atype
> p_atype_gtycon = Atype_gtycon <$> p_gtycon
> p_atype_tyvar :: Parser Token Atype
> p_atype_tyvar = Atype_tyvar <$> p_tyvar
> {-
> p_atype_tuple :: Parser Token Atype
> p_atype_tuple = (Atype_tuple . get_rights)
>             <$  pSym (Special '(')
>             <*> pSList False 2 p_comma p_type
>             <*  pSym (Special ')')
> -}
> p_atype_list :: Parser Token Atype
> p_atype_list = Atype_list
>            <$  pSym (Special '[')
>            <*> p_type
>            <*  pSym (Special ']')
> {-
> p_atype_paren :: Parser Token Atype
> p_atype_paren = Atype_paren
>             <$  pSym (Special '(')
>             <*> p_type
>             <*  pSym (Special ')')
> -}
> p_atype_tuple_paren :: Parser Token Atype
> p_atype_tuple_paren = (\tl -> case tl of
>                                   [Right t] -> Atype_paren t
>                                   _ -> Atype_tuple (get_rights tl)
>                       )
>                   <$  pSym (Special '(')
>                   <*> pSList False 1 p_comma p_type
>                   <*  pSym (Special ')')

> p_gtycon :: Parser Token Gtycon
> p_gtycon = p_gtycon_qtycon <|> p_gtycon_unit <|> p_gtycon_list
>        <|> p_gtycon_function <|> p_gtycon_tuple
> p_gtycon_qtycon :: Parser Token Gtycon
> p_gtycon_qtycon = Gtycon_qtycon <$> p_qtycon
> p_gtycon_unit :: Parser Token Gtycon
> p_gtycon_unit = Gtycon_unit
>             <$  pSym (Special '(')
>             <*  pSym (Special ')')
> p_gtycon_list :: Parser Token Gtycon
> p_gtycon_list = Gtycon_list
>             <$  pSym (Special '[')
>             <*  pSym (Special ']')
> p_gtycon_function :: Parser Token Gtycon
> p_gtycon_function = Gtycon_function
>                 <$  pSym (Special '(')
>                 <*  pSym (ReservedOp "->")
>                 <*  pSym (Special ')')
> p_gtycon_tuple :: Parser Token Gtycon
> p_gtycon_tuple = (Gtycon_tupling . length)
>              <$  pSym (Special '(')
>              <*> pList 1 p_comma
>              <*  pSym (Special ')')

> p_context :: Parser Token Context
> p_context = (Context <$> p_class)
>         <|  (    (Context_list . get_rights)
>              <$  pSym (Special '(')
>              <*> pSList False 0 p_comma p_class
>              <*  pSym (Special ')')
>             )

> p_class :: Parser Token Class
> p_class = (Class <$> p_qtycls <*> p_tyvar)
>       <|  (    Class_list
>            <$> p_qtycls
>            <*  pSym (Special '(')
>            <*> p_tyvar
>            <*> pList 1 p_atype
>            <*  pSym (Special ')')
>           )

> p_scontext :: Parser Token Scontext
> p_scontext = (Scontext <$> p_simpleclass)
>          <|  (    (Scontext_list . get_rights)
>               <$  pSym (Special '(')
>               <*> pSList False 0 p_comma p_simpleclass
>               <*  pSym (Special ')')
>              )

> p_simpleclass :: Parser Token Simpleclass
> p_simpleclass = Simpleclass <$> p_qtycls <*> p_tyvar

> p_simpletype :: Parser Token Simpletype
> p_simpletype = Simpletype <$> p_tycon <*> pList 0 p_tyvar

> p_constrs :: Parser Token Constrs
> p_constrs = (Constrs . get_rights)
>         <$> pSList False 1 (pSym (ReservedOp "|")) p_constr

> p_constr :: Parser Token Constr
> p_constr = p_constr_atype <|> p_constr_infix <|> p_constr_field
> p_constr_atype :: Parser Token Constr
> p_constr_atype = Constr_atype
>              <$> p_con
>              <*> pList 0 (    (,)
>                           <$> pPredHolds is_bang
>                           <*> p_atype
>                          )
> p_constr_infix :: Parser Token Constr
> p_constr_infix = Constr_infix
>              <$> (    (Left <$> p_btype)
>                   <|  (    Right
>                        <$  pPred is_bang
>                        <*> p_atype
>                       )
>                  )
>              <*> p_conop
>              <*> (    (Left <$> p_btype)
>                   <|  (    Right
>                        <$  pPred is_bang
>                        <*> p_atype
>                       )
>                  )
> p_constr_field :: Parser Token Constr
> p_constr_field = Constr_field
>              <$> p_con
>              <*  p_curly_open
>              <*> (get_rights <$> pSList False 0 p_comma p_fielddecl)
>              <*  p_curly_close

> p_newconstr :: Parser Token Newconstr
> p_newconstr = (Newconstr_atype <$> p_con <*> p_atype)
>           <|  (    Newconstr_var_type
>                <$> p_con
>                <*  p_curly_open
>                <*> p_var
>                <*  pSym (ReservedOp "::")
>                <*> p_type
>                <*  p_curly_close
>               )

> p_fielddecl :: Parser Token Fielddecl
> p_fielddecl = Fielddecl
>           <$> p_vars
>           <*  pSym (ReservedOp "::")
>           <*> (    (Left <$> p_type)
>                <|  (    Right
>                     <$  pPred is_bang
>                     <*> p_atype
>                    )
>               )

> p_deriving :: Parser Token Deriving
> p_deriving = (    Deriving
>               <$  pSym (ReservedID "deriving")
>               <*> p_dclass
>              )
>          <|  (    (Deriving_list . get_rights)
>               <$  pSym (ReservedID "deriving")
>               <*  pSym (Special '(')
>               <*> pSList False 0 p_comma p_dclass
>               <*  pSym (Special ')')
>              )

> p_dclass :: Parser Token Dclass
> p_dclass = Dclass <$> p_qtycls

> p_inst :: Parser Token Inst
> p_inst = p_inst_gtycon <|> p_inst_gtycon_concr_list <|> p_inst_gtycon_list
>      <|> p_inst_tuple <|> p_inst_list <|> p_inst_function
> p_inst_gtycon :: Parser Token Inst
> p_inst_gtycon = Inst_gtycon <$> p_gtycon
> p_inst_gtycon_concr_list :: Parser Token Inst
> p_inst_gtycon_concr_list = Inst_gtycon_concr_list
> 		   <$  pSym (Special '(')
> 		   <*> p_gtycon
>                  <*> pList 1 p_gtycon
>                  <*  pSym (Special ')')
> p_inst_gtycon_list :: Parser Token Inst
> p_inst_gtycon_list = Inst_gtycon_list
>                  <$  pSym (Special '(')
>                  <*> p_gtycon
>                  <*> pList 0 p_tyvar
>                  <*  pSym (Special ')')
> p_inst_tuple :: Parser Token Inst
> p_inst_tuple = (Inst_tuple . get_rights)
>            <$  pSym (Special '(')
>            <*> pSList False 2 p_comma p_tyvar
>            <*  pSym (Special ')')
> p_inst_list :: Parser Token Inst
> p_inst_list = Inst_list
>           <$  pSym (Special '[')
>           <*> p_tyvar
>           <*  pSym (Special ']')
> p_inst_function :: Parser Token Inst
> p_inst_function = Inst_function
>               <$  pSym (Special '(')
>               <*> p_tyvar
>               <*  pSym (ReservedOp "->")
>               <*> p_tyvar
>               <*  pSym (Special ')')

> p_funlhs :: Parser Token Funlhs
> p_funlhs = p_funlhs_var <|> p_funlhs_pat <|> p_funlhs_bracket
> p_funlhs_var :: Parser Token Funlhs
> p_funlhs_var = Funlhs_var
>            <$> p_var
>            <*> pList 1 p_apat
> -- This can easily create incorrect parse trees - in particular note
> -- the absense of l- and r-patterns and the precedence checking.
> -- A checking pass should make sure this is OK.
> p_funlhs_pat :: Parser Token Funlhs
> p_funlhs_pat = Funlhs_pat
>            <$> p_pat_i 0
>            <*> p_varop
>            <*> p_pat_i 0
> p_funlhs_bracket :: Parser Token Funlhs
> p_funlhs_bracket = Funlhs_apats
>                <$  pSym (Special '(')
>                <*> p_funlhs
>                <*  pSym (Special ')')
>                <*> pList 1 p_apat

> p_rhs :: Parser Token Rhs
> p_rhs = p_rhs_exp <|  p_rhs_gdrhs
> p_rhs_exp :: Parser Token Rhs
> p_rhs_exp = Rhs_exp
>         <$  pSym (ReservedOp "=")
>         <*> p_exp
>         <*> pMaybe (    id
>                     <$  pSym (ReservedID "where")
>                     <*> p_decls
>                    )
> p_rhs_gdrhs :: Parser Token Rhs
> p_rhs_gdrhs = Rhs_gdrhs
>           <$> p_gdrhs
>           <*> pMaybe (    id
>                       <$  pSym (ReservedID "where")
>                       <*> p_decls
>                      )

> p_gdrhs :: Parser Token Gdrhs
> p_gdrhs = Gdrhs
>       <$> p_gd
>       <*  pSym (ReservedOp "=")
>       <*> p_exp
>       <*> pMaybe p_gdrhs

> p_gd :: Parser Token Gd
> p_gd = Gd <$  pSym (ReservedOp "|") <*> p_exp_i 0

> p_exp :: Parser Token Exp
> {-
> p_exp = (    Exp_sig
>          <$> p_exp_i 0
>          <*  pSym (ReservedOp "::")
>          <*> pMaybe (    id
>                      <$> p_context
>                      <*  pSym (ReservedOp "=>")
>                     )
>          <*> p_type
>         )
>     <|  (Exp <$> p_exp_i 0)
> -}
> p_exp = (\ei m -> case m of
>                       Nothing -> Exp ei
>                       Just (mc, t) -> Exp_sig ei mc t
>         )
>     <$> p_exp_i 0
>     <*> pMaybe (    (,)
>                 <$  pSym (ReservedOp "::")
>                 <*> pMaybe (    id
>                             <$> p_context
>                             <*  pSym (ReservedOp "=>")
>                            )
>                 <*> p_type
>                )

> p_exp_from_exp_i :: Parser Token (Exp_i -> Exp)
> p_exp_from_exp_i = (    (\m t e -> Exp_sig e m t)
>                     <$  pSym (ReservedOp "::")
>                     <*> pMaybe (    id
>                                 <$> p_context
>                                 <*  pSym (ReservedOp "=>")
>                                )
>                     <*> p_type
>                    )
>                <|  pSucceed Exp

> p_exp_i :: Int -> Parser Token Exp_i
> p_exp_i 10 = Exp_i_10 <$> p_exp_10
> p_exp_i 6 = p_exp_i_6_work <|> p_exp_i_work 6
> p_exp_i i = p_exp_i_work i
> p_exp_i_6_work :: Parser Token Exp_i
> p_exp_i_6_work = (\lexp xs -> Exp_i_lexp $ merge_lexp lexp xs)
>              <$> (    Lexp_i_neg
>                   <$  pPred is_negative
>                   <*> p_exp_i 7
>                  )
>              <*> pList 0 (p_lexp_i_e 6)
> p_exp_i_work :: Int -> Parser Token Exp_i
> p_exp_i_work i = flip ($)
>              <$> p_exp_i (i + 1)
>              <*> (p_exp_i_e i <|> p_lexp_i i <|> p_rexp_i i)
> p_exp_i_e :: Int -> Parser Token (Exp_i -> Exp_i)
> p_exp_i_e i = pSucceed (\e -> Exp_i_exp e Nothing)
>            |> (    (\qop e2 e1 -> Exp_i_exp e1 (Just (qop, e2)))
>                <$> p_qop_infix (InfixN i)
>                <*> p_exp_i (i + 1)
>               )

> p_lexp_i :: Int -> Parser Token (Exp_i -> Exp_i)
> p_lexp_i i = (\(op, exp2) xs exp1 ->
>                   Exp_i_lexp $ merge_lexp (Lexp_i_e exp1 op exp2) xs
>              )
>          <$> p_lexp_i_e i
>          <*> pList 0 (p_lexp_i_e i)
> merge_lexp :: Lexp_i -> [(Qop, Exp_i)] -> Lexp_i
> merge_lexp = foldl (\lexp (op, e) -> Lexp_i_l lexp op e)
> p_lexp_i_e :: Int -> Parser Token (Qop, Exp_i)
> p_lexp_i_e i = (,)
>            <$> p_qop_infix (InfixL i)
>            <*> p_exp_i (i + 1)

> p_rexp_i :: Int -> Parser Token (Exp_i -> Exp_i)
> p_rexp_i i = (\rs e1 -> Exp_i_rexp (merge_rexp e1 rs))
>          <$> pList 1 ((,) <$> p_qop_infix (InfixR i) <*> p_exp_i (i + 1))
> merge_rexp :: Exp_i -> [(Qop, Exp_i)] -> Rexp_i
> merge_rexp _ [] = error "merge_rexp: Can't happen: _ []"
> merge_rexp e1 [(op, e2)] = Rexp_i_e e1 op e2
> merge_rexp e1 ((op, e2):rest) = Rexp_i_r e1 op (merge_rexp e2 rest)

> p_exp_10 :: Parser Token Exp_10
> p_exp_10 = p_exp_10_lambda <|  p_exp_10_let <|  p_exp_10_if
>        <|  p_exp_10_case <|  p_exp_10_do <|  p_exp_10_fexp
> p_exp_10_lambda :: Parser Token Exp_10
> p_exp_10_lambda = Exp_10_lambda
>               <$  pSym (ReservedOp "\\")
>               <*> pList 1 p_apat
>               <*  pSym (ReservedOp "->")
>               <*> p_exp
> p_exp_10_let :: Parser Token Exp_10
> p_exp_10_let = Exp_10_let
>            <$  pSym (ReservedID "let")
>            <*> p_decls
>            <*  pSym (ReservedID "in")
>            <*> p_exp
> p_exp_10_if :: Parser Token Exp_10
> p_exp_10_if = Exp_10_if
>           <$  pSym (ReservedID "if")
>           <*> p_exp
>           <*  pSym (ReservedID "then")
>           <*> p_exp
>           <*  pSym (ReservedID "else")
>           <*> p_exp
> p_exp_10_case :: Parser Token Exp_10
> p_exp_10_case = (\e (b, alts) -> Exp_10_case e b alts)
>           <$  pSym (ReservedID "case")
>           <*> p_exp
>           <*  pSym (ReservedID "of")
>           <*> p_in_curlies p_alts
> p_exp_10_do :: Parser Token Exp_10
> p_exp_10_do = uncurry Exp_10_do
>           <$  pSym (ReservedID "do")
>           <*> p_in_curlies p_stmts
> p_exp_10_fexp :: Parser Token Exp_10
> p_exp_10_fexp = Exp_10_fexp <$> p_fexp

> p_fexp :: Parser Token Fexp
> p_fexp = Fexp <$> pList 1 p_aexp

> -- Mangled
> p_aexp :: Parser Token Aexp
> p_aexp = p_aexp_qvar <|> p_aexp_gcon <|> p_aexp_literal
>         {- <|> p_aexp_paren <|> p_aexp_tuple -} <|> p_aexp_paren_tuple_left
>         {- <|> p_aexp_list <|> p_aexp_arith <|> p_aexp_listcomp -}
>         <|> p_aexp_list_arith_listcomp
>         {- <|> p_aexp_left -} <|> p_aexp_right <|> p_aexp_qcon
> p_aexp_qvar :: Parser Token Aexp
> p_aexp_qvar = (\a m_u -> case m_u of
>                              Nothing -> a
>                              Just u -> Aexp_update a u)
>           <$> (Aexp_qvar <$> p_qvar)
>           <*> pMaybe p_aexp_update
> p_aexp_gcon :: Parser Token Aexp
> p_aexp_gcon = Aexp_gcon <$> p_gcon
> p_aexp_literal :: Parser Token Aexp
> p_aexp_literal = Aexp_literal <$> p_literal
> {-
> p_aexp_paren :: Parser Token Aexp
> p_aexp_paren = Aexp_paren
>            <$  pSym (Special '(')
>            <*> p_exp
>            <*  pSym (Special ')')
> p_aexp_tuple :: Parser Token Aexp
> p_aexp_tuple = (Aexp_tuple . get_rights)
>            <$  pSym (Special '(')
>            <*> pSList False 2 p_comma p_exp
>            <*  pSym (Special ')')
> -}
> p_aexp_paren_tuple_left :: Parser Token Aexp
> p_aexp_paren_tuple_left = flip ($)
>                       <$  pSym (Special '(')
>                       <*> p_exp_i 0
>                       <*> (    (    (\efei f ei -> f (efei ei))
>                                 <$> p_exp_from_exp_i
>                                 <*> (    pSucceed Aexp_paren
>                                      <|> (    (\el e -> Aexp_tuple (e:get_rights el))
>                                           <$  p_comma
>                                           <*> pSList False 1 p_comma p_exp
>                                          )
>                                     )
>                                )
>                            <|> (    flip Aexp_left
>                                 <$> p_qop
>                                )
>                           )
>                       <*  pSym (Special ')')
> {-
> p_aexp_list :: Parser Token Aexp
> p_aexp_list = (Aexp_list . get_rights)
>           <$  pSym (Special '[')
>           <*> pSList False 1 p_comma p_exp
>           <*  pSym (Special ']')
> p_aexp_arith :: Parser Token Aexp
> p_aexp_arith = Aexp_arith
>            <$  pSym (Special '[')
>            <*> p_exp
>            <*> pMaybe (    id
>                        <$  pSym (Special ',')
>                        <*> p_exp
>                       )
>            <*  pSym (ReservedOp "..")
>            <*> pMaybe p_exp
>            <*  pSym (Special ']')
> p_aexp_listcomp :: Parser Token Aexp
> p_aexp_listcomp = (\exp list -> Aexp_listcomp exp (get_rights list))
>               <$  pSym (Special '[')
>               <*> p_exp
>               <*  pSym (ReservedOp "|")
>               <*> pSList False 0 p_comma p_qual
>               <*  pSym (Special ']')
> -}
> p_aexp_list_arith_listcomp :: Parser Token Aexp
> p_aexp_list_arith_listcomp = flip ($)
>                          <$  pSym (Special '[')
>                          <*> p_exp
>                          <*> (    p_aexp_list_end1
>                               <|> p_aexp_arith_end1
>                               <|> p_aexp_listcomp_end
>                               <|> (    flip ($)
>                                    <$  p_comma
>                                    <*> p_exp
>                                    <*> (    p_aexp_arith_end2
>                                         <|> p_aexp_list_end2
>                                        )
>                                   )
>                              )
>                          <*  pSym (Special ']')
> p_aexp_list_end1 :: Parser Token (Exp -> Aexp)
> p_aexp_list_end1 = pSucceed (\e -> Aexp_list [e])
> p_aexp_list_end2 :: Parser Token (Exp -> Exp -> Aexp)
> p_aexp_list_end2 = (\end second first -> Aexp_list (first:second:end))
>                <$> (    (    get_rights
>                          <$  p_comma
>                          <*> pSList False 1 p_comma p_exp
>                         )
>                     <|> pSucceed []
>                    )
> p_aexp_arith_end1 :: Parser Token (Exp -> Aexp)
> p_aexp_arith_end1 = (\end first -> Aexp_arith first Nothing end)
>                 <$  pSym (ReservedOp "..")
>                 <*> pMaybe p_exp
> p_aexp_arith_end2 :: Parser Token (Exp -> Exp -> Aexp)
> p_aexp_arith_end2 = (\end second first -> Aexp_arith first (Just second) end)
>                 <$  pSym (ReservedOp "..")
>                 <*> pMaybe p_exp
> p_aexp_listcomp_end :: Parser Token (Exp -> Aexp)
> p_aexp_listcomp_end = (\list e -> Aexp_listcomp e (get_rights list))
>                   <$  pSym (ReservedOp "|")
>                   <*> pSList False 1 p_comma p_qual
> {-
> p_aexp_left :: Parser Token Aexp
> p_aexp_left = Aexp_left
>           <$  pSym (Special '(')
>           <*> p_exp_i 0
>           <*> p_qop
>           <*  pSym (Special ')')
> -}
> -- XXX Note we can create invalid trees here which the next pass
> -- should notice
> p_aexp_right :: Parser Token Aexp
> p_aexp_right = Aexp_right
>            <$  pSym (Special '(')
>            <*> (p_qop <!> pPred is_minus)
>            <*> p_exp_i 0
>            <*  pSym (Special ')')
>   where is_minus :: Token -> Bool
>         is_minus (QVarSym _ Nothing "-") = True
>         is_minus _ = False
> p_aexp_qcon :: Parser Token Aexp
> p_aexp_qcon = (\a m_u -> case m_u of
>                              Nothing -> a
>                              Just u -> Aexp_update a u)
>           <$> (    Aexp_qcon
>                <$> p_qcon
>                <*  p_curly_open
>                <*> (get_rights <$> pSList False 0 p_comma p_fbind)
>                <*  p_curly_close
>               )
>           <*> pMaybe p_aexp_update
> p_aexp_update :: Parser Token [[Fbind]]
> p_aexp_update = pList 1 p_aexp_update_segment
> p_aexp_update_segment :: Parser Token [Fbind]
> p_aexp_update_segment = id
>                     <$  p_curly_open
>                     <*> pList 1 p_fbind
>                     <*  p_curly_close

> p_qual :: Parser Token Qual
> p_qual = p_qual_generator <|> p_qual_let <|> p_qual_exp
> p_qual_generator :: Parser Token Qual
> p_qual_generator = Qual_generator
>                <$> p_pat
>                <*  pSym (ReservedOp "<-")
>                <*> p_exp
> p_qual_let :: Parser Token Qual
> p_qual_let = Qual_let
>          <$  pSym (ReservedID "let")
>          <*> p_decls
> p_qual_exp :: Parser Token Qual
> p_qual_exp = Qual_exp <$> p_exp

> p_alts :: Parser Token Alts
> p_alts = Alts <$> pSList False 0 p_semicolon p_alt

> p_alt :: Parser Token Alt
> p_alt = p_alt_exp <|  p_alt_gdpat <|  p_alt_empty
> p_alt_exp :: Parser Token Alt
> p_alt_exp = Alt_exp
>         <$> p_pat
>         <*  pSym (ReservedOp "->")
>         <*> p_exp
>         <*> pMaybe (    id
>                     <$  pSym (ReservedID "where")
>                     <*> p_decls
>                    )
> p_alt_gdpat :: Parser Token Alt
> p_alt_gdpat = Alt_gdpat
>           <$> p_pat
>           <*> p_gdpat
>           <*> pMaybe (    id
>                       <$  pSym (ReservedID "where")
>                       <*> p_decls
>                      )
> p_alt_empty :: Parser Token Alt
> p_alt_empty = pSucceed Alt_empty

> p_gdpat :: Parser Token Gdpat
> p_gdpat = Gdpat
>       <$> p_gd
>       <*  pSym (ReservedOp "->")
>       <*> p_exp
>       <*> pMaybe p_gdpat

XXX inline pSListEnd and then check stmt, stmts

> p_stmts :: Parser Token Stmts
> p_stmts = (\(ss, e, mb) -> Stmts ss e mb)
>       <$> pSListEnd p_stmt p_exp p_semicolon Stmt_exp

> p_stmt :: Parser Token (Bool -> Stmt)
> p_stmt = {- p_stmt_exp <|> -} p_stmt_pat <|> p_stmt_let <|> p_stmt_empty
> p_stmt_exp :: Parser Token (Bool -> Stmt)
> p_stmt_exp = Stmt_exp
>          <$> p_exp
> p_stmt_pat :: Parser Token (Bool -> Stmt)
> p_stmt_pat = Stmt_pat
>          <$> p_pat
>          <*  pSym (ReservedOp "<-")
>          <*> p_exp
> p_stmt_let :: Parser Token (Bool -> Stmt)
> p_stmt_let = Stmt_let
>          <$  pSym (ReservedID "let")
>          <*> p_decls
> p_stmt_empty :: Parser Token (Bool -> Stmt)
> p_stmt_empty = pSucceed Stmt_empty

> p_fbind :: Parser Token Fbind
> p_fbind = Fbind
>       <$> p_qvar
>       <*  pSym (ReservedOp "=")
>       <*> p_exp

> p_pat :: Parser Token Pat
> p_pat = (    Pat_succ
>          <$> p_var
>          <*  pPred is_plus
>          <*> p_integer
>         )
>     <|> (Pat <$> p_pat_i 0)

> -- Check me (compare to exp)
> p_pat_i :: Int -> Parser Token Pat_i
> p_pat_i 10 = Pat_i_10 <$> p_pat_10
> p_pat_i 6 = p_pat_i_6_integer <|> p_pat_i_6_float <|> p_pat_i_work 6
> p_pat_i i = p_pat_i_work i
> p_pat_i_6_integer :: Parser Token Pat_i
> p_pat_i_6_integer = (Pat_i_lpat . Lpat_i_6_integer)
>                 <$  pPred is_negative
>                 <*> p_integer
> p_pat_i_6_float :: Parser Token Pat_i
> p_pat_i_6_float = (Pat_i_lpat . Lpat_i_6_float)
>               <$  pPred is_negative
>               <*> p_float
> p_pat_i_work :: Int -> Parser Token Pat_i
> p_pat_i_work i = flip ($)
>              <$> p_pat_i (i + 1)
>              <*> (    p_pat_i_p i
>                   <|> ((\a b -> Pat_i_lpat (a b)) <$> p_lpat_i i)
>                   <|> ((\a b -> Pat_i_rpat (a b)) <$> p_rpat_i i)
>                  )
> p_pat_i_p :: Int -> Parser Token (Pat_i -> Pat_i)
> p_pat_i_p i = pSucceed (\p -> Pat_i_pat p Nothing)
>            |> (    (\qconop p2 p1 -> Pat_i_pat p1 (Just (qconop, p2)))
>                <$> p_qconop_infix (InfixN i)
>                <*> p_pat_i (i + 1)
>               )

> -- Check me
> p_lpat_i :: Int -> Parser Token (Pat_i -> Lpat_i)
> p_lpat_i i = (\(op, pat2) xs pat1 -> merge_lpat (Lpat_i_p pat1 op pat2) xs)
>          <$> p_lpat_i_p i
>          <*> pList 0 (p_lpat_i_p i) -- Check me, and pList 1 again?
> merge_lpat :: Lpat_i -> [(Qconop, Pat_i)] -> Lpat_i
> merge_lpat = foldl (\lpat (op, pat) -> Lpat_i_l lpat op pat)
> p_lpat_i_p :: Int -> Parser Token (Qconop, Pat_i)
> p_lpat_i_p i = (,)
>            <$> p_qconop_infix (InfixL i)
>            <*> p_pat_i (i + 1)

> -- Check me
> -- Fix up nicer
> p_rpat_i :: Int -> Parser Token (Pat_i -> Rpat_i)
> p_rpat_i i = (\op pat2 rest pat1 -> bar_pat pat1 ((op, pat2):rest))
>          <$> p_qconop_infix (InfixR i)
>          <*> p_pat_i (i + 1)
>          <*> pList 0 ((,) <$> p_qconop_infix (InfixR i) <*> p_pat_i (i + 1))
> bar_pat :: Pat_i -> [(Qconop, Pat_i)] -> Rpat_i
> bar_pat _ [] = undefined
> bar_pat p1 [(op, p2)] = Rpat_i_p p1 op p2
> bar_pat p1 ((op, p2):b) = Rpat_i_r p1 op (bar_pat p2 b)

> p_pat_10 :: Parser Token Pat_10
> p_pat_10 = (Pat_10_apat <$> p_apat)
>        <|> (Pat_10_gcon <$> p_gcon <*> pList 1 p_apat)

> p_apat :: Parser Token Apat
> p_apat = p_apat_var <|> p_apat_gcon <|> p_apat_qcon <|> p_apat_literal
>      <|> p_apat_wildcard
>      {- <|> p_apat_paren <|> p_apat_tuple -} <|> p_apat_paren_tuple
>      <|> p_apat_list <|> p_apat_irrefutable
> p_apat_var :: Parser Token Apat
> p_apat_var = Apat_var
>          <$> p_var
>          <*> pMaybe (    id
>                      <$  pSym (ReservedOp "@")
>                      <*> p_apat
>                     )
> p_apat_gcon :: Parser Token Apat
> p_apat_gcon = Apat_gcon <$> p_gcon
> p_apat_qcon :: Parser Token Apat
> p_apat_qcon = Apat_qcon
>           <$> p_qcon
>           <*  p_curly_open
>           <*> (get_rights <$> pSList False 0 p_comma p_fpat)
>           <*  p_curly_close
> p_apat_literal :: Parser Token Apat
> p_apat_literal = Apat_literal <$> p_literal
> p_apat_wildcard :: Parser Token Apat
> p_apat_wildcard = Apat_wildcard <$  pSym (ReservedID "_")
> {-
> p_apat_paren :: Parser Token Apat
> p_apat_paren = Apat_paren
>            <$  pSym (Special '(')
>            <*> p_pat
>            <*  pSym (Special ')')
> p_apat_tuple :: Parser Token Apat
> p_apat_tuple = (Apat_tuple . get_rights)
>            <$  pSym (Special '(')
>            <*> pSList False 2 p_comma p_pat
>            <*  pSym (Special ')')
> -}
> p_apat_paren_tuple :: Parser Token Apat
> p_apat_paren_tuple = (\pl -> case pl of
>                                  [p] -> Apat_paren p
>                                  _ -> Apat_tuple pl
>                      )
>                  <$  pSym (Special '(')
>                  <*> (get_rights <$> pSList False 1 p_comma p_pat)
>                  <*  pSym (Special ')')
> p_apat_list :: Parser Token Apat
> p_apat_list = (Apat_list . get_rights)
>           <$  pSym (Special '[')
>           <*> pSList False 1 p_comma p_pat
>           <*  pSym (Special ']')
> p_apat_irrefutable :: Parser Token Apat
> p_apat_irrefutable = Apat_irrefutable
>                  <$  pSym (ReservedOp "~")
>                  <*> p_apat

> p_fpat :: Parser Token Fpat
> p_fpat = Fpat
>      <$> p_qvar
>      <*  pSym (ReservedOp "=")
>      <*> p_pat

> p_gcon :: Parser Token Gcon
> p_gcon = p_gcon_unit <|  p_gcon_list <|  p_gcon_tuple <|  p_gcon_qcon
> p_gcon_unit :: Parser Token Gcon
> p_gcon_unit = Gcon_unit
>           <$  pSym (Special '(')
>           <*  pSym (Special ')')
> p_gcon_list :: Parser Token Gcon
> p_gcon_list = Gcon_list
>           <$  pSym (Special '[')
>           <*  pSym (Special ']')
> p_gcon_tuple :: Parser Token Gcon
> p_gcon_tuple = (Gcon_tuple . length)
>            <$  pSym (Special '(')
>            <*> pList 1 p_comma
>            <*  pSym (Special ')')
> p_gcon_qcon :: Parser Token Gcon
> p_gcon_qcon = Gcon_qcon <$> p_qcon

> p_var :: Parser Token Var
> p_var = (Var_varid <$> p_varid)
>     <|  (    Var_varsym
>          <$  pSym (Special '(')
>          <*> p_varsym
>          <*  pSym (Special ')')
>         )

> p_qvar :: Parser Token Qvar
> p_qvar = (Qvar_qvarid <$> p_qvarid)
>      <|  (    Qvar_qvarsym
>           <$  pSym (Special '(')
>           <*> p_qvarsym
>           <*  pSym (Special ')')
>          )

> p_con :: Parser Token Con
> p_con = (Con_conid <$> p_conid)
>     <|  (    Con_consym
>          <$  pSym (Special '(')
>          <*> p_consym
>          <*  pSym (Special ')')
>         )

> p_qcon :: Parser Token Qcon
> p_qcon = (Qcon_qconid <$> p_qconid)
>      <|  (    Qcon_gconsym
>           <$  pSym (Special '(')
>           <*> p_gconsym
>           <*  pSym (Special ')')
>          )

> p_varop :: Parser Token Varop
> p_varop = (Varop_varsym <$> p_varsym)
>     <|  (    Varop_varid
>          <$  pSym (Special '`')
>          <*> p_varid
>          <*  pSym (Special '`')
>         )

> p_qvarop :: Parser Token Qvarop
> p_qvarop = (Qvarop_qvarsym <$> p_qvarsym)
>        <|  (    Qvarop_qvarid
>             <$  pSym (Special '`')
>             <*> p_qvarid
>             <*  pSym (Special '`')
>            )
> p_qvarop_infix :: Infix -> Parser Token Qvarop
> p_qvarop_infix i = (Qvarop_qvarsym <$> p_qvarsym_infix i)
>                <|  (    Qvarop_qvarid
>                     <$  pSym (Special '`')
>                     <*> p_qvarid_infix i
>                     <*  pSym (Special '`')
>                    )

> p_conop :: Parser Token Conop
> p_conop = (Conop_consym <$> p_consym)
>       <|  (    Conop_conid
>            <$  pSym (Special '`')
>            <*> p_conid
>            <*  pSym (Special '`')
>           )

> p_qconop :: Parser Token Qconop
> p_qconop = (Qconop_gconsym <$> p_gconsym)
>        <|  (    Qconop_qconid
>             <$  pSym (Special '`')
>             <*> p_qconid
>             <*  pSym (Special '`')
>            )
> p_qconop_infix :: Infix -> Parser Token Qconop
> p_qconop_infix i = (Qconop_gconsym <$> p_gconsym_infix i)
>                <|  (    Qconop_qconid
>                     <$  pSym (Special '`')
>                     <*> p_qconid_infix i
>                     <*  pSym (Special '`')
>                    )

> p_op :: Parser Token Op
> p_op = (Op_varop <$> p_varop)
>    <|  (Op_conop <$> p_conop)

> p_qop :: Parser Token Qop
> p_qop = (Qop_qvarop <$> p_qvarop)
>     <|  (Qop_qconop <$> p_qconop)
> p_qop_infix :: Infix -> Parser Token Qop
> p_qop_infix i = (Qop_qvarop <$> p_qvarop_infix i)
>             <|  (Qop_qconop <$> p_qconop_infix i)
> {-
> p_qop_infix_a :: Int -> Parser Token Qop
> p_qop_infix_a i = p_qop_infix (InfixN i)
>               <|> p_qop_infix (InfixL i)
>               <|> p_qop_infix (InfixR i)
> -}

> p_gconsym :: Parser Token Gconsym
> p_gconsym = (Gconsym_cons <$  pSym (ReservedOp ":"))
>         <|  (Gconsym_qconsym <$> p_qconsym)

> p_gconsym_infix :: Infix -> Parser Token Gconsym
> p_gconsym_infix i = (if i == (InfixR 5) then (Gconsym_cons <$  pSym (ReservedOp ":")) else pFail)
>                 <|  (Gconsym_qconsym <$> p_qconsym_infix i)



> -- Sort this lot out:

> p_literal :: Parser Token Literal
> p_literal = PLiteral <$> pPred is_literal

> p_integer :: Parser Token String
> p_integer = fromInteger' <$> pPred is_integer

> p_float :: Parser Token String
> p_float = fromFloat <$> pPred is_float

> p_in_curlies :: Parser Token b -> Parser Token (Bool, b)
> p_in_curlies p = (    (\r -> (True, r))
>                   <$  p_curly_open
>                   <*> p
>                   <*  p_curly_close
>                  )
>              <|  (    (\r -> (False, r))
>                   <$  p_imp_curly_open
>                   <*> p
>                   <*  p_imp_curly_close
>                  )

> p_curly_open_any :: Parser Token Bool
> p_curly_open_any = (    True
>                     <$  p_curly_open
>                    )
>                <|  (    False
>                     <$  p_imp_curly_open
>                    )

> p_curly_close_any :: Parser Token Bool
> p_curly_close_any = (    True
>                      <$  p_curly_close
>                     )
>                 <|  (    False
>                      <$  p_imp_curly_close
>                     )

> p_curly_open :: Parser Token ()
> p_curly_open = () <$  pSym (Special '{')
> p_imp_curly_open :: Parser Token ()
> p_imp_curly_open = () <$  pSym (ImplicitSpecial '{')
> p_curly_close :: Parser Token ()
> p_curly_close = () <$  pSym (Special '}')
> p_imp_curly_close :: Parser Token ()
> p_imp_curly_close = () <$  pSym (ImplicitSpecial '}')
> p_semicolon :: Parser Token Bool
> p_semicolon = (True <$  pSym (Special ';'))
>           <|  (False <$  pSym (ImplicitSpecial ';'))
> p_comma :: Parser Token ()
> p_comma = () <$  pSym (Special ',')

> p_qconid :: Parser Token QConID
> p_qconid = tupleQConID <$> pPred is_QConID
> p_qtycon :: Parser Token QConID
> p_qtycon = p_qconid
> p_qtycls :: Parser Token QConID
> p_qtycls = p_qconid

> p_conid :: Parser Token ConID
> p_conid = fromConID <$> pPred is_ConID
> p_tycon :: Parser Token ConID
> p_tycon = p_conid
> p_tycls :: Parser Token ConID
> p_tycls = p_conid

> p_qvarid :: Parser Token QVarID
> p_qvarid = tupleQVarID <$> pPred is_QVarID

> p_varid :: Parser Token VarID
> p_varid = fromVarID <$> pPred is_VarID
> p_tyvar :: Parser Token VarID
> p_tyvar = p_varid

> p_qvarsym_infix :: Infix -> Parser Token QVarSym
> p_qvarsym_infix i = tupleQVarSym <$> pPred (is_QVarSym_infix i)

> p_qvarid_infix :: Infix -> Parser Token QVarID
> p_qvarid_infix i = tupleQVarID <$> pPred (is_QVarID_infix i)

> p_qconid_infix :: Infix -> Parser Token QConID
> p_qconid_infix i = tupleQConID <$> pPred (is_QConID_infix i)

> p_varsym :: Parser Token VarSym
> p_varsym = fromVarSym <$> pPred is_VarSym

> p_consym :: Parser Token ConSym
> p_consym = fromConSym <$> pPred is_ConSym

> p_qconsym :: Parser Token QConSym
> p_qconsym = tupleQConSym <$> pPred is_QConSym

> p_qconsym_infix :: Infix -> Parser Token QConSym
> p_qconsym_infix i = tupleQConSym <$> pPred (is_QConSym_infix i)

> p_qvarsym :: Parser Token QVarSym
> p_qvarsym = tupleQVarSym <$> pPred is_QVarSym

