
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Haskell2LaTeX.Offside (indentmarkify, offsideify) where

> import Common.Tokens
> import Haskell2LaTeX.IA
> import Common.Position
> import Haskell2LaTeX.Either
> import Common.Utils

> data IndentState = BOL
>                  | BOS
>                  | BOM
>                  | Normal

indentmark adds the IndentLine and IndentToken tokens as appropriate.

> indentmarkify :: Input [(Token, Position)]
>               -> (Input [(Token, Position)], [String])
> indentmarkify es = (output, errors)
>  where num_rights = (length . filter is_right) es
>        init_state = (num_rights, BOM)
>        (_, output, errors) = mapAccumL_right indentmarkify_work init_state es

> indentmarkify_work :: (Int, IndentState)
>                    -> ([(Token, Position)], Position)
>                    -> Either String ((Int, IndentState), [(Token, Position)])
> indentmarkify_work (n, is) (tps, pos) = Right ((n-1, is'), this)
>  where (is', this) = indentmark (n == 1) pos is tps

> indentmark :: Bool     -- Last section
>            -> Position -- Position at end of section
>            -> IndentState
>            -> [(Token, Position)]
>            -> (IndentState, [(Token, Position)])
>
> -- If we are completely out of input
> indentmark True pos BOM [] = (this, [(IndentToken 0, pos)])
>   where this = error "indentmark: Shouldn't ever look at this"
> -- If we are expecting a '{' at the end of the input
> indentmark True pos BOS [] = (BOL, [(IndentToken 0, pos)])
> indentmark False pos BOS [] = (BOL, [(IndentToken (-1), pos)])
> -- Otherwise we just proceed on to the next section, if appropriate,
> -- with the same state
> indentmark _ _ is [] = (is, [])
>
> -- If we get a Newline token in Normal mode then we need to start
> -- looking for the first token on the next line
> indentmark ls pos Normal (tp@(Newline _, _):tps)
>  = tp `cons_2_2` indentmark ls pos BOL tps
> -- Any other white space anywhere can just be passed through
> indentmark ls pos is (tp@(t, _):tps)
>  | is_white_space t = tp `cons_2_2` indentmark ls pos is tps
>
> -- If we are expecting the beginning of the module
> indentmark ls pos BOM tps@(tp@(t, p):tps')
>  | t == Special '{'         = tp `cons_2_2` indentmark ls pos Normal tps'
>  | t == ReservedID "module" = tp `cons_2_2` indentmark ls pos Normal tps'
>  | otherwise                = this `cons_2_2` indentmark ls pos Normal tps
>   where this = (IndentToken (get_pos_char p), p)
>
> -- If we are expecting a '{'
> indentmark ls pos BOS tps@(tp@(t, p):tps')
>  | t == Special '{' = tp `cons_2_2` indentmark ls pos Normal tps'
>  | otherwise        = this `cons_2_2` indentmark ls pos Normal tps
>   where this = (IndentToken (get_pos_char p), p)
>
> -- Otherwise, if we are looking for a beginning of a line then...
> indentmark ls pos BOL tps@((_, p):_)
>  | otherwise        = this `cons_2_2` indentmark ls pos Normal tps
>   where this = (IndentLine (get_pos_char p), p)
>
> -- "let", "where", "do" and "of" tokens mean we have to start looking
> -- for a '{'
> indentmark ls pos _ (tp@(ReservedID r, _):tps)
>  | r `elem` ["let", "where", "do", "of"]
>   = tp `cons_2_2` indentmark ls pos BOS tps
>
> -- Anything else and we can just continue
> indentmark ls pos Normal (tp:tps)
>  = tp `cons_2_2` indentmark ls pos Normal tps


> offsideify :: Input [(Token, Position)]
>            -> (Input [(Token, Position)], [String])
> offsideify es = (output', errors)
>  where init_state = (init_states, [])
>        ((ss, is), output, errors) = mapAccumL_right offsideify' init_state es
>        output' = if null the_tail
>                  then output
>                  else output ++ [Right ("", the_tail, end_of_file)]
>        the_tail = step_tail ss is

> -- Int here should be Offset!
> -- Should pass errors back
> offsideify' :: (Automaton_State, -- IA states
>                 [Int])  -- Our stack
>             -> ([(Token, Position)], Position)
>             -> Either String ((Automaton_State, [Int]), [(Token, Position)])
> offsideify' (ss, is) (tps, pos) = this
>  where this = case step ss pos is tps of
>                   Left l -> Left l
>                   Right (tps', ss', is') -> Right ((ss', is'), tps'')
>                       where tps'' = strip_leading_implicit_semicolon tps'

> strip_leading_implicit_semicolon :: [(Token, Position)]
>                                  -> [(Token, Position)]
> strip_leading_implicit_semicolon [] = []
> strip_leading_implicit_semicolon tps@(tp@(t, _):tps')
>  | is_white_space t         = tp:strip_leading_implicit_semicolon tps'
>  | t == ImplicitSpecial ';' = tps'
>  | otherwise                = tps

> step :: Automaton_State
>      -> Position
>      -> [Int]
>      -> [(Token, Position)]
>      -> Either String ([(Token, Position)], Automaton_State, [Int])
>
> -- White space can just be passed through
> step as pos ms (tp@(t, _):tps)
>  | is_white_space t = tp `cons` step as pos ms tps
>
> -- Recovery over multiple blocks
> step as pos ((-1):ms) tps@((_, p):_) = step as pos (get_pos_char p:ms) tps
>
> -- Deal with IndentLine tokens
> step as pos (m:ms) tps@((IndentLine n, p):tps')
>  | m == n = step' as pos (m:ms) [im ';' p] tps'
>  | n < m = step' as pos ms [im '}' p] tps
> step as pos ms ((IndentLine _, _):tps) = step as pos ms tps
>
> -- Deal with IndentToken tokens
> step as pos ms@(m:_) ((IndentToken n, p):tps)
>  | n > m = step' as pos (n:ms) [im '{' p] tps
> step as pos [] ((IndentToken n, p):tps)
>  | n > 0 = step' as pos [n] [im '{' p] tps
> step as pos ms ((IndentToken (-1), p):tps)
>  = step' as pos (-1:ms) [im '{' p] tps
> step as pos ms ((IndentToken n, p):tps)
>  = step' as pos ms [im '{' p, im '}' p] ((IndentLine n, p):tps)
>
> step as pos (0:ms) (tp@(Special '}', _):tps) = step' as pos ms [tp] tps
> step as pos ms (tp@(Special '{', _):tps) = step' as pos (0:ms) [tp] tps
> step as pos (m:ms) tps@((t, p):_)
>  | m /= 0 && parse_error as t = step' as pos ms [im '}' p] tps
> step as pos ms (tp:tps) = step' as pos ms [tp] tps
> step as _ [] [] = Right ([], as, [])
> step as _ [m] [] = Right ([], as, [m])
> step as pos (m:ms) []
>  | m /= 0 = step' as pos ms [im '}' pos] []
> step _ pos (_:_:_) []
>  = Left (    "Stack too big and can't shrink it in step at "
>          ++ show_pos pos ++ " - this can't happen!")


> step' :: Automaton_State
>       -> Position
>       -> [Int]
>       -> [(Token, Position)]
>       -> [(Token, Position)]
>       -> Either String ([(Token, Position)], Automaton_State, [Int])
> step' as pos ms [] inp = step as pos ms inp
> step' as pos ms (tp@(t, p):tps) inp
>  | no_such_state as' = Left ("No such state at " ++ show_pos p)
>  | otherwise         = tp `cons` step' as' pos ms tps inp
>   where as' = step_automaton as t

> cons :: (Token, Position)
>      -> Either String ([(Token, Position)], Automaton_State, [Int])
>      -> Either String ([(Token, Position)], Automaton_State, [Int])
> cons _ l@(Left _) = l
> cons x (Right (xs, y, z)) = Right (x:xs, y, z)

> im :: Char -> Position -> (Token, Position)
> im c p = (ImplicitSpecial c, p)

> parse_error :: Automaton_State -> Token -> Bool
> parse_error _ (Special '}') = True
> parse_error as t = no_such_state (step_automaton as t)

> step_tail :: Automaton_State -> [Int] -> [(Token, Position)]
> step_tail as [m]
>  | m /= 0 && not (no_such_state as') = [im '}' end_of_file]
>  where as' = step_automaton as (ImplicitSpecial '}')
> step_tail _ [] = []
> step_tail _ _ = error "step_tail"

