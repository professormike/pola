
> module Haskell2LaTeX.FiniteMap (listToFM, lookupFM, FiniteMap) where

> import Data.List

> data FiniteMap a b = LeafFM
>                    | Branch (FiniteMap a b) a b (FiniteMap a b)
>  deriving Show

> listToFM :: Ord a => [(a, b)] -> FiniteMap a b
> listToFM xs = fst $ listToFM' (sortBy compare_fst xs) (length xs)

> compare_fst :: Ord a => (a, b) -> (a, b) -> Ordering
> compare_fst (x, _) (y, _) = compare x y

> listToFM' :: [(a, b)]
>           -> Int
>           -> (FiniteMap a b, [(a, b)])
> listToFM' xs 0 = (LeafFM, xs)
> listToFM' xs n = (Branch left key value right, xs'')
>  where n_div_2 = n `div` 2
>        (left, (key, value):xs') = listToFM' xs n_div_2
>        (right, xs'') = listToFM' xs' (n - n_div_2 - 1)

> lookupFM :: (Ord a) => FiniteMap a b -> a -> Maybe b
> lookupFM LeafFM _ = Nothing
> lookupFM (Branch left key val right) look
>  | look == key = Just val
>  | look < key = lookupFM left look
>  | otherwise = lookupFM right look

