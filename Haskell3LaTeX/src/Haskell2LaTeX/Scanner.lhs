
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Haskell2LaTeX.Scanner (scan) where

> import Prelude hiding ((<$>), (<*>), (<$), (<*))
> import Haskell2LaTeX.Either
> import Common.Tokens
> import Common.PC
> import Common.Position
> import Data.Char
> import Common.Utils

We may want to refine this to handle error cases better, but this will
for now.

> scan :: Input [(Char, Position)] -> (Input [(Token, Position)], [String])
> scan input = do_rights scan_block input

> scan_block :: [(Char, Position)] -> Either String [(Token, Position)]
> scan_block inp
>  = if parse_succeeded this_parse
>    then case rest_of_input this_parse of
>             [] -> Right (ret_val this_parse)
>             (_, p):_ ->
>                 Left (   "Scanner finished with input still left at "
>                       ++ show_pos p)
>    else Left ("Scanner failed at " ++ show_pos (fail_pos this_parse))
>  where this_parse = scanner inp


> scanner :: Parser Char [(Token, Position)]
> scanner = lexts_program


> lexts_program :: Parser Char [(Token, Position)]
> lexts_program = concat
>             <$> pList 0 (lexts_lexeme <|> lexts_whitespace)

> lexts_lexeme :: Parser Char [(Token, Position)]
> lexts_lexeme = wrap <$> lext_lexeme

> lext_lexeme :: Parser Char (Token, Position)
> lext_lexeme = lext_qvarid
>           <|  lext_qconid
>           <|  lext_qvarsym
>           <|  lext_qconsym
>           <|  lext_literal
>           <|  lext_special
>           <|  lext_reservedop
>           <|  lext_reservedid

> lext_literal :: Parser Char (Token, Position)
> lext_literal = lext_integer
>             |> lext_float
>            <|  lext_char
>            <|  lext_string

> lext_special :: Parser Char (Token, Position)
> lext_special = Special <&> lexc_special
> lexc_special :: Parser Char Char
> lexc_special = pSym '(' <|  pSym ')' <|  pSym ',' <|  pSym ';'
>            <|  pSym '[' <|  pSym ']' <|  pSym '`' <|  pSym '{'
>            <|  pSym '}'

> lexts_whitespace :: Parser Char [(Token, Position)]
> lexts_whitespace = concat <$> pList 1 lexts_whitestuff

> lexts_whitestuff :: Parser Char [(Token, Position)]
> lexts_whitestuff = wrap <$> lext_whitechar
>                <|  wrap <$> lext_comment
>                <|  lexts_ncomment

> lext_whitechar :: Parser Char (Token, Position)
> lext_whitechar = Newline <&> lexs_newline
>              <|  WhiteChar <&> (    lexc_vertab
>                                 <|  lexc_space
>                                 <|  lexc_tab
>                                )
> lexs_whitechar :: Parser Char String
> lexs_whitechar = lexs_newline
>              <|  wrap <$> lexc_vertab
>              <|  wrap <$> lexc_space
>              <|  wrap <$> lexc_tab
> lexs_newline :: Parser Char String
> lexs_newline = (\r l -> [r, l]) <$> lexc_return <*> lexc_linefeed
>            <|  wrap <$> lexc_return
>            <|  wrap <$> lexc_linefeed
>            <|  wrap <$> lexc_formfeed

> lexc_return :: Parser Char Char
> lexc_return = pSym '\r'
> lexc_linefeed :: Parser Char Char
> lexc_linefeed = pSym '\n'
> lexc_vertab :: Parser Char Char
> lexc_vertab = pSym '\v'
> lexc_formfeed :: Parser Char Char
> lexc_formfeed = pSym '\f'
> lexc_space :: Parser Char Char
> lexc_space = pSym ' '
> lexc_tab :: Parser Char Char
> lexc_tab = pSym '\t'

XXX lext problems here and possibly a couple of other places - redo the naming scheme to have lextp?

> lext_comment :: Parser Char (Token, Position)
> lext_comment = keep_pos lexs_comment
> lexs_comment :: Parser Char Token
> lexs_comment = Comment
>            <$> lexi_dashes
>            <*> (    (:)
>                 <$> (lext_any <!> lexc_symbol)
>                 <*> pList 0 lext_any
>                 `opt` ""
>                )
>            <*  lexs_newline
> lexi_dashes :: Parser Char Int
> lexi_dashes = length <$> pList 2 (pSym '-')
> lexs_opencom :: Parser Char (Token, Position)
> lexs_opencom = NCommentOpen <&  pSyms "{-"
> lexs_closecom :: Parser Char (Token, Position)
> lexs_closecom = NCommentClose <&  pSyms "-}"
> lexts_ncomment :: Parser Char [(Token, Position)]
> lexts_ncomment = (\oc as xs cc -> [oc] ++ as ++ concat xs ++ [cc])
>              <$> lexs_opencom
>              <*> lexts_ANYseq
>              <*> pList 0 ((++) <$> lexts_ncomment <*> lexts_ANYseq)
>              <*> lexs_closecom

> lexts_ANYseq :: Parser Char [(Token, Position)]
> lexts_ANYseq = pList 0 (lext_ANY <!> (lexs_opencom <|  lexs_closecom))
> lext_ANY :: Parser Char (Token, Position)
> lext_ANY = WhiteChar <&> lexc_graphic <|  lext_whitechar
> lext_any :: Parser Char Char
> lext_any = lexc_graphic <|  lexc_space <|  lexc_tab

> lexc_graphic :: Parser Char Char
> lexc_graphic = lexc_small <|  lexc_large <|  lexc_symbol <|  lexc_digit
>            <|  lexc_special <|  pSym ':' <|  pSym '"' <|  pSym '\''

> lexc_small :: Parser Char Char
> lexc_small = lexc_ascSmall <|  pSym '_'
> lexc_ascSmall :: Parser Char Char
> lexc_ascSmall = pPred isLower
> lexc_large :: Parser Char Char
> lexc_large = lexc_ascLarge
> lexc_ascLarge :: Parser Char Char
> lexc_ascLarge = pPred isUpper

> lexc_symbol :: Parser Char Char
> lexc_symbol = lexc_ascSymbol
> lexc_ascSymbol :: Parser Char Char
> lexc_ascSymbol = pSym '!' <|  pSym '#' <|  pSym '$' <|  pSym '%'
>              <|  pSym '&' <|  pSym '*' <|  pSym '+' <|  pSym '.'
>              <|  pSym '/' <|  pSym '<' <|  pSym '=' <|  pSym '>'
>              <|  pSym '?' <|  pSym '@' <|  pSym '\\' <|  pSym '^'
>              <|  pSym '|' <|  pSym '-' <|  pSym '~'

> lexc_digit :: Parser Char Char
> lexc_digit = lexc_ascDigit
> lexc_ascDigit :: Parser Char Char
> lexc_ascDigit = pPred isDigit
> lexc_octit :: Parser Char Char
> lexc_octit = pPred isOctDigit
> lexc_hexit :: Parser Char Char
> lexc_hexit = pPred isHexDigit

> id_body :: Parser Char Char
> id_body = lexc_small <|  lexc_large <|  lexc_digit <|  pSym '\'' <| pSym '.'
> lexs_varid :: Parser Char String
> lexs_varid = (:)
>          <$> lexc_small
>          <*> pList 0 id_body
>          <!> lext_reservedid
> lexs_conid :: Parser Char String
> lexs_conid = (:)
>          <$> lexc_large
>          <*> pList 0 id_body
> lext_reservedid :: Parser Char (Token, Position)
> lext_reservedid = ReservedID <&> lexs_reservedid
> lexs_reservedid :: Parser Char String
> lexs_reservedid = pSyms "case"     <|  pSyms "class"
>               <|  pSyms "data"     <|  pSyms "default"
>               <|  pSyms "deriving" <|  pSyms "do"
>               <|  pSyms "else"     <|  pSyms "if"
>               <|  pSyms "import"   <|  pSyms "in"
>                |> pSyms "infix"     |> pSyms "infixl"
>                |> pSyms "infixr"    |> pSyms "instance"
>               <|  pSyms "let"      <|  pSyms "module"
>               <|  pSyms "newtype"  <|  pSyms "of"
>               <|  pSyms "then"     <|  pSyms "type"
>               <|  pSyms "where"    <|  pSyms "_"

> sym_body :: Parser Char Char
> sym_body = lexc_symbol <|  pSym ':'
> lexs_varsym :: Parser Char String
> lexs_varsym = (:)
>           <$> lexc_symbol
>           <*> pList 0 sym_body
>           <!> lext_reservedop
>           <!> lexi_dashes
> lexs_consym :: Parser Char String
> lexs_consym = (:)
>           <$> pSym ':'
>           <*> pList 0 sym_body
>           <!> lext_reservedop
> lext_reservedop :: Parser Char (Token, Position)
> lext_reservedop = ReservedOp <&> lexs_reservedop
> lexs_reservedop :: Parser Char String
> lexs_reservedop = pSyms ".."  |> pSyms ":"   |> pSyms "::"
>                |> pSyms "="   |> pSyms "\\"  |> pSyms "|"
>                |> pSyms "<-"  |> pSyms "->"  |> pSyms "@"
>                |> pSyms "~"   |> pSyms "=>"

> can_qual :: (Maybe String -> String -> Token)
>          -> Parser Char String
>          -> Parser Char (Token, Position)
> can_qual c p = keep_pos (can_qual' c p)

> can_qual' :: (Maybe String -> String -> Token)
>           -> Parser Char String
>           -> Parser Char Token
> can_qual' c p = c
>             <$> (Just <$> lexs_conid)
>             <*  pSym '.'
>             <*> p
>         <|>     c Nothing
>             <$> p

> lext_qvarid :: Parser Char (Token, Position)
> lext_qvarid = can_qual (QVarID (InfixL 9)) lexs_varid

> lext_qconid :: Parser Char (Token, Position)
> lext_qconid = can_qual (QConID (InfixL 9)) lexs_conid

> lext_qvarsym :: Parser Char (Token, Position)
> lext_qvarsym = can_qual (QVarSym (InfixL 9)) lexs_varsym

> lext_qconsym :: Parser Char (Token, Position)
> lext_qconsym = can_qual (QConSym (InfixL 9)) lexs_consym

> lexs_decimal :: Parser Char String
> lexs_decimal = pList 1 lexc_digit
> lexs_octal :: Parser Char String
> lexs_octal = pList 1 lexc_octit
> lexs_hexadecimal :: Parser Char String
> lexs_hexadecimal = pList 1 lexc_hexit

> lext_integer :: Parser Char (Token, Position)
> lext_integer = Integer
>            <&> (lexs_decimal  |> hex  |> oct)
>  where hex = (++)
>          <$> (pSyms "0x" <|  pSyms "0X")
>          <*> lexs_hexadecimal
>        oct = (++)
>          <$> (pSyms "0o" <|  pSyms "0O")
>          <*> lexs_octal

> lext_float :: Parser Char (Token, Position)
> lext_float = Float <&> lexs_float

> lexs_float :: Parser Char String
> lexs_float = (\ds1 d ds2 e -> ds1 ++ [d] ++ ds2 ++ e)
>          <$> lexs_decimal
>          <*> pSym '.'
>          <*> lexs_decimal
>          <*> (lexs_exponent `opt` "")
>      <|>     (++)
>          <$> lexs_decimal
>          <*> lexs_exponent

> lexs_exponent :: Parser Char String
> lexs_exponent = (\e s ds -> [e] ++ s ++ ds)
>             <$> (pSym 'e' <|  pSym 'E')
>             <*> ((pSyms "-" <|  pSyms "+") `opt` "")
>             <*> lexs_decimal

> lext_char :: Parser Char (Token, Position)
> lext_char = Char <&> lexs_char
> lexs_char :: Parser Char String
> lexs_char = id
>         <$  pSym '\''
>         <*> char_inside
>         <*  pSym '\''
>  where char_inside :: Parser Char String
>        char_inside = wrap <$> char_graphic
>                  <|  wrap <$> lexc_space
>                  <|  (lexs_escape <!> pSyms "\\&")
>        char_graphic :: Parser Char Char
>        char_graphic = lexc_graphic <!> (pSym '\'' <|> pSym '\\')

> lext_string :: Parser Char (Token, Position)
> lext_string = keep_pos $ String
>           <$  pSym '"'
>           <*> (StringContents . concat <$> pList 0 str_content)
>           <*  pSym '"'
>  where str_content :: Parser Char [StringContent]
>        str_content = wrap . StringChar <$> str_real_content <|> str_gap
>        str_real_content = wrap <$> (str_graphic <|  lexc_space) <|  lexs_escape
>        str_graphic = lexc_graphic <!> pSym '"' <|  pSym '\\'
>        str_gap :: Parser Char [StringContent]
>        str_gap = (\start m end -> [start] ++ m ++ [end])
>              <$> (GapStart <$  pSym '\\')
>              <*> pList 1 (StringWhiteChar <$> lexs_whitechar)
>              <*> (GapEnd <$  pSym '\\')


> lexs_escape :: Parser Char String
> lexs_escape = (:) <$> pSym '\\' <*> esc
>  where esc = lexs_charesc
>          <|  lexs_ascii
>          <|  lexs_decimal
>           |> o_oct
>           |> x_hex
>        o_oct :: Parser Char String
>        o_oct = (:) <$> pSym 'o' <*> lexs_octal
>        x_hex :: Parser Char String
>        x_hex = (:) <$> pSym 'x' <*> lexs_hexadecimal

> lexs_charesc :: Parser Char String
> lexs_charesc = wrap <$> lexc_charesc

> lexc_charesc :: Parser Char Char
> lexc_charesc = pSym 'a' <|  pSym 'b'  <|  pSym 'f' <|  pSym 'n'
>            <|  pSym 'r' <|  pSym 't'  <|  pSym 'v' <|  pSym '\\'
>            <|  pSym '"' <|  pSym '\'' <|  pSym '&'

> lexs_ascii :: Parser Char String
> lexs_ascii = (\x y -> [x, y]) <$> pSym '^' <*> lexc_cntrl
>          <|  pSyms "NUL" <|  pSyms "SOH" <|  pSyms "STX"
>          <|  pSyms "ETX" <|  pSyms "EOT" <|  pSyms "ENQ"
>          <|  pSyms "ACK" <|  pSyms "BEL" <|  pSyms "BS"
>          <|  pSyms "HT"  <|  pSyms "LF"  <|  pSyms "VT"
>          <|  pSyms "FF"  <|  pSyms "CR"  <|  pSyms "SO"
>          <|  pSyms "SI"  <|  pSyms "DLE" <|  pSyms "DC1"
>          <|  pSyms "DC2" <|  pSyms "DC3" <|  pSyms "DC4"
>          <|  pSyms "NAK" <|  pSyms "SYN" <|  pSyms "ETB"
>          <|  pSyms "CAN" <|  pSyms "EM"  <|  pSyms "SUB"
>          <|  pSyms "ESC" <|  pSyms "FS"  <|  pSyms "GS"
>          <|  pSyms "RS"  <|  pSyms "US"  <|  pSyms "SP"
>          <|  pSyms "DEL"

> lexc_cntrl :: Parser Char Char
> lexc_cntrl = lexc_ascLarge <|  pSym '@' <|  pSym '[' <|  pSym '\\'
>                            <|  pSym ']' <|  pSym '^' <|  pSym '_'

