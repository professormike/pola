% vim: nowrap

> module Haskell2LaTeX.Args (Args,
>              parse_commandline,
>              usage_info,
>              do_unlit,
>              stop_after_unlit, stop_after_lex, stop_after_indentmark,
>              stop_after_offside, stop_after_fixity, stop_after_parseable,
>              stop_after_parse,
>              get_output_file,
>              print_list_text, print_tree_text, print_tree_latex,
>              want_help, want_version)
>   where
                                
> import Haskell2LaTeX.GetOpt                     
> import Data.Char
> import Data.Maybe

> data Args = Args [Flag]
> data Flag = Help
>           | Output FilePath
>           | Print Print
>           | StopAfter Phase
>           | UnlitInput Bool
>           | Version
>    deriving (Show, Eq)
> data Phase = Unlit
>            | Lex
>            | Indentmark
>            | Offside
>            | Fixity
>            | Parseable
>            | Parse
>    deriving (Show, Eq)
> data Print = List
>            | Tree
>            | Text
>            | LaTeX
>    deriving (Show, Eq)

> options :: [OptDescr Flag]                       
> options =
>  [Option ['d'] ["no-unlit"]   (NoArg (UnlitInput False)) "Do not unlit input (default if filename with extension .hs is given)",
>   Option ['h'] ["help"]       (NoArg Help)               "display this help and exit",
>   Option ['o'] ["output"]     (ReqArg Output "FILE")     "output to FILE",
>   Option ['p'] ["print"]      (ReqArg get_print "PRINT") "Print in style PRINT: list, tree, text or latex. Defaults to LaTeX for a tree or text for a list",
>   Option ['s'] ["stop-after"] (ReqArg get_phase "PHASE") "Stop after PHASE: unlit, lex, indentmark, offside, fixity, parseable or parse. Defaults to parse",
>   Option ['u'] ["unlit"]      (NoArg (UnlitInput True))  "Unlit input (default unless filename with extension .hs is given)",
>   Option ['V'] ["version"]    (NoArg Version)            "output version information and exit"
>  ]                  

> get_print :: String -> Flag
> get_print p
>  | p' == "list"  = Print List
>  | p' == "tree"  = Print Tree
>  | p' == "text"  = Print Text
>  | p' == "latex" = Print LaTeX
>  where p' = map toLower p
> get_print _ = error "Foo! XXX"

> get_phase :: String -> Flag
> get_phase p
>  | p' == "unlit"      = StopAfter Unlit
>  | p' == "lex"        = StopAfter Lex
>  | p' == "indentmark" = StopAfter Indentmark
>  | p' == "offside"    = StopAfter Offside
>  | p' == "fixity"     = StopAfter Fixity
>  | p' == "parseable"  = StopAfter Parseable
>  | p' == "parse"      = StopAfter Parse
>  where p' = map toLower p
> get_phase _ = error "Foo! XXX"

Complex if multiple filenames - what if -o is given etc?

> parse_commandline :: [String] -> (Args, FilePath)
> parse_commandline ss = case (getOpt Permute options ss) of
>                            (opts, [],         []) -> mk_args opts "-"
>                            (opts, [filename], []) -> mk_args opts filename
>                            (_, _, []) -> error "Too many files XXX"
>                            _ -> error "Bad options XXX"

> mk_args :: [Flag] -> FilePath -> (Args, FilePath)
> mk_args opts filename = (Args (reverse opts ++ defaults), filename)
>   where default_unlit = if take 3 (reverse filename) == reverse ".hs"
>                         then UnlitInput False
>                         else UnlitInput True
>         defaults = [default_unlit,
>                     Output "-",
>                     Print LaTeX, Print Text,
>                     StopAfter Parse]

> usage_info :: String
> usage_info = usageInfo "" options

> headCatMaybesMap :: (a -> Maybe b) -> [a] -> b
> headCatMaybesMap f = head . catMaybes . map f

> do_unlit :: Args -> Bool
> do_unlit (Args as) = headCatMaybesMap unwrap_unlit as

> unwrap_unlit :: Flag -> Maybe Bool
> unwrap_unlit (UnlitInput b) = Just b
> unwrap_unlit _ = Nothing

> stop_after_phase :: Phase -> Args -> Bool
> stop_after_phase p (Args as) = ((p ==) . headCatMaybesMap unwrap_phases) as

> unwrap_phases :: Flag -> Maybe Phase
> unwrap_phases (StopAfter p) = Just p
> unwrap_phases _ = Nothing

> stop_after_unlit :: Args -> Bool
> stop_after_unlit = stop_after_phase Unlit

> stop_after_lex :: Args -> Bool
> stop_after_lex = stop_after_phase Lex

> stop_after_indentmark :: Args -> Bool
> stop_after_indentmark = stop_after_phase Indentmark

> stop_after_offside :: Args -> Bool
> stop_after_offside = stop_after_phase Offside

> stop_after_fixity :: Args -> Bool
> stop_after_fixity = stop_after_phase Fixity

> stop_after_parseable :: Args -> Bool
> stop_after_parseable = stop_after_phase Parseable

> stop_after_parse :: Args -> Bool
> stop_after_parse = stop_after_phase Parse

> get_output_file :: Args -> FilePath
> get_output_file (Args as) = headCatMaybesMap unwrap_output as

> unwrap_output :: Flag -> Maybe FilePath
> unwrap_output (Output f) = Just f
> unwrap_output _ = Nothing

> print_tree_style :: Args -> Print
> print_tree_style (Args as) = headCatMaybesMap unwrap_tree_prints as

> unwrap_tree_prints :: Flag -> Maybe Print
> unwrap_tree_prints (Print Tree) = Just Tree
> unwrap_tree_prints (Print Text) = Just Text
> unwrap_tree_prints (Print LaTeX) = Just LaTeX
> unwrap_tree_prints _ = Nothing

> print_list_style :: Args -> Print
> print_list_style (Args as) = headCatMaybesMap unwrap_list_prints as

> unwrap_list_prints :: Flag -> Maybe Print
> unwrap_list_prints (Print List) = Just List
> unwrap_list_prints (Print Text) = Just Text
> unwrap_list_prints _ = Nothing

> print_list_text :: Args -> Bool
> print_list_text args = print_list_style args == Text

> print_tree_text :: Args -> Bool
> print_tree_text args = print_tree_style args == Text

> print_tree_latex :: Args -> Bool
> print_tree_latex args = print_tree_style args == LaTeX

> want_help :: Args -> Bool
> want_help (Args as) = any (Help ==) as

> want_version :: Args -> Bool
> want_version (Args as) = any (Version ==) as

