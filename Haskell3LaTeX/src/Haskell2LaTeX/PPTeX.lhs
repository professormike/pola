
GPL, copyright Ian Lynagh <igloo@earth.li> 2001

> module Haskell2LaTeX.PPTeX (pp_ModuleHead, pp_ModuleImpdecls, pp_ModuleTopdecls,
>               pp_ModuleTail)
>  where

> import Data.Char
> import Haskell2LaTeX.Tree
> import Common.Tokens
> import Haskell2LaTeX.Subject

> char :: Char -> String
> char = escape
> text :: String -> String
> text = concat . map escape

> s_op :: String -> String
> s_op "." = "$\\circ$"
> s_op ">=" = "$\\ge$"
> s_op "<=" = "$\\le$"
> s_op "/=" = "$\\ne$"
> s_op "++" = "+\\kern-0.3em+"
> s_op "*" = "$\\times$"
> s_op xs = concat $ map escape xs
> s_tyvar :: String -> String
> s_tyvar "a" = "$\\alpha$"
> s_tyvar "b" = "$\\beta$"
> s_tyvar "c" = "$\\gamma$"
> s_tyvar "d" = "$\\delta$"
> s_tyvar tyvar = text tyvar

> escape :: Char -> String
> escape '^' = "\\^{}"
> escape '~' = "\\~{}"
> escape '*' = "$\\ast$"
> escape '\\' = "$\\backslash$"
> escape x | isMath x      = "${" ++ [x] ++ "}$"
>          | needsEscape x = ['\\', x]
>          | otherwise     = [x]

> needsEscape :: Char -> Bool
> needsEscape c = any (c ==) "_$&{}%#"

> isMath :: Char -> Bool
> isMath c = any (c ==) "-<>|"

> pp_qual :: (String -> String) -> (Maybe String, String) -> String
> pp_qual f (Nothing, s) = f s
> pp_qual f (Just q, s) = text q ++ "." ++ f s

> lambda :: String
> -- lambda = char '\\'
> lambda = "$\\lambda$ "

> pp_VarID :: VarID -> String
> pp_VarID v = "\\textit{" ++ text v ++ "\\/}"
> pp_ConID :: ConID -> String
> pp_ConID v = "\\textsf{" ++ text v ++ "\\/}"
> pp_VarSym :: VarSym -> String
> pp_VarSym = s_op
> pp_ConSym :: ConSym -> String
> pp_ConSym v = "\\textsf{" ++ text v ++ "\\/}"
> pp_Tyvar :: Tyvar -> String
> pp_Tyvar = s_tyvar
> pp_Tycon :: Tycon -> String
> pp_Tycon v = "\\textsf{" ++ text v ++ "\\/}"
> pp_Tycls :: Tycls -> String
> pp_Tycls v = "\\textsf{" ++ text v ++ "\\/}"
> pp_Tycon_Tycls :: Tycon_Tycls -> String
> pp_Tycon_Tycls = text
> pp_Modid :: Modid -> String
> pp_Modid v = "\\textsf{" ++ text v ++ "\\/}"
> pp_QVarID :: QVarID -> String
> pp_QVarID = pp_qual pp_VarID
> pp_QConID :: QConID -> String
> pp_QConID = pp_qual pp_ConID
> pp_Qtycon :: Qtycon -> String
> pp_Qtycon = pp_QConID
> pp_Qtycls :: Qtycls -> String
> pp_Qtycls = pp_QConID
> pp_Qtycon_Qtycls :: Qtycon_Qtycls -> String
> pp_Qtycon_Qtycls = pp_QConID
> pp_QVarSym :: QVarSym -> String
> pp_QVarSym = pp_qual pp_VarSym
> pp_QConSym :: QConSym -> String
> pp_QConSym = pp_qual pp_ConSym

-----

> takeDropAround :: (a -> Bool) -> [a] -> ([a], [a])
> takeDropAround _ [] = ([], [])
> takeDropAround f (x:xs)
>  | f x = ([], xs)
>  | otherwise = (x:ys, zs)
>  where (ys, zs) = takeDropAround f xs

> ($$) :: String -> String -> String
> a $$ b = a ++ "\n\n" ++ b
> (<+>) :: String -> String -> String
> "" <+> b = b
> a <+> "" = a
> a <+> b = a ++ " " ++ b

> pp_semicolon :: Bool -> String
> pp_semicolon True = ";"
> pp_semicolon False = ""

> pp_comma :: Bool -> String
> pp_comma True = ","
> pp_comma False = ""

> pp_im_semicolon_list :: (a -> String) -> [Either Bool a] -> String
> pp_im_semicolon_list _ [] = ""
> pp_im_semicolon_list f [Right x] = f x
> {-
> pp_im_semicolon_list f [Right x, Left semicolon]
>  = f x ++ pp_semicolon semicolon
> -}
> pp_im_semicolon_list f (Right x:Left semicolon:xs)
>  = this' ++ pp_im_semicolon_list f xs
>  where this = f x ++ pp_semicolon semicolon
>        this' = if this == "" then "" else this ++ "\\\\\n"
> pp_im_semicolon_list _ _ = error "Illegal im semicolon list"

> pp_comma_list :: (a -> String) -> [a] -> String
> pp_comma_list = pp_thing_list ", "

> pp_thing_list :: String -> (a -> String) -> [a] -> String
> pp_thing_list _ _ [] = ""
> pp_thing_list thing f xs = concat (map ((++ thing) . f) ys ++ [f y])
>  where (ys, y) = (init xs, last xs)

> sconcat :: [String] -> String
> sconcat = pp_thing_list " " id

> snconcat :: [String] -> String
> snconcat = sconcat . filter (/= "")

> is_visible :: Bool -> String
> is_visible b = "{" ++ show (fromEnum b) ++ "}"

> parens :: String -> String
> parens x = char '(' ++ x ++ char ')'

> braces :: String -> String
> braces x = char '{' ++ x ++ char '}'

> resizing_parens :: String -> String
> resizing_parens s = "\\IGLhaskellresize{(}{)}{" ++ s ++ "}"
> -- resizing_parens = surround "(" ")"

> resizing_brackets :: String -> String
> resizing_brackets s = "\\IGLhaskellresize{[}{]}{" ++ s ++ "}"
> -- resizing_brackets = surround "[" "]"

> brackets :: String -> String
> brackets x = char '[' ++ x ++ char ']'

 surround :: String -> String -> String -> String
 surround left right middle = "$\\left" ++ left ++ "\\matrix{\\hbox{"
                           ++ middle
                           ++ "}}\\right" ++ right ++ "$"

> pp_Literal :: Literal -> String
> pp_Literal (PLiteral t) = pp_literal t

> pp_literal :: Token -> String
> pp_literal (String s) = pp_string s
> pp_literal (Char s) = "`" ++ text s ++ "'"
> pp_literal (Integer s) = text s
> pp_literal (Float s) = text s
> pp_literal t = error ("pp_literal: Token " ++ show t ++ " is not a literal")

> pp_string :: StringContents -> String
> pp_string (StringContents scs) = "\\hbox{``" ++ text (pp_scs scs) ++ "''}"
> pp_scs :: [StringContent] -> String
> pp_scs = concat . map pp_sc
> pp_sc :: StringContent -> String
> pp_sc GapStart = "\\"
> pp_sc GapEnd = "\\"
> pp_sc (StringChar s) = s -- EEEEEK!
> pp_sc (StringWhiteChar s) = s -- EEEEEK!

---------------

> pp_ModuleHead :: ModuleHead -> String
> pp_ModuleHead (ModuleHead_with_id modid m_exports open)
>  =    "\\begin{IGLhaskell}"
>    ++ "\\IGLhaskellmodule"
>    ++ "{" ++ pp_Modid modid ++ "}"
>    ++ "{" ++ s_exports ++ "}"
>    ++ s_open ++ "\n\n"
>    ++ "\\end{IGLhaskell}"
>  where s_exports = case m_exports of
>                        Nothing -> ""
>                        Just exports -> parens (pp_Exports exports)
>        s_open = if open then " \\{" else ""
> pp_ModuleHead (ModuleHead_no_id open) = if open
>                                         then "\\begin{IGLhaskell}"
>                                           ++ "\\{"
>                                           ++ "\\end{IGLhaskell}"
>                                         else "\n\n"

> pp_ModuleImpdecls :: Impdecls -> String
> pp_ModuleImpdecls is = "\\begin{IGLhaskell}"
>                     ++ pp_Impdecls is
>                     ++ "\\end{IGLhaskell}"

> pp_ModuleTopdecls :: Topdecls -> String
> pp_ModuleTopdecls ts = "\\begin{IGLhaskell}"
>                     ++ pp_Topdecls ts
>                     ++ "\\end{IGLhaskell}"

> pp_ModuleTail :: ModuleTail -> String
> pp_ModuleTail (ModuleTail close) = if close
>                                    then "\\begin{IGLhaskell}"
>                                      ++ "\\}"
>                                      ++ "\\end{IGLhaskell}"
>                                    else ""

> pp_Module :: Module -> String
> pp_Module (Module_with_id modid m_exports body)
>  = "\\IGLhaskellmodule"
>    ++ "{" ++ pp_Modid modid ++ "}"
>    ++ "{" ++ s_exports ++ "}\n\n"
>    ++ pp_Body body
>  where s_exports = case m_exports of
>                        Nothing -> ""
>                        Just exports ->
>                            char '(' ++ pp_Exports exports ++ char ')'
> pp_Module (Module_no_id body) = pp_Body body

> pp_Body :: Body -> String
> pp_Body (Body_both True impdecls semicolon topdecls)
>  =    "\\{\n\n"
>    ++ pp_Body (Body_both False impdecls semicolon topdecls)
>    ++ "\n\n\\}"
> pp_Body (Body_both False impdecls semicolon topdecls)
>  =    pp_Impdecls impdecls ++ pp_semicolon semicolon ++ "\n\n"
>    ++ pp_Topdecls topdecls ++ "\n"
> pp_Body (Body_imp True impdecls) = "\\{\n\n"
>                                 ++ pp_Impdecls impdecls
>                                 ++ "\n\n\\}\n"
> pp_Body (Body_imp False impdecls) = pp_Impdecls impdecls
> pp_Body (Body_top True topdecls) = "\\{\n\n"
>                                 ++ pp_Topdecls topdecls
>                                 ++ "\n\n\\}\n"
> pp_Body (Body_top False topdecls) = pp_Topdecls topdecls

> pp_Impdecls :: Impdecls -> String
> pp_Impdecls (Impdecls impdecls) = pp_im_semicolon_list pp_Impdecl impdecls

> pp_Exports :: Exports -> String
> pp_Exports (Exports es tc) = pp_comma_list pp_Export es ++ pp_comma tc

> pp_Export :: Export -> String
> pp_Export (Export_qvar qvar) = pp_Qvar qvar
> pp_Export (Export_qtycon_qtycls q mme_cs_qvs)
>  = snconcat [pp_Qtycon_Qtycls q, bits]
>  where bits = case mme_cs_qvs of
>                   Nothing -> ""
>                   Just Nothing -> parens "\\twodots"
>                   Just (Just (Left cs)) -> parens (pp_comma_list pp_Con cs)
>                   Just (Just (Right qvs)) -> parens (pp_comma_list pp_Qvar qvs)
> pp_Export (Export_module modid) = "\\IGLhaskellkeyword{module} "
>                                ++ pp_Modid modid

> pp_Impdecl :: Impdecl -> String
> pp_Impdecl (Impdecl qual modid m_as m_impspec)
>  = "\\IGLhaskellimport" ++ is_visible qual
>                   ++ "{" ++ pp_Modid modid ++ "}"
>                   ++ "{" ++ s_as ++ "}"
>                   ++ "{" ++ impspec ++ "}"
>  where s_as = case m_as of
>                   Nothing -> ""
>                   Just as_modid -> pp_Modid as_modid
>        impspec = case m_impspec of
>                      Nothing -> ""
>                      Just i -> pp_Impspec i
> pp_Impdecl Impdecl_empty = ""

> pp_Impspec :: Impspec -> String
> pp_Impspec (Impspec hid imports tc)
>  = snconcat [doc_hiding, parens (pp_imports imports ++ pp_comma tc)]
>  where doc_hiding = if hid then "\\IGLhaskellkeyword{hiding}" else ""

> pp_imports :: [Import] -> String
> pp_imports = pp_comma_list pp_Import

> pp_Import :: Import -> String
> pp_Import (Import_var var) = pp_Var var
> pp_Import (Import_tycon_tycls t mme_cs_vs) = snconcat [pp_Tycon_Tycls t, bits]
>  where bits = case mme_cs_vs of
>                   Nothing -> ""
>                   Just Nothing -> parens "\\twodots"
>                   Just (Just (Left cs)) -> parens (pp_comma_list pp_Con cs)
>                   Just (Just (Right vs)) -> parens (pp_comma_list pp_Var vs)

> pp_Topdecls :: Topdecls -> String
> pp_Topdecls (Topdecls topdecls)
>  = pp_xdecls_list get_subject_Topdecl pp_Topdecl (collapse topdecls)

> pp_Topdecl :: (Topdecl, Bool) -> String
> pp_Topdecl (Topdecl_type simpletype t, semi)
>  = "\\IGLhaskelltype{" ++ pp_Simpletype simpletype ++ "}"
>                 ++ "{" ++ pp_Type t ++ s_semi ++ "}"
>  where s_semi = pp_semicolon semi
> pp_Topdecl (Topdecl_data m_context simpletype constrs m_deriving, semi)
>  = "\\IGLhaskelldata{" ++ context ++ "}"
>                 ++ "{" ++ pp_Simpletype simpletype ++ "}"
>                 ++ "{" ++ pp_Constrs constrs c_semi ++ "}"
>                 ++ s_deriving
>  where context = case m_context of
>                      Nothing -> ""
>                      Just c -> pp_Context c ++ " $\\Rightarrow$"
>        (s_deriving, c_semi) = case m_deriving of
>                                   Nothing -> ("", semi)
>                                   Just d -> (pp_Deriving d semi, False)
> pp_Topdecl (Topdecl_newtype m_context simpletype newconstr m_deriving, semi)
>  = "\\IGLhaskellnewtype{" ++ context ++ "}"
>                    ++ "{" ++ pp_Simpletype simpletype ++ "}"
>                    ++ "{" ++ pp_Newconstr newconstr c_semi ++ "}"
>                    ++ s_deriving
>  where context = case m_context of
>                      Nothing -> ""
>                      Just c -> pp_Context c ++ " $\\Rightarrow$"
>        (s_deriving, c_semi) = case m_deriving of
>                                   Nothing -> ("", semi)
>                                   Just d -> (pp_Deriving d semi, False)
> pp_Topdecl (Topdecl_class m_scontext tycls tyvar m_cdecls, semi)
>  = "\\IGLhaskellclass{" ++ scontext ++ "}"
>                  ++ "{" ++ pp_Tycls tycls ++ "}"
>                  ++ "{" ++ pp_Tyvar tyvar ++ s_semi ++ "}"
>                  ++ cdecls
>  where scontext = case m_scontext of
>                       Nothing -> ""
>                       Just sc -> pp_Scontext sc ++ " $\\Rightarrow$"
>        (cdecls, s_semi)
>         = case m_cdecls of
>               Nothing -> ("", pp_semicolon semi)
>               Just cd ->
>                   (   "\\nopagebreak\\\\\\IGLhaskellwhere"
>                    ++ pp_Cdecls cd
>                    ++ is_visible semi,
>                    "")
> pp_Topdecl (Topdecl_instance m_scontext qtycls inst m_idecls, semi)
>  = "\\IGLhaskellinstance{" ++ scontext ++ "}"
>                     ++ "{" ++ pp_Qtycls qtycls ++ "}"
>                     ++ "{" ++ pp_Inst inst ++ s_semi ++ "}"
>                     ++ idecls
>  where scontext = case m_scontext of
>                       Nothing -> ""
>                       Just sc -> pp_Scontext sc ++ " $\\Rightarrow$"
>        (idecls, s_semi)
>         = case m_idecls of
>               Nothing -> ("", pp_semicolon semi)
>               Just ids -> (   "\\nopagebreak\\\\\\IGLhaskellwhere"
>                            ++ pp_Idecls ids
>                            ++ is_visible semi,
>                            "")
> pp_Topdecl (Topdecl_default types, semi) = "\\IGLhaskelldefault{("
>                                         ++ pp_type_list types
>                                         ++ ")" ++ pp_semicolon semi ++ "}"
> pp_Topdecl (Topdecl_decl decl, semi) = pp_Decl (decl, semi)

> pp_type_list :: [Type] -> String
> pp_type_list = pp_comma_list pp_Type

> pp_xdecls_list :: (a -> Maybe String)
>                -> ((a, Bool)
>                -> String)
>                -> [(a, Bool)]
>                -> String
> pp_xdecls_list _ _ [] = ""
> pp_xdecls_list get_subject pp_this (tb@(t, _):tbs)
>  = join2 this_group (pp_xdecls_list get_subject pp_this rest)
>  where this_subject = get_subject t
>        (same_subject, rest)
>            = span ((\s -> (this_subject /= Just "") && (s == this_subject)) . get_subject . fst) tbs
>        this_group = header
>                  ++ combine (map pp_this (tb:same_subject))
>                  ++ footer
>        combine = if this_subject == Nothing then newline_concat else concat
>        (header, footer) = if this_subject == Nothing
>                           then ("", "")
>                           else ("\\IGLhaskellfunctions{", "}")

 join2 :: String -> String -> String
 join2 l r = l ++ r

> join2 :: String -> String -> String
> join2 "" r = r
> join2 l "" = l
> join2 l r = l ++ "\n\n" ++ r

> join :: String -> String -> String
> join "" r = r
> join l "" = l
> join l r = l ++ "\\\\\n" ++ r

> newline_concat :: [String] -> String
> newline_concat = foldr join ""

 newline_concat [] = ""
 newline_concat (s:ss)
  | s    == "" = rest
  | rest == "" = s
  | otherwise  = s ++ "\\\\\n" ++ rest
  where rest = newline_concat ss

> pp_Decls :: Decls -> String
> pp_Decls (Decls b decls)
>  =    is_visible b
>    ++ "{" ++ pp_xdecls_list get_subject_Decl pp_Decl (collapse decls) ++ "}"

> pp_Decl :: (Decl, Bool) -> String
> pp_Decl (Decl_gendecl gendecl, semi) = pp_Gendecl gendecl semi
> pp_Decl (Decl_fun funlhs rhs, semi) = pp_Funlhs funlhs ++ pp_Rhs rhs semi
> pp_Decl (Decl_pat0 pat_i rhs, semi) = pp_Pat_i pat_i ++ pp_Rhs rhs semi

> pp_Cdecls :: Cdecls -> String
> pp_Cdecls (Cdecls b cdecls)
>  =    is_visible b
>    ++ "{"
>    ++ pp_xdecls_list get_subject_Cdecl pp_Cdecl (collapse cdecls)
>    ++ "}"

> pp_Cdecl :: (Cdecl, Bool) -> String
> pp_Cdecl (Cdecl_gendecl gendecl, semi) = pp_Gendecl gendecl semi
> pp_Cdecl (Cdecl_fun funlhs rhs, semi) = pp_Funlhs funlhs ++ pp_Rhs rhs semi
> pp_Cdecl (Cdecl_var var rhs, semi) = pp_Var var ++ pp_Rhs rhs semi

> pp_Idecls :: Idecls -> String
> pp_Idecls (Idecls b idecls)
>  =    is_visible b
>    ++ "{"
>    ++ pp_xdecls_list get_subject_Idecl pp_Idecl (collapse idecls)
>    ++ "}"

> pp_Idecl :: (Idecl, Bool) -> String
> pp_Idecl (Idecl_fun funlhs rhs, semi) = pp_Funlhs funlhs ++ pp_Rhs rhs semi
> pp_Idecl (Idecl_var var rhs, semi) = pp_Var var ++ pp_Rhs rhs semi
> pp_Idecl (Idecl_empty, semi) = pp_semicolon semi

> pp_Gendecl :: Gendecl -> Bool -> String
> pp_Gendecl (Gendecl_type vars m_context t) semi
>  =    pp_Vars vars
>    ++ "\\IGLhaskelltyperhs{"
>    ++ context ++ pp_Type t ++ pp_semicolon semi
>    ++ "}"
>  where context = case m_context of
>                      Nothing -> ""
>                      Just c -> pp_Context c ++ " $\\Rightarrow$ "
> pp_Gendecl (Gendecl_fixity fixity m_int ops) semi
>  = snconcat [pp_Fixity fixity, doc_int, pp_Ops ops] ++ pp_semicolon semi
>  where doc_int = case m_int of
>                      Nothing -> ""
>                      Just i -> i
> pp_Gendecl Gendecl_empty semi = pp_semicolon semi

> pp_Ops :: Ops -> String
> pp_Ops (Ops []) = error "pp_Ops: This can't happen! ("" Ops list)"
> pp_Ops (Ops ops) = pp_comma_list pp_Op ops

> pp_Vars :: Vars -> String
> pp_Vars (Vars []) = error "pp_Vars: This can't happen! ("" Vars list)"
> pp_Vars (Vars vars) = pp_comma_list pp_Var vars

> pp_Fixity :: Fixity -> String
> pp_Fixity Fixity_infixl = "\\IGLhaskellkeyword{infixl}"
> pp_Fixity Fixity_infixr = "\\IGLhaskellkeyword{infixr}"
> pp_Fixity Fixity_infix  = "\\IGLhaskellkeyword{infix}"

> pp_Type :: Type -> String
> pp_Type (Type btype Nothing) = pp_Btype btype
> pp_Type (Type btype (Just t)) = sconcat [pp_Btype btype,
>                                          "$\\rightarrow$",
>                                          pp_Type t]

> pp_Btype :: Btype -> String
> pp_Btype (Btype atypes) = sconcat (map pp_Atype atypes)

> pp_Atype :: Atype -> String
> pp_Atype (Atype_gtycon gtycon) = pp_Gtycon gtycon
> pp_Atype (Atype_tyvar tyvar) = pp_Tyvar tyvar
> pp_Atype (Atype_tuple types) = parens $ pp_comma_list pp_Type types
> pp_Atype (Atype_list t) = brackets $ pp_Type t
> pp_Atype (Atype_paren t) = parens $ pp_Type t

> pp_Gtycon :: Gtycon -> String
> pp_Gtycon (Gtycon_qtycon qtycon) = pp_Qtycon qtycon
> pp_Gtycon Gtycon_unit = "()"
> pp_Gtycon Gtycon_list = "[\\,]"
> pp_Gtycon Gtycon_function = parens "$\\rightarrow$"
> pp_Gtycon (Gtycon_tupling n) = parens $ text $ replicate n ','

> pp_Context :: Context -> String
> pp_Context (Context c) = pp_Class c
> pp_Context (Context_list classes) = parens $ pp_comma_list pp_Class classes

> pp_Class :: Class -> String
> pp_Class (Class qtycls tyvar) = pp_Qtycls qtycls <+> pp_Tyvar tyvar
> pp_Class (Class_list qtycls tyvar atypes)
>  = pp_Qtycls qtycls <+> parens (sconcat (pp_Tyvar tyvar:map pp_Atype atypes))

> pp_Scontext :: Scontext -> String
> pp_Scontext (Scontext simpleclass) = pp_Simpleclass simpleclass
> pp_Scontext (Scontext_list simpleclasses)
>  = parens $ pp_comma_list pp_Simpleclass simpleclasses

> pp_Simpleclass :: Simpleclass -> String
> pp_Simpleclass (Simpleclass qtycls tyvar)
>  = pp_Qtycls qtycls <+> pp_Tyvar tyvar

> pp_Simpletype :: Simpletype -> String
> pp_Simpletype (Simpletype tycon tyvars)
>  = sconcat (pp_Tycon tycon:map pp_Tyvar tyvars)

> pp_Constrs :: Constrs -> Bool -> String
> pp_Constrs (Constrs constrs) semi = pp_Constr_list False semi constrs

> pp_Constr_list :: Bool -> Bool -> [Constr] -> String
> pp_Constr_list pipe _ []
>  | pipe      = []
>  | otherwise = error "Empty constr list"
> pp_Constr_list pipe semi (c:cs)
>  | pipe      = "\\IGLhaskellconstr{" ++ this ++ "}" ++ rest
>  | otherwise = "\\IGLhaskellconstrfirst{" ++ this ++ "}" ++ rest
>  where s_semi = case cs of
>                     [] -> pp_semicolon semi
>                     _ -> ""
>        this = pp_Constr c ++ s_semi
>        rest = pp_Constr_list True semi cs

> pp_Constr :: Constr -> String
> pp_Constr (Constr_atype con bas) = sconcat (pp_Con con:map docify bas)
>  where docify (bang, atype) = (if bang then char '!' else "")
>                            ++ pp_Atype atype
> pp_Constr (Constr_infix eba1 conop eba2)
>  = doc_eba1 <+> pp_Conop conop <+> doc_eba2
>  where doc_eba1 = case eba1 of
>                       Left btype -> pp_Btype btype
>                       Right atype -> char '!' ++ pp_Atype atype
>        doc_eba2 = case eba2 of
>                       Left btype -> pp_Btype btype
>                       Right atype -> char '!' ++ pp_Atype atype
> pp_Constr (Constr_field con fds)
>  = pp_Con con <+> braces (pp_comma_list pp_Fielddecl fds)

> pp_Newconstr :: Newconstr -> Bool -> String
> pp_Newconstr (Newconstr_atype con atype) semi
>  = pp_Con con ++ " " ++  pp_Atype atype ++ pp_semicolon semi
> pp_Newconstr (Newconstr_var_type con var t) semi
>  =    pp_Con con ++ " "
>    ++ braces (pp_Var var ++ text " :: " ++ pp_Type t ++ pp_semicolon semi)

> pp_Fielddecl :: Fielddecl -> String
> pp_Fielddecl (Fielddecl vars eta) = pp_Vars vars <+> text "::" <+> doc_eta
>  where doc_eta = case eta of
>                      Left t -> pp_Type t
>                      Right atype -> char '!' ++ pp_Atype atype

> pp_Deriving :: Deriving -> Bool -> String
> pp_Deriving (Deriving dclass) semi
>  = "\\IGLhaskellderiving{" ++ pp_Dclass dclass ++ pp_semicolon semi ++ "}"
> pp_Deriving (Deriving_list dclasses) semi
>  =    "\\IGLhaskellderiving{"
>    ++ parens (pp_comma_list pp_Dclass dclasses)
>    ++ pp_semicolon semi
>    ++ "}"

> pp_Dclass :: Dclass -> String
> pp_Dclass (Dclass qtycls) = pp_Qtycls qtycls

> pp_Inst :: Inst -> String
> pp_Inst (Inst_gtycon gtycon) = pp_Gtycon gtycon
> pp_Inst (Inst_gtycon_concr_list gtycon gtycons)
>  = parens $ sconcat $ pp_Gtycon gtycon:map pp_Gtycon gtycons
> pp_Inst (Inst_gtycon_list gtycon tyvars)
>  = parens $ sconcat $ pp_Gtycon gtycon:map pp_Tyvar tyvars
> pp_Inst (Inst_tuple tyvars) = parens $ pp_comma_list pp_Tyvar tyvars
> pp_Inst (Inst_list tyvar) = brackets $ pp_Tyvar tyvar
> pp_Inst (Inst_function tyvar1 tyvar2)
>  = brackets $ pp_Tyvar tyvar1 <+> "$\\rightarrow$" <+> pp_Tyvar tyvar2

> pp_Funlhs :: Funlhs -> String
> pp_Funlhs (Funlhs_var var apats) = sconcat $ pp_Var var:map pp_Apat apats
> pp_Funlhs (Funlhs_pat pat_i1 varop pat_i2)
>  = sconcat [pp_Pat_i pat_i1, pp_Varop varop, pp_Pat_i pat_i2]
> pp_Funlhs (Funlhs_lpat lpat_i varop pat_i)
>  = sconcat [pp_Lpat_i lpat_i, pp_Varop varop, pp_Pat_i pat_i]
> pp_Funlhs (Funlhs_rpat pat_i varop rpat_i)
>  = sconcat [pp_Pat_i pat_i, pp_Varop varop, pp_Rpat_i rpat_i]
> pp_Funlhs (Funlhs_apats funlhs apats)
>  = sconcat $ resizing_parens (pp_Funlhs funlhs):map pp_Apat apats

> pp_Rhs :: Rhs -> Bool -> String
> pp_Rhs (Rhs_exp e m_decls) semi
>  = "\\IGLhaskelleqrhs{" ++ pp_Exp e ++ s_semi ++ "}" ++ doc_decls
>  where (doc_decls, s_semi)
>         = case m_decls of
>               Nothing -> ("", pp_semicolon semi)
>               Just decls -> ("\\IGLhaskellinsidefunctions{\\IGLhaskellwhere"
>                              ++ pp_Decls decls ++ is_visible semi ++ "}",
>                              "")
> pp_Rhs (Rhs_gdrhs gdrhs m_decls) semi
>  = "\\IGLhaskellguards{" ++ pp_Gdrhs gdrhs g_semi ++ "}" ++ doc_decls
>  where (doc_decls, g_semi)
>         = case m_decls of
>               Nothing -> ("", semi)
>               Just decls -> ("\\IGLhaskellinsidefunctions{\\IGLhaskellwhere"
>                              ++ pp_Decls decls ++ is_visible semi ++ "}",
>                              False)

> pp_Gdrhs :: Gdrhs -> Bool -> String
> pp_Gdrhs (Gdrhs gd e m_gdrhs) semi
>  =    pp_Gd gd ++ "\\IGLhaskelleqrhs{" ++ pp_Exp e ++ s_semi ++ "}"
>    ++ doc_gdrhs
>  where (doc_gdrhs, s_semi) = case m_gdrhs of
>                                  Nothing -> ("", pp_semicolon semi)
>                                  Just gdrhs -> (pp_Gdrhs gdrhs semi, "")

> pp_Gd :: Gd -> String
> pp_Gd (Gd exp_i) = "\\IGLhaskellguard{" ++ pp_Exp_i exp_i ++ "}"

> pp_Exp :: Exp -> String
> pp_Exp (Exp_sig exp_i m_context t)
>  = snconcat [pp_Exp_i exp_i, "::", doc_context, pp_Type t]
>  where doc_context = case m_context of
>                          Nothing -> ""
>                          Just context -> pp_Context context
>                                      <+> "$\\Rightarrow$"
> pp_Exp (Exp exp_i) = pp_Exp_i exp_i

> pp_Exp_i :: Exp_i -> String
> pp_Exp_i (Exp_i_exp exp_i Nothing) = pp_Exp_i exp_i
> pp_Exp_i (Exp_i_exp exp_i1 (Just (qop, exp_i2)))
>  = pp_Exp_i exp_i1 <+> pp_Qop qop <+> pp_Exp_i exp_i2
> pp_Exp_i (Exp_i_lexp lexp_i) = pp_Lexp_i lexp_i
> pp_Exp_i (Exp_i_rexp rexp_i) = pp_Rexp_i rexp_i
> pp_Exp_i (Exp_i_10 exp_10) = pp_Exp_10 exp_10

> pp_Lexp_i :: Lexp_i -> String
> pp_Lexp_i (Lexp_i_l lexp_i qop exp_i)
>  = pp_Lexp_i lexp_i <+> pp_Qop qop <+> pp_Exp_i exp_i
> pp_Lexp_i (Lexp_i_e exp_i1 qop exp_i2)
>  = pp_Exp_i exp_i1 <+> pp_Qop qop <+> pp_Exp_i exp_i2
> pp_Lexp_i (Lexp_i_neg exp_i) = char '-' ++ pp_Exp_i exp_i

> pp_Rexp_i :: Rexp_i -> String
> pp_Rexp_i (Rexp_i_e exp_i1 qop exp_i2)
>  = pp_Exp_i exp_i1 <+> pp_Qop qop <+> pp_Exp_i exp_i2
> pp_Rexp_i (Rexp_i_r exp_i qop rexp_i)
>  = pp_Exp_i exp_i <+> pp_Qop qop <+> pp_Rexp_i rexp_i

> pp_Exp_10 :: Exp_10 -> String
> pp_Exp_10 (Exp_10_lambda apats e)
>  =     lambda ++ sconcat (map pp_Apat apats)
>    <+> "$\\rightarrow$" <+> pp_Exp e
> pp_Exp_10 (Exp_10_let decls e)
>  =     "\\IGLhaskellletin" ++ pp_Decls decls
>     ++ "{" ++ pp_Exp e ++ "}"
> pp_Exp_10 (Exp_10_if guard t f)
>  = "\\IGLhaskellif{" ++ pp_Exp guard ++ "}"
>               ++ "{" ++ pp_Exp t ++ "}"
>               ++ "{" ++ pp_Exp f ++ "}"
> pp_Exp_10 (Exp_10_case e bool alts)
>  =     "\\IGLhaskellcase{" ++ pp_Exp e ++ "}"
>     ++ is_visible bool
>     ++ "{" ++ pp_Alts alts ++ "}"
> pp_Exp_10 (Exp_10_do bool stmts)
>  = "\\IGLhaskelldo{" ++ show (fromEnum bool)
>              ++ "}{" ++ pp_Stmts stmts ++ "}"
> pp_Exp_10 (Exp_10_fexp fexp) = pp_Fexp fexp

> pp_Fexp :: Fexp -> String
> pp_Fexp (Fexp aexps) = sconcat $ map pp_Aexp aexps

> pp_Aexp :: Aexp -> String
> pp_Aexp (Aexp_qvar qvar) = pp_Qvar qvar
> pp_Aexp (Aexp_gcon gcon) = pp_Gcon gcon
> pp_Aexp (Aexp_literal literal) = pp_Literal literal
> pp_Aexp (Aexp_paren e) = resizing_parens $ pp_Exp e
> pp_Aexp (Aexp_tuple exps) = resizing_parens $ pp_comma_list pp_Exp exps
> pp_Aexp (Aexp_list exps) = resizing_brackets $ pp_comma_list pp_Exp exps
> pp_Aexp (Aexp_arith e1 m_e2 m_e3)
>  = resizing_brackets (snconcat [pp_Exp e1, doc_e2, "\\twodots", doc_e3])
>  where doc_e2 = case m_e2 of
>                     Just e2 -> char ',' ++ pp_Exp e2
>                     Nothing -> ""
>        doc_e3 = case m_e3 of
>                     Just e3 -> pp_Exp e3
>                     Nothing -> ""
> pp_Aexp (Aexp_listcomp e quals)
>  = resizing_brackets (pp_Exp e <+> char '|' <+> pp_comma_list pp_Qual quals)
> pp_Aexp (Aexp_left exp_i qop) = resizing_parens (pp_Exp_i exp_i <+> pp_Qop qop)
> pp_Aexp (Aexp_right qop exp_i) = resizing_parens (pp_Qop qop <+> pp_Exp_i exp_i)
> pp_Aexp (Aexp_qcon qcon fbinds)
>  = pp_Qcon qcon <+> braces (pp_comma_list pp_Fbind fbinds)
> pp_Aexp (Aexp_update aexp fbindss)
>  = pp_Aexp aexp <+> sconcat (map do_fbinds fbindss)
>  where do_fbinds fbinds = braces (pp_comma_list pp_Fbind fbinds)

> pp_Qual :: Qual -> String
> pp_Qual (Qual_generator pat e) = pp_Pat pat <+> "$\\leftarrow$"
>                                               <+> pp_Exp e
> pp_Qual (Qual_let decls) = "\\IGLhaskelllet" ++ pp_Decls decls
> pp_Qual (Qual_exp e) = pp_Exp e

> pp_Alts :: Alts -> String
> pp_Alts (Alts alts) = concat (map pp_Alt (collapse alts))

> collapse :: [Either Bool a] -> [(a, Bool)]
> collapse (Right r:Left l:xs) = (r, l):collapse xs
> collapse [Right r] = [(r, False)]
> collapse _ = error "collapse bad list"

> -- XXX missing case Alt_gdpat
> pp_Alt :: (Alt, Bool) -> String
> pp_Alt (Alt_exp pat e m_decls, semi)
>  = "\\IGLhaskellalt{" ++ pp_Pat pat ++ "}"
>                ++ "{" ++ pp_Exp e ++ doc_decls ++ pp_semicolon g_semi ++ "}"
>  where (doc_decls, g_semi)
>         = case m_decls of
>               Nothing -> ("", semi)
>               Just decls -> (   "\\IGLhaskellinsidealts{"
>                              ++ "\\IGLhaskellwhere"
>                              ++ pp_Decls decls ++ is_visible semi ++ "}",
>                              False)
> pp_Alt (Alt_gdpat pat gdpat m_decls, semi)
>  = "&" ++ pp_Pat pat <+> pp_Gdpat gdpat ++ doc_decls ++ pp_semicolon g_semi
>  where (doc_decls, g_semi)
>         = case m_decls of
>               Nothing -> ("", semi)
>               Just decls -> (   "\\IGLhaskellinsidealts{"
>                              ++ "\\IGLhaskellwhere"
>                              ++ pp_Decls decls ++ is_visible semi ++ "}",
>                              False)
> pp_Alt (Alt_empty, semi) = pp_semicolon semi

> pp_Gdpat :: Gdpat -> String
> pp_Gdpat (Gdpat gd e m_gdpat)
>  = "\\\\&" ++ pp_Gd gd <+> "$\\rightarrow$" <+> pp_Exp e ++ doc_gdpat
>  where doc_gdpat = case m_gdpat of
>                        Nothing -> ""
>                        Just gdpat -> pp_Gdpat gdpat

> pp_Stmts :: Stmts -> String
> pp_Stmts (Stmts stmts e semi) = s_stmts ++ s_exp ++ s_semi
>  where do_stmt = (\s -> "\\IGLhaskelldostmt{" ++ s ++ "}")
>        s_stmts = concat $ map (do_stmt . pp_Stmt) stmts
>        s_exp = do_stmt (pp_Exp e)
>        s_semi = case semi of
>                     Just s -> pp_semicolon s
>                     Nothing -> ""

> pp_Stmt :: Stmt -> String
> pp_Stmt (Stmt_exp e semi) = pp_Exp e ++ pp_semicolon semi
> pp_Stmt (Stmt_pat pat e semi) = pp_Pat pat
>                                ++ " $\\leftarrow$ "
>                                ++ pp_Exp e
>                                ++ pp_semicolon semi
> pp_Stmt (Stmt_let decls semi) = "\\IGLhaskelllet" ++ pp_Decls decls
>                              ++ pp_semicolon semi
> pp_Stmt (Stmt_empty semi) = pp_semicolon semi

> pp_Fbind :: Fbind -> String
> pp_Fbind (Fbind qvar e) = pp_Qvar qvar <+> char '=' <+> pp_Exp e

> pp_Pat :: Pat -> String
> pp_Pat (Pat_succ var integer) = pp_Var var <+> char '+' ++ integer
> pp_Pat (Pat pat_i) = pp_Pat_i pat_i

> pp_Pat_i :: Pat_i -> String
> pp_Pat_i (Pat_i_pat pat_i Nothing) = pp_Pat_i pat_i
> pp_Pat_i (Pat_i_pat pat_i1 (Just (qconop, pat_i2)))
>  = pp_Pat_i pat_i1 <+> pp_Qconop qconop <+> pp_Pat_i pat_i2
> pp_Pat_i (Pat_i_lpat lpat_i) = pp_Lpat_i lpat_i
> pp_Pat_i (Pat_i_rpat rpat_i) = pp_Rpat_i rpat_i
> pp_Pat_i (Pat_i_10 pat_10) = pp_Pat_10 pat_10

> pp_Lpat_i :: Lpat_i -> String
> pp_Lpat_i (Lpat_i_l lpat_i qconop pat_i)
>  = pp_Lpat_i lpat_i <+> pp_Qconop qconop <+> pp_Pat_i pat_i
> pp_Lpat_i (Lpat_i_p pat_i1 qconop pat_i2)
>  = pp_Pat_i pat_i1 <+> pp_Qconop qconop <+> pp_Pat_i pat_i2
> pp_Lpat_i (Lpat_i_6_integer integer) = char '-' ++ integer
> pp_Lpat_i (Lpat_i_6_float float) = char '-' ++ float

> pp_Rpat_i :: Rpat_i -> String
> pp_Rpat_i (Rpat_i_p pat_i1 qconop pat_i2)
>  = pp_Pat_i pat_i1 <+> pp_Qconop qconop <+> pp_Pat_i pat_i2
> pp_Rpat_i (Rpat_i_r pat_i qconop rpat_i)
>  = pp_Pat_i pat_i <+> pp_Qconop qconop <+> pp_Rpat_i rpat_i

> pp_Pat_10 :: Pat_10 -> String
> pp_Pat_10 (Pat_10_apat apat) = pp_Apat apat
> pp_Pat_10 (Pat_10_gcon gcon apats) = sconcat (pp_Gcon gcon:map pp_Apat apats)

> pp_Apat :: Apat -> String
> pp_Apat (Apat_var var m_apat) = pp_Var var ++ doc_apat
>  where doc_apat = case m_apat of
>                       Nothing -> ""
>                       Just apat -> char '@' ++ pp_Apat apat
> pp_Apat (Apat_gcon gcon) = pp_Gcon gcon
> pp_Apat (Apat_qcon qcon fpats)
>  = pp_Qcon qcon <+> braces (pp_comma_list pp_Fpat fpats)
> pp_Apat (Apat_literal literal) = pp_Literal literal
> pp_Apat Apat_wildcard = char '_'
> pp_Apat (Apat_paren pat) = parens (pp_Pat pat)
> pp_Apat (Apat_tuple pats) = parens (pp_comma_list pp_Pat pats)
> pp_Apat (Apat_list pats) = brackets (pp_comma_list pp_Pat pats)
> pp_Apat (Apat_irrefutable apat) = char '~' ++ pp_Apat apat

> pp_Fpat :: Fpat -> String
> pp_Fpat (Fpat qvar pat) = pp_Qvar qvar <+> char '=' <+> pp_Pat pat

> pp_Gcon :: Gcon -> String
> pp_Gcon Gcon_unit = "()"
> pp_Gcon Gcon_list = "[\\,]"
> pp_Gcon (Gcon_tuple n) = parens $ text $ replicate n ','
> pp_Gcon (Gcon_qcon qcon) = pp_Qcon qcon

> pp_Var :: Var -> String
> pp_Var (Var_varid varid) = pp_VarID varid
> pp_Var (Var_varsym varsym) = parens (pp_VarSym varsym)

> pp_Qvar :: Qvar -> String
> pp_Qvar (Qvar_qvarid qvarid) = pp_QVarID qvarid
> pp_Qvar (Qvar_qvarsym qvarsym) = parens (pp_QVarSym qvarsym)

> pp_Con :: Con -> String
> pp_Con (Con_conid conid) = pp_ConID conid
> pp_Con (Con_consym consym) = parens (pp_ConSym consym)

> pp_Qcon :: Qcon -> String
> pp_Qcon (Qcon_qconid qconid) = pp_QConID qconid
> pp_Qcon (Qcon_gconsym gconsym) = parens (pp_Gconsym gconsym)

> pp_Varop :: Varop -> String
> pp_Varop (Varop_varid varid) = char '`' ++ pp_VarID varid ++ char '`'
> pp_Varop (Varop_varsym varsym) = pp_VarSym varsym

> pp_Qvarop :: Qvarop -> String
> pp_Qvarop (Qvarop_qvarid qvarid) = char '`' ++ pp_QVarID qvarid ++ char '`'
> pp_Qvarop (Qvarop_qvarsym qvarsym) = pp_QVarSym qvarsym

> pp_Conop :: Conop -> String
> pp_Conop (Conop_conid conid) = char '`' ++ pp_ConID conid ++ char '`'
> pp_Conop (Conop_consym consym) = pp_ConSym consym

> pp_Qconop :: Qconop -> String
> pp_Qconop (Qconop_qconid qconid) = char '`' ++ pp_QConID qconid ++ char '`'
> pp_Qconop (Qconop_gconsym gconsym) = pp_Gconsym gconsym

> pp_Op :: Op -> String
> pp_Op (Op_varop varop) = pp_Varop varop
> pp_Op (Op_conop conop) = pp_Conop conop

> pp_Qop :: Qop -> String
> pp_Qop (Qop_qvarop qvarop) = pp_Qvarop qvarop
> pp_Qop (Qop_qconop qconop) = pp_Qconop qconop

> pp_Gconsym :: Gconsym -> String
> pp_Gconsym Gconsym_cons = "\\unskip " ++ char ':' ++ "\\ignorespaces "
> pp_Gconsym (Gconsym_qconsym qconsym) = " " ++ pp_QConSym qconsym ++ " "

