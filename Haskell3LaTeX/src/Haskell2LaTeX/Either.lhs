
> module Haskell2LaTeX.Either (get_merge_rights, do_rights, Input,
> 		get_end_state, get_rights, is_right, is_left, last_is_left,
> 		mapAccumL_right) where

> import Common.Position

> type Input a = [Either String (String, a, Position)]

> get_rights :: [Either a b] -> [b]
> get_rights [] = []
> get_rights (Left _:es) = get_rights es
> get_rights (Right r:es) = r:get_rights es

> do_rights :: (a -> Either String b) -> Input a -> (Input b, [String])
> do_rights f inp = drop_fst (mapAccumL_right f' () inp)
>  where f' = (\_ (x, _) -> case f x of
>                               Left l -> Left l
>                               Right r -> Right ((), r))
>        drop_fst (_, x, y) = (x, y)

> get_end_state :: (a -> b -> a) -> a -> Input b -> a
> get_end_state f e inp = take_fst (mapAccumL_right f' e inp)
>  where f' = (\s (x, _) -> Right (f s x, x))
>        take_fst (x, _, _) = x

> mapAccumL_right :: (a -> (b, Position) -> Either String (a, c))
>                 -> a
>                 -> Input b
>                 -> (a, Input c, [String])
> mapAccumL_right _ e [] = (e, [], [])
> mapAccumL_right f e (Left l:xs) = (e', Left l:rest, errors)
>  where (e', rest, errors) = mapAccumL_right f e xs
> mapAccumL_right f e (Right (s, thing, p):xs) = (e'', this:rest, errors')
>  where (e', this, errors') = case f e (thing, p) of
>                         Right (state, t) -> (state, Right (s, t, p), errors)
>                         Left l -> (e, Left (as_orig s), l:errors)
>        (e'', rest, errors) = mapAccumL_right f e' xs

> as_orig :: String -> String
> as_orig s = "\\begin{code}\n" ++ s ++ "\\end{code}\n"

> get_merge_rights :: [Either a (b, [c], d)] -> [c]
> get_merge_rights [] = []
> get_merge_rights (Left _:es) = get_merge_rights es
> get_merge_rights (Right (_, r, _):es) = r ++ get_merge_rights es

> is_right :: Either a b -> Bool
> is_right (Left _) = False
> is_right (Right _) = True

> is_left :: Either a b -> Bool
> is_left (Right _) = False
> is_left (Left _) = True

> last_is_left :: [Either a b] -> Bool
> last_is_left [] = False
> last_is_left xs = (is_left . last) xs

