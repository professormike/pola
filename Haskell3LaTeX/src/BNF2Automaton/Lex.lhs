
> module BNF2Automaton.Lex (scan) where

> import Prelude hiding ((<$>), (<*>), (<$), (<*))
> import Data.Char
> import Common.PC
> import Common.Positionise
> import BNF2Automaton.Token

> scan :: [(Char, Position)] -> [(Token, Position)]
> scan cps
>  | success && null rest = ret_val this
>  | otherwise            = error ("Lexer failed at " ++ show_pos fail)
>   where this = (filter ((/=) Whitespace . fst) <$> scan_top) cps
>         success = parse_succeeded this
>         rest = rest_of_input this
>         fail = fail_pos this

> scan_top :: Parser Char [(Token, Position)]
> scan_top = pList 0 (keep_pos (lexeme <|> whitespace))

> whitespace :: Parser Char Token
> whitespace = Whitespace <$  pList 1 (pPred isSpace)
>      <|>     Whitespace
>          <$  pSym '#'
>          <*  pList 0 (pPred ('\n' /=))

> lexeme :: Parser Char Token
> lexeme = comma <|> pipe <|> star <|> question <|> arrow <|> bracket
>      <|> link <|> literal <|> number <|> equals <|> equiv

> equiv :: Parser Char Token
> equiv = Equiv <$  pSyms "==" <*> pList 0 (pPred ('\n' /=))

> equals :: Parser Char Token
> equals = Equals <$  pSym '='

> pipe :: Parser Char Token
> pipe = Pipe <$  pSym '|'

> comma :: Parser Char Token
> comma = Comma <$  pSym ','

> star :: Parser Char Token
> star = Star <$  pSym '*'

> question :: Parser Char Token
> question = Question <$  pSym '?'

> arrow :: Parser Char Token
> arrow = Arrow <$  pSyms "->"

> bracket :: Parser Char Token
> bracket = OpenParen <$  pSym '('
>       <|> CloseParen <$  pSym ')'
>       <|> OpenSq <$  pSym '['
>       <|> CloseSq <$  pSym ']'
>       <|> OpenBrace <$  pSym '{'
>       <|> CloseBrace <$  pSym '}'

> link :: Parser Char Token
> link = Link
>    <$  pSym '\''
>    <*> pList 1 (in_string '\'')
>    <*  pSym '\''

> in_string :: Char -> Parser Char Char
> in_string c = pPred (c /=)
>           <|> (id <$  pSym '\\' <*> pPred (const True))

> literal :: Parser Char Token
> literal = Literal
>       <$  pSym '"'
>       <*> pList 1 (in_string '"')
>       <*  pSym '"'

> number :: Parser Char Token
> number = (Number . read)
>      <$  pSym '<'
>      <*> pList 1 (pPred isDigit)
>      <*  pSym '>'

