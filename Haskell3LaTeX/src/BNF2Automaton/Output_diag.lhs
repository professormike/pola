
> module BNF2Automaton.Output_diag (output_diag) where

> import Control.Monad
> import Data.List
> import Common.Args
> import Common.Tree

> output_diag :: Config -> Tree -> IO ()
> output_diag config (Tree _ ds) = do foldM f 0 $ map def ds
>                                     return ()
>   where full_prefix = out_dir config ++ "/" ++ prefix config
>         f :: Integer -> String -> IO Integer
>         f i s = do let file = full_prefix ++ show i ++ ".lout"
>                    writeFile file s
>                    putStrLn ("Written " ++ file)
>                    return (i + 1)

> def :: Def -> String
> def (Def _ s e) = "@SysInclude{diag}\n\
>                   \@SysInclude{eq}\n\
>                   \@SysInclude{picture}\n\n\
>                   \@Illustration {\n\
>                   \@Diag\n\
>                   \    aoutline { box }\n\
>                   \    afont { 0.5f }\n\
>                   \    boutline { curvebox }\n\
>                   \    bfont { 0.5f }\n\
>                   \    coutline { circle }\n\
>                   \    cfont { 0.5f }\n\
>                   \    cmargin { 0.3f }\n\
>                   \{\n\
>                   \@PP\n\
>                   \@CurveBox font { 0.5f Italic } @Eq { " ++ s ++ " }\n\
>                   \@Eq { { 0.5f } @Font == }\n\
>                   \@StartRight\n\
>                   \    @Sequence\n\
>                   \    A { " ++ e' ++ " }\n\
>                   \    B { @CCell \"" ++ show i ++ "\" }\n\
>                   \    @PP\n\
>                   \}\n\
>                   \}\n"
>   where (e', i) = expr 0 e

> escape :: String -> String
> escape = concat . map f
>  where f '"' = "\\\""
>        f c = [c]

List undefined XXX
XXX limits? Code sharing for choices/sequences?

> expr :: Integer -> Expr -> (String, Integer)
> expr i (LiteralE s) = (add_num i (" @ACell @F \"" ++ escape s ++ "\" "), i + 1)
> expr i (LinkE s) = (add_num i (" @BCell @Eq { " ++ s ++ " } "), i + 1)
> expr i (Seqs es) = seqs i es
> expr i (Choices es) = (add_num i (" @Select\n" ++ es'), i')
>   where (es', i') = choices (i + 1) 'A' es
> expr i (List e 0) = expr i (Optional (List e 1))
> expr i (List e 1) = (" @Repeat { " ++ append_num i' e' ++ " }\n", i' + 1)
>   where (e', i') = expr i e
> expr i (List e n) = expr i (Seqs (lead ++ end))
>   where lead = replicate (fromIntegral n - 1) e
>         end = [(List e 1)]
> expr i (SepList e s 0) = expr i (Optional (SepList e s 1))
> expr i (SepList e s 1) = (" @Loop\n\
>                           \     A { " ++ append_num i' e' ++ " }\n\
>                           \     B { " ++ s' ++ "}\n", i'')
>   where (e', i') = expr i e
>         (s', i'') = expr (i' + 1) s
> expr i (SepList e s n) = expr i (Seqs (concat lead ++ end))
>   where lead = replicate (fromIntegral n - 1) [e, s]
>         end = [(SepList e s 1)]
> expr i (Optional e) = (add_num i (" @Optional " ++ e'), i')
>   where (e', i') = expr (i + 1) e

> add_num :: Integer -> String -> String
> add_num i s = " @Sequence\n\
>               \ A { @CCell \"" ++ show_num i ++ "\" }\n\
>               \ B { " ++ s ++ " }\n"

> append_num :: Integer -> String -> String
> append_num i s = " @Sequence\n\
>                  \ A { " ++ s ++ " }\n\
>                  \ B { @CCell \"" ++ show_num i ++ "\" }\n"

> show_num :: Integer -> String
> show_num i = if length i' == 1 then " " ++ i' ++ " " else i'
>   where i' = show i

> seqs :: Integer -> [Expr] -> (String, Integer)
> seqs i [] = (add_num i " @Skip ", i + 1)
> seqs i [e] = expr i e
> seqs i (e:es) = (" @Sequence\n\
>                  \ A { " ++ e' ++ " }\n\
>                  \ B { " ++ es' ++ " }\n", i'')
>   where (e', i') = expr i e
>         (es', i'') = seqs i' es

> choices :: Integer -> Char -> [Expr] -> (String, Integer)
> choices i _ [] = ("", i)
> choices _ 'M' _ = error "Can't do this: choices _ 'M' _"
> choices i c (e:es) = ([c] ++ " { " ++ e' ++ " }\n" ++ es', i'')
>   where (e', i') = expr i e
>         c' = toEnum $ (1 +) $ fromEnum c
>         (es', i'') = choices i' c' es

