
> module BNF2Automaton.Output_code (output_code) where

> import Data.Maybe
> import Data.List
> import Common.Args
> import Common.Tokens
> import Common.Tree

Jump is:       Jump to any state in is within the same def
Long_Jump s i: Jump to another def s then return to state i of this def
SLiteral s i:  Do literal s then move to state i of current def
End:           This def is complete

> data StateChange = Jump [Integer]
>                  | Long_Jump (Either String Integer) Integer
>                  | SLiteral (Either String (Either Token Func)) Integer
>                  | End
>   deriving Show

> output_code :: Config -> Tree -> IO ()
> output_code config (Tree t ds)
>   = do header <- readFile (header_file config)
>        let dat = header ++ "    " ++ show_fm state_changes''' ++ "\n"
>        writeFile (out_file config) dat
>   where (state_changes, names) = unzip (map def ds)
>         state_changes' = concat $ zipWith delocalise [0..] state_changes
>         state_changes'' = flatten_longjumps lookup_table state_changes'
>         state_changes''' = flatten_literals t' state_changes''
>         lookup_table = zip names [0..]
>         t' = make_trans_table t

> show_fm :: [((Integer, Integer), StateChange)] -> String
> show_fm xs = "[" ++ concat (intersperse "," (map show_fmaplet xs)) ++ "]"

> show_fmaplet :: ((Integer, Integer), StateChange) -> String
> show_fmaplet (ii, sc) = "(" ++ show ii ++ "," ++ show_sc sc ++ ")"

> show_sc :: StateChange -> String
> show_sc (Long_Jump (Right i) j) = "Long_Jump " ++ show i ++ " " ++ show j
> show_sc (Long_Jump (Left _) _) = error "show_sc: (Long_Jump (Left _) _)"
> show_sc (SLiteral (Right (Right x)) i) = "SLiteral (Right (Func " ++ show x ++ " (" ++ x ++ "))) " ++ show i
> show_sc (SLiteral (Right x) i) = "SLiteral (" ++ show x ++ ") " ++ show i
> show_sc (SLiteral (Left _) _) = error "show_sc: (SLiteral (Left _) _)"
> show_sc sc = show sc

> make_trans_table :: [LitTrans] -> [(String, Either Token Func)]
> make_trans_table [] = []
> make_trans_table (TokEq s t:lts) = (s, Left t):make_trans_table lts
> make_trans_table (TokF s t:lts) = (s, Right t):make_trans_table lts

> flatten_literals :: [(String, Either Token Func)]
>                  -> [((Integer, Integer), StateChange)]
>                  -> [((Integer, Integer), StateChange)]
> flatten_literals _ [] = []
> flatten_literals t ((ii, SLiteral (Left s) i):xs) = (ii, this):rest
>   where this = SLiteral (Right (lookup_literal t s)) i
>         rest = flatten_literals t xs
> flatten_literals t (x:xs) = x:flatten_literals t xs

> flatten_longjumps :: [(String, Integer)] -> [((Integer, Integer), StateChange)] -> [((Integer, Integer), StateChange)]
> flatten_longjumps _ [] = []
> flatten_longjumps t ((ii, Long_Jump (Left s) i):xs) = (ii, this):rest
>   where this = Long_Jump (Right (lookup_longjump t s)) i
>         rest = flatten_longjumps t xs
> flatten_longjumps t (x:xs) = x:flatten_longjumps t xs

> lookup_literal :: [(String, Either Token Func)]
>                -> String
>                -> Either Token Func
> lookup_literal t s = case lookup s t of
>                          Nothing -> error ("Failed to find literal " ++ s)
>                          Just ets -> ets

> lookup_longjump :: [(String, Integer)] -> String -> Integer
> lookup_longjump t s = case lookup s t of
>                           Nothing -> error ("Failed to longjump to " ++ s)
>                           Just i -> i

> delocalise :: Integer
>            -> [(Integer, StateChange)]
>            -> [((Integer, Integer), StateChange)]
> delocalise i = map (\(x, y) -> ((i, x), y))

> def :: Def -> ([(Integer, StateChange)], String)
> def (Def _ name e) = ((i, End):iss, name)
>   where (iss, i) = expr 0 i e

> expr :: Integer -> Integer -> Expr -> ([(Integer, StateChange)], Integer)
> -- in state i you can do s to get to state j
> expr i j (LiteralE s) = ([(i, SLiteral (Left s) j)], i + 1)
> -- in state i you can jump to s coming back to j
> expr i j (LinkE s) = ([(i, Long_Jump (Left s) j)], i + 1)
> expr i j (Seqs es) = seqs i j es
> expr i j (Choices es) = ((i, Jump is):iss, i')
>   where (iss, is, i') = choices (i + 1) j es
> expr i j (List e 0) = expr i j (Optional (List e 1))
> expr i j (List e 1) = ((i', Jump [i, j]):iss, i' + 1)
>   where (iss, i') = expr i i' e
> expr i j (List e n) = expr i j (Seqs (lead ++ end))
>   where lead = replicate (fromIntegral n - 1) e
>         end = [(List e 1)]
> expr i j (SepList e s 0) = expr i j (Optional (SepList e s 1))
> expr i j (SepList e s 1) = ((i', Jump [i' + 1, j]):iss1 ++ iss2, i'')
>   where (iss1, i') = expr i i' e
>         (iss2, i'') = expr (i' + 1) i s
> expr i j (SepList e s n) = expr i j (Seqs (concat lead ++ end))
>   where lead = replicate (fromIntegral n - 1) [e, s]
>         end = [(SepList e s 1)]
> expr i j (Optional e) = ((i, Jump [i + 1, j]):iss, i')
>   where (iss, i') = expr (i + 1) j e

> seqs :: Integer -> Integer -> [Expr] -> ([(Integer, StateChange)], Integer)
> seqs i j [] = ([(i, Jump [j])], i + 1)
> seqs i j [e] = expr i j e
> seqs i j (e:es) = (iss1 ++ iss2, i'')
>   where (iss1, i') = expr i i' e
>         (iss2, i'') = seqs i' j es

> choices :: Integer -> Integer -> [Expr] -> ([(Integer, StateChange)], [Integer], Integer)
> choices i _ [] = ([], [], i)
> choices i j (e:es) = (iss1 ++ iss2, i:is, i'')
>   where (iss1, i') = expr i j e
>         (iss2, is, i'') = choices i' j es

