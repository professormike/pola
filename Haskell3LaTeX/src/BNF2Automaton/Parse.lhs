
> module BNF2Automaton.Parse (parse) where

> import Prelude hiding ((<$>), (<*>), (<$), (<*))
> import Common.PC
> import Common.Positionise
> import BNF2Automaton.Token
> import qualified Common.Tokens
> import Common.Tree

> parse :: [(Token, Position)] -> Tree
> parse tps
>  | success && null rest = ret_val this
>  | otherwise            = error ("Parser failed at " ++ show_pos fail)
>   where this = parse_top tps
>         success = parse_succeeded this
>         rest = rest_of_input this
>         fail = fail_pos this

> parse_top :: Parser Token Tree
> parse_top = Tree <$> pList 0 lit_trans <*> pList 0 def

> lit_trans :: Parser Token LitTrans
> lit_trans = equiv <|> func

> equiv :: Parser Token LitTrans
> equiv = TokEq
>     <$> literal_name
>     <*> (get_token <$> equiv_str)
>   where get_token s = fst $ head $ (reads s :: [(Common.Tokens.Token, String)])

> func :: Parser Token LitTrans
> func = TokF
>    <$> literal_name
>    <*  pSym Equals
>    <*> link_name

> def :: Parser Token Def
> def = Def
>   <$> def_start
>   <*> link_name
>   <*  pPred (Arrow ==)
>   <*> expr

> def_start :: Parser Token Bool
> def_start = True <$  pSym Star
>         <|  False <$  pSym Question

> equiv_str :: Parser Token String
> equiv_str = (\(Equiv x) -> x) <$> pPred is_equiv

> literal_name :: Parser Token String
> literal_name = (\(Literal x) -> x) <$> pPred is_literal

> link_name :: Parser Token String
> link_name = (\(Link x) -> x) <$> pPred is_link

> get_number :: Parser Token Integer
> get_number = (\(Number n) -> n) <$> pPred is_number

> expr :: Parser Token Expr
> expr = expr0
> expr0 :: Parser Token Expr
> expr0 = join
>     <$> expr1
>     <*> pList 0 (    id
>                  <$  pSym Pipe
>                  <*> expr1
>                 )
>   where join e [] = e
>         join e es = Choices (e:es)

XXX join badly named in both cases

> expr1 :: Parser Token Expr
> expr1 = join
>     <$> pList 0 expr2
>   where join [e] = e
>         join es = Seqs es

> expr2 :: Parser Token Expr
> expr2 = list <|> optional <|> brac <|> lit <|> link
> list :: Parser Token Expr
> list = join
>    <$  pSym OpenBrace
>    <*> expr
>    <*> pMaybe (    id
>                <$  pSym Comma
>                <*> expr
>               )
>    <*  pSym CloseBrace
>    <*> get_number
>   where join e1 Nothing i = List e1 i
>         join e1 (Just e2) i = SepList e1 e2 i
> optional :: Parser Token Expr
> optional = Optional
>    <$  pSym OpenSq
>    <*> expr
>    <*  pSym CloseSq
> brac :: Parser Token Expr
> brac = id
>    <$  pSym OpenParen
>    <*> expr
>    <*  pSym CloseParen
> lit :: Parser Token Expr
> lit = (\(Literal x) -> LiteralE x) <$> pPred is_literal
> link :: Parser Token Expr
> link = (\(Link x) -> LinkE x) <$> pPred is_link

> is_link :: Token -> Bool
> is_link (Link _) = True
> is_link _ = False

> is_equiv :: Token -> Bool
> is_equiv (Equiv _) = True
> is_equiv _ = False

> is_literal :: Token -> Bool
> is_literal (Literal _) = True
> is_literal _ = False

> is_number :: Token -> Bool
> is_number (Number _) = True
> is_number _ = False

