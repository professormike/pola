
> module BNF2Automaton.Token (Token(..)) where

> data Token = Star
>            | Question
>            | Arrow
>            | Comma
>            | Pipe
>            | OpenParen
>            | CloseParen
>            | OpenSq
>            | CloseSq
>            | OpenBrace
>            | CloseBrace
>            | Link String
>            | Literal String
>            | Number Integer
>            | Equiv String
>            | Equals
>            | Whitespace
>   deriving (Eq, Show)

