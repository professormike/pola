
> module Main where

> import Control.Monad
> import Common.Args
> import Common.Positionise
> import BNF2Automaton.Lex
> import BNF2Automaton.Parse
> import BNF2Automaton.Output_code
> import BNF2Automaton.Output_diag
> import System.Environment
> import Common.Tree

> main :: IO ()
> main = do c <- getContents
>           args <- getArgs
>           let config = parse_args args
>               output = case config of
>                            Code _ _ -> output_code
>                            Diags _ _ -> output_diag
>           (output config . interesting_defs . parse . scan . positionise) c

> interesting_defs :: Tree -> Tree
> interesting_defs (Tree t ds) = Tree t (filter interesting_def ds)

> interesting_def :: Def -> Bool
> interesting_def (Def b _ _) = b

