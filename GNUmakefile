HS_OPTS = -fno-warn-tabs -Werror -odir built -hidir built -isrc:built
OBJ_FILES = built/Bounds.o built/Interpreter.o built/Main.o built/Parser.o built/PatternMatching.o
OBJ_FILES += built/TypeInference.o built/Utility.o built/LaTeXPrint.o
LATEX_FILES = built/Bounds.latex built/Interpreter.latex built/Main.latex built/Parser.latex
LATEX_FILES += built/PatternMatching.latex built/TypeInference.latex built/Utility.latex

all:	compiler test doc dist/stump
	cp built/pola dist
	cp built/compiler.pdf dist
compiler:	built/pola
doc:		built/compiler.pdf
test:		compiler
	@test_success=0 && for t in Simple List BNat Coinductive BinaryTree PatternMatching Objects ; do \
		echo "testing $$t" ; \
		built/pola test/$$t.pola <test/$$t.input 2>&1 \
			| diff test/$$t.expected-output - >/dev/null \
			|| { echo '\tfailed!' ; test_success=`expr $$test_success + 1` ; } ; \
	done && [ $$test_success -eq 0 ] && \
	echo "\n******************\n all tests passed\n******************" || \
	(echo "\n!!!!!!!!!!!!!!!!!!!\n $$test_success test(s) failed \n!!!!!!!!!!!!!!!!!!!" ; false)

doc-clean:
	$(RM) built/*.latex built/compiler.*

# a couple of stupid rules to automatically create directories which aren't in the svn repository
built/stump:
	(mkdir built ; touch built/stump)
dist/stump:
	(mkdir dist ; touch dist/stump)

#
# rules used for building the compiler
#
depend:
	ghc $(HS_OPTS) -M Main -dep-makefile GNUmakefile -dep-suffix ""
built/pola:	$(OBJ_FILES) built/stump
	ghc $(HS_OPTS) -o $@ $(OBJ_FILES)
src/Parser.hs:	src/Parser.ly
	happy $<
built/%.o:	src/%.hs built/stump
	ghc -fno-warn-overlapping-patterns $(HS_OPTS) -c $<
built/%.o:	src/%.lhs built/stump
	ghc -fwarn-unused-binds $(HS_OPTS) -c $<
built/%.hi:	built/%.o built/stump
	@:
src/%.lhs:	src/%.ly lib/ly2lhs.awk
	lib/ly2lhs.awk <$< >$@

#
# rules used for building the documentation
#
built/compiler.pdf:	src/compiler.latex $(LATEX_FILES) built/stump
	ln -f lib/*.sty built
	(cd built && \
	pdflatex -interaction=nonstopmode ../$< && pdflatex -interaction=batchmode ../$<)
built/%.latex:	src/%.lhs Haskell3LaTeX/built/Haskell3LaTeX built/stump
	Haskell3LaTeX/built/Haskell3LaTeX <$< >$@
built/%.latex:	src/%.ly Haskell3LaTeX/built/Haskell3LaTeX built/stump
	lib/ly2lhs.awk <$< | Haskell3LaTeX/built/Haskell3LaTeX >$@

################################################################
# Nothing below this line is written by humans
################################################################
# (except this line, and the line of octothorpes right there ^^)

# DO NOT DELETE: Beginning of Haskell dependencies
built/Utility.o : src/Utility.lhs
built/Parser.o : src/Parser.hs
built/LaTeXPrint.o : src/LaTeXPrint.lhs
built/LaTeXPrint.o : built/Parser.hi
built/TypeInference.o : src/TypeInference.lhs
built/TypeInference.o : built/Utility.hi
built/TypeInference.o : built/Parser.hi
built/Interpreter.o : src/Interpreter.lhs
built/Interpreter.o : built/Parser.hi
built/PatternMatching.o : src/PatternMatching.lhs
built/PatternMatching.o : built/Interpreter.hi
built/PatternMatching.o : built/Utility.hi
built/PatternMatching.o : built/Parser.hi
built/Bounds.o : src/Bounds.lhs
built/Bounds.o : built/Utility.hi
built/Bounds.o : built/TypeInference.hi
built/Bounds.o : built/Parser.hi
built/Main.o : src/Main.lhs
built/Main.o : built/Bounds.hi
built/Main.o : built/PatternMatching.hi
built/Main.o : built/Interpreter.hi
built/Main.o : built/TypeInference.hi
built/Main.o : built/LaTeXPrint.hi
built/Main.o : built/Parser.hi
# DO NOT DELETE: End of Haskell dependencies
