#!/usr/bin/awk -f

# a guide to the happy_state variable:
# 	straight Haskell code. pass on verbatim to Haskell3LaTeX
# 0	finished the preamble
# 01	printing the grammar (finished the preamble)
# 010	waiting to start a new nonterminal
# 011	printing out the rule of a nonterminal
# 012	printing out a multi-line attribute for a rule
# 1	still in the preamble
# 10	dealing with operator precedences
# 11	printing the list of tokens (in the preamble of the grammar)

BEGIN {		is_haskell = 0; is_happy = 0
		happy_state = ""; happy_env = ""; first_line = 0 }
# in Literate Happy, the line "> }" ends Haskell code
/^> *}/ {	is_haskell = 0; print "" }
is_haskell {	print $0 }
# in Literate Happy, the line "> {" introduces Haskell code
/^> *{/ {	is_haskell = 1
		happy_state = ""
		if (is_happy) {
			printf("\\end{%s}\n", happy_env)
			is_happy = 0
		} }
!is_haskell && $1 == ">" && $2 != "}" && happy_state !~ /^0/ {
			if (!is_happy) {
				if ($2 == "")
					print ""
				else {
					if ($2 == "%left" || $2 == "%right")
						happy_state = "10"
					else if ($2 == "%token")
						happy_state = "11"
					if (happy_state ~ /^1/)
						happy_env = "tabular"
					else
						happy_env = "verbatim"
					printf("\\begin{%s}\n", happy_env)
					first_line = 1
					is_happy = 1
					is_haskell = 0
				}
			}
			}
is_happy && happy_state !~ /^0/ && /^> *[^}]/,/^$/ {
			if (is_happy && $1 ~ ">") {
	if (first_line) {
		first_line = 0
		if (happy_state == "10")
			start_array = "{l|l}"
		else if (happy_state == "11")
			start_array = "{ll|l}\\textbf{\\%token}"
		else
			start_array = ""
	} else
		start_array = "\\\\ "
	if ($2 == "%left" || $2 == "%right") {
		macro = "textit"
		if ($3 ~ /^'/)
			macro = "texttt"
		printf("%s\\textbf{\\%s} & \\%s{%s}\n", start_array, $2,
			macro, $3)
	} else if ($2 == "%token")
		printf("%s & &\n", start_array)
	else if (happy_state == "11" && $2 && $4) {
		macro = "textit"
		if ($2 ~ /^'/)
			macro = "texttt"
		extra = ""
		if ($5 ~ /\$\$/)
			extra = "$\\:\\diamondsuit$"
		sub(/}/, "\\}", $2)
		sub(/{/, "\\{", $2)
		printf("%s&\\%s{%s} & \\textsf{%s}%s\n", start_array, macro,
			fancy_symbol($2), $4, extra)
	} else if (happy_state !~ /^0/)
		print substr($0, 3) } }
is_happy && /^> *%%/ {	happy_state = "010" }
happy_state ~ /^01/ && /^> *[A-Z]/ {
			if (happy_state == "011" && is_happy) {
				happy_state = "010"
				print "\\end{tabular}"
			}
			is_happy = 1
			happy_env = "tabular"
			print "\\begin{tabular}{llr}"
			printf("\\multicolumn{3}{l}{%s $\\longrightarrow$}\n",
				$2)
			happy_state = "011" }
happy_state == "011" && /^>[ \t]*[:|]/ {
			n = 3
			prod = ""
			while ($n != "{" && $n != "{%" && $n) {
				gsub(/{/, "\\{", $n)
				gsub(/}/, "\\}", $n)
				gsub(/%/, "\\%", $n)
				macro = "textbf"
				if ($n ~ /[A-Z]/)
					macro = "textsf"
				else if ($n ~ /'/)
					macro = "texttt"
				prod = sprintf("%s \\%s{%s}", prod, macro, fancy_symbol($n))
				n++
			}
			if (prod)
				printf("\\\\&\\multicolumn{2}{l}{%s%s}\n",
					"$|$ ", prod)
			else
				print "\\\\&$|\\:\\varepsilon$\n"
			happy_state = "012"
			printf("\\\\& &")
			super_n = 0
			n++
			# print out each "word" of code
			while ($n !~ /}/ && $n != "") {
				print_code_word($n)
				n++
				super_n++
			}
			if ($0 ~ /}$/)
				happy_state = "011" }
happy_state == "012" && $1 ~ />/ && $2 !~ /[:|]/ {
			n = 2
			while ($n ~ /{/)
				n++
			# print out each "word" of code
			while ($n !~ /}/ && $n != "") {
				# decide to break the line or not
				if (super_n > 7 || (super_n > 2 &&
				($n == "then" || $n == "else"))) {
					super_n = 0
					printf("\\\\& &")
				}
				print_code_word($n)
				n++
				super_n++
			}
			if ($0 ~ /}$/)
				happy_state = "011" }
# if we're in happy mode and we discover a non-birded line (i.e., some
# documentation), we want to end our verbatim block.
!is_haskell && $1 != ">" {
			if (is_happy) {
				is_happy = 0
				printf("\\end{%s}\n", happy_env);
			}
			print $0 }

function fancy_symbol(s) {
	sub(/\^/, "$\\uparrow$", s)
	sub(/_/, "\\_", s)
	return s
}

function print_code_word(word) {
	printf(" ")
	gsub(/\$/, "\\$", word)
	gsub(/&/, "\\\\&", word)
	while (word ~ /^[[(]/) {	# it's an open bracket
		printf("\\texttt{%s}", substr(word, 1, 1))
		word = substr(word, 2)
	}
	trailword = ""
	while (word ~ /[]),]$/) {	# it's a close bracket or comma
		trailword = substr(word, length(word)) trailword
		word = substr(word, 1, length(word) - 1)
	}
	if (word ~ /^"/)	# it's a string literal
		printf("``%s", substr(fancy_symbol(word), 2))
	else {
		macro = "texttt"
		if (word ~ /\$/)
			macro = "textit"
		else if (word == "let" || word == "in" || word == "if" ||
		word == "then" || word == "else")
			macro = "textbf"
		else if (word ~ /^[A-Z]/)
			macro = "textsf"
		else if (word ~ /^[a-z]/)
			macro = "textit"
		else if (word ~ /^[0-9]/)
			macro = "textrm"
		printf("\\%s{%s}", macro, word)
	}
	if (trailword != "")
		printf("\\texttt{%s}", trailword)
}
