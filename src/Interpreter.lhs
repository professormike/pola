\chapter{Interpreting}

The interpreting follows the operational semantics of the language.

\section{Preamble}

\subsection{Exported symbols}

The major symbol exported by this module is \textit{interpret}, which takes a list of
function definitions and an expression and evaluates the application. We also export the
\textit{cut} symbol because it handy when calculating bounds.

Note that the interpreter module does \textbf{not} export any data types.

\begin{code}
module Interpreter (interpret, cut)
where

import Parser
import Data.List (delete)
\end{code}

\section{Cuts}

The most common operation in interpreting is variable substition. The \textit{cut}
function deals with cuts in expressions.

\begin{code}
cut sb (FunctionCall f es) = FunctionCall f $ map (cut sb) es
cut sb (Construction c es) = Construction c $ map (cut sb) es
cut sb (Destruction d es) = Destruction d $ map (cut sb) es
cut sb (Fold f'@(_, x, ys) pes coda) = Fold f' pes' coda' where
	pes' = cutPes (substMinus sb (x:ys)) pes
	coda' = cut sb coda
cut sb (Unfold f'@(_, ys) pes coda) = Unfold f' pes' coda' where
	pes' = cutPes (substMinus sb ys) pes
	coda' = cut sb coda
cut sb (Case e pes) = Case (cut sb e) (cutPes sb pes)
cut sb (Peek e pes) = Peek (cut sb e) (cutPes sb pes)
cut sb (Record pes) = Record $ cutPes sb pes
cut sb s'@(VariableName s) = case lookup s sb of
	Just e -> e
	Nothing -> s'
cut sb (TupleExpr es) = TupleExpr $ map (cut sb) es
cut sb (Where e vs) = Where (cut sb' e) $ map vCut vs where
	sb' = substMinus sb $ map getVar vs
	vCut (Left (s, e)) = Left (s, cut sb' e)
	vCut (Right (s, e)) = Right (s, cut sb' e)
	getVar (Left (s, _)) = s
	getVar (Right (s, _)) = s
cut _ e@(ErrorExpr _) = e
\end{code}

The other cut function deals with cutting into pattern-expressions, such as
those found in cases, folds, unfolds or records.

\begin{code}
cutPes sb = map (cutPe sb)
cutPe sb (p, e) = (p, cut (substMinus sb $ allVars p) e) where
	allVars (VariablePattern v1 v2) = [v1, v2]
	allVars (TuplePattern ps) = concatMap allVars ps
	allVars (ConstructorPattern _ ps) = concatMap allVars ps
	allVars _ = []
\end{code}

Finally we introduce the \textit{substMinus} function, which removes variables
from a substitution list.

\begin{code}
substMinus = foldl m where
	m sb v = case lookup v sb of
		Just _ -> filter (\ (u, _) -> u /= v) sb
		Nothing -> sb
\end{code}

\section{Interpreting}

We need a data type to keep track of function stacks.

\begin{code}
data FunctionStack
	= EmptyStack
	| Function String [String] Expression FunctionStack FunctionStack
\end{code}

The \textit{interpret} function is a thing wrapper around the unexported \textit{intrp}
function. It transforms the list of function definitions into a lookup table. There are
no documented rules for evaluating, but they do follow closely the operational semantics
that Brian drew up.

\begin{code}
interpret :: [FunctionDefinition] -> Expression -> Expression
interpret defns = intrp stack where
	stack = makeStack defns
	makeStack [] = EmptyStack
	makeStack ((f, _, os, ps, e):fs) = Function f (os ++ ps) e stack
		(makeStack fs)
\end{code}

The main function is the \textit{interpret} function, which reduces expressions in
a lazy manner.

\begin{code}
intrp :: FunctionStack -> Expression -> Expression
\end{code}

Many of the cases are easy to interpret as they are comprised solely of reducing
sub-expressions.

\begin{code}
intrp defns (Construction c es) = Construction c $ map (intrp defns) es
intrp defns (TupleExpr es) = TupleExpr $ map (intrp defns) es
\end{code}

Even a function call is a fairly simple cut.

\begin{code}
intrp defns (FunctionCall f es) = intrp defns' $ cut sbs e where
	(defns', vs, e) = lookupFunction defns
	sbs = zip vs $ map (intrp defns) es
	lookupFunction (Function g vs e stk rest)
		| f == g = (stk, vs, e)
		| otherwise = lookupFunction rest
	lookupFunction EmptyStack = error $ "Can't find " ++ f
\end{code}

As is a where clause.

\begin{code}
intrp defns (Where e vs) = intrp defns $ cut (map v vs) e where
	v (Left ve) = ve
	v (Right ve) = ve
\end{code}

To implement folds, we construct a new function definition (which has a
case expression embedded in it) and then proved a function call to the
newly constructed function.

\begin{code}
intrp defns (Fold (f, x, ys) pes coda) = intrp defns' coda where
	defns' = Function f (x:ys) (Case (VariableName x) pes) defns' defns
\end{code}

The burden is on the cases then. We must evaluate its argument to get a
constructor, then match it against the patterns provided. We then evaluate
the pursuant expression with cuts against the pattern's variables.

\begin{code}
intrp defns (Case e [(VariablePattern v1 v2, pe)]) = pe' where
	pe' = intrp defns $ cut [(v1, e), (v2, e)] pe
intrp defns (Case e [(TuplePattern ps, pe)]) = pe' where
	pvs = map (\ (VariablePattern v1 "_") -> v1) ps
	pe' = intrp defns $ cut (zip pvs es) pe
	TupleExpr es = intrp defns e
intrp defns (Case e pes) = intrp defns pe' where
	Construction c ces = case intrp defns e of
		x@(Construction _ _) -> x
		ErrorExpr e -> error $ "Fatal error: " ++ e
		x -> error $ "UH OH!! " ++ show x
	(ConstructorPattern _ ps, pe) = lookupPE c pes
	pvs = concat $ zipWith (\ (VariablePattern v1 v2) e -> [(v1, e), (v2, e)]) ps ces
	pe' = intrp defns $ cut pvs pe
intrp defns (Peek e pes) = intrp defns $ Case e pes
\end{code}

Next we consider destructions.

\begin{code}
intrp defns (Destruction d (e:es)) = intrp defns $ cut sbs e' where
	Record pes = intrp defns e
	(DestructorPattern _ vs, e') = lookupPE d pes
	sbs = zip vs $ map (intrp defns) es
\end{code}

And we have unfolds.

\begin{code}
intrp defns (Unfold (g, ys) pes coda) = intrp defns' coda where
	defns' = Function g ys (Record pes) defns' defns
\end{code}

Lastly we consider records.

\begin{code}
intrp defns (Record pes) = Record $ map doPE pes where
	doPE (DestructorPattern d vs, e) = (DestructorPattern d vs, intrp defns e)
\end{code}

We have a catch-all for cases we haven't handled yet.

\begin{code}
intrp defns e = e
\end{code}

The \textit{lookupPE} function searches through pattern-expressions for
a given construction or destructor.

\begin{code}
lookupPE s (p@(ConstructorPattern t _, _):ps)
	| s == t = p
	| otherwise = lookupPE s ps
lookupPE s (p@(DestructorPattern t _, _):ps)
	| s == t = p
	| otherwise = lookupPE s ps
\end{code}
