FIXME: make this literate?

\begin{code}
{-# OPTIONS_GHC -XFlexibleInstances #-}
module LaTeXPrint (latexPrintType, latexPrintFunction, compositionalPrintType,
	compositionalPrintFunction) where

import Data.Char (isDigit, isUpper)
import Data.List (groupBy)
import Parser

class LaTeXShow a where
	latexShow :: Integer -> a -> String
instance LaTeXShow Expression where
	latexShow n (FunctionCall f es) = variable f ++ "(" ++ (commaDelimit $
		map (latexShow $ n + 1) es) ++ ")"
	latexShow n (Construction c []) = upperIdent c
	latexShow n (Construction c es) = upperIdent c ++ "(" ++ (commaDelimit $
		map (latexShow $ n + 1) es) ++ ")"
	latexShow n (Destruction d [x]) = upperIdent d ++ "[" ++ latexShow (n + 1) x ++ "]"
	latexShow n (Destruction d (x:ps)) = upperIdent d ++ "[" ++ latexShow (n + 1) x ++ "](" ++
		(commaDelimit $ map (latexShow $ n + 1) ps) ++ ")"
	latexShow n (Fold (f, x, xs) pes coda) = branches header (n + 1) pes ++ footer where
		header = keyword "fold" ++ "~" ++
			latexShow n (FunctionCall f $ map VariableName $ x:xs) ++ "~" ++
			keyword "as"
		footer = "}\\\\\n" ++ newLine n ++ keyword "in" ++ "~" ++ latexShow (n + 1) coda
	latexShow n (Record pes) = cobranches "" (n + 1) pes where
	latexShow n (Unfold (g, xs) pes coda) = cobranches header (n + 1) pes ++ footer where
		header = keyword "unfold" ++ "~" ++
			latexShow n (FunctionCall g $ map VariableName xs) ++ "~" ++
			keyword "as"
		footer = "}\\\\\n" ++ newLine n ++ keyword "in" ++ "~" ++ latexShow (n + 1) coda
	latexShow n (Case t pes) = casePeek "case" n t pes
	latexShow n (Peek t pes) = casePeek "peek" n t pes
	latexShow _ (VariableName v) = variable v
	latexShow n (TupleExpr es) = "(" ++ (commaDelimit $ map (latexShow $ n + 1) es) ++ ")"
	latexShow n (Where e ws) = latexShow (n + 1) e ++ "~" ++ keyword "where" ++ "~\\{}\\\\\n" ++
		(delimit ";}\\\\\n" $ map (showWhere $ n + 1) ws)
	latexShow _ (ErrorExpr s) = "!!~\\mbox{``" ++ s ++ "''}~!!"
instance LaTeXShow Pattern where
	latexShow _ (VariablePattern v "_") = variable v
	latexShow _ (VariablePattern v1 v2) = variable v1 ++ "\\mid " ++ variable v2
	latexShow _ (ConstructorPattern c []) = upperIdent c
	latexShow _ (ConstructorPattern c ps) = upperIdent c ++ "(" ++ (commaDelimit $
		map (latexShow 0) ps) ++ ")"
	latexShow _ (DestructorPattern d []) = upperIdent d
	latexShow _ (DestructorPattern d ps) = upperIdent d ++ ":" ++ commaDelimit (map variable ps)

latexPrintType (Inductive (nme, vs) conses) = putStrLn $ header ++ pCs "=" conses ++ ";}" where
	header = newLine 0 ++ keyword "data" ++ "\\;" ++ upperIdent nme ++ vs' ++ "\\rightarrow c"
	vs' = case vs of
		[] -> ""
		_ -> "(" ++ commaDelimit (map variable vs) ++ ")"
	pCs _ [] = ""
	pCs h ((c, a, r):cs) = "}\\\\" ++ newLine 1 ++ h ++ "\\;" ++ upperIdent c ++ ":" ++
		commaDelimit (map pt a) ++ "\\rightarrow " ++ pt r ++
		pCs "\\mid" cs
	pt (NamedType t xs)
		| t == nme = variable "c"
		| xs == [] = ptn t
		| otherwise = ptn t ++ "(" ++ commaDelimit (map pt xs) ++ ")"
	pt (TupleType ts) = "(" ++ commaDelimit (map pt ts) ++ ")"
	pt (PolyType a) = variable a
	ptn cs@(c:_)	| isUpper c = upperIdent cs
			| otherwise = variable cs
latexPrintType (Coinductive (nme, vs) dests) = putStrLn $ header ++ pDs "=" dests ++ ";}" where
	header = newLine 0 ++ keyword "data" ++ "\\;c\\rightarrow " ++ upperIdent nme ++ vs'
	vs' = case vs of
		[] -> ""
		_ -> "(" ++ commaDelimit (map variable vs) ++ ")"
	pDs _ [] = ""
	pDs h ((c, a, r):cs) = "}\\\\" ++ newLine 1 ++ h ++ "\\;" ++ upperIdent c ++ ":" ++
		commaDelimit (map pt a) ++ "\\rightarrow " ++ commaDelimit (map pt r) ++
		pDs "\\mid" cs
	pt (NamedType t xs)
		| t == nme = variable "c"
		| xs == [] = ptn t
		| otherwise = ptn t ++ "(" ++ commaDelimit (map pt xs) ++ ")"
	pt (TupleType ts) = "(" ++ commaDelimit (map pt ts) ++ ")"
	pt (PolyType a) = variable a
	ptn cs@(c:_)	| isUpper c = upperIdent cs
			| otherwise = variable cs
latexPrintFunction (f, _, os, ps, e) = putStrLn $ header ++ latexShow 0 e ++ "}" where
	header = newLine 0 ++ variable f ++ "=" ++ os' ++ "\\mid " ++ ps' ++ "."
	os' = case os of
		[] -> "~"
		_ -> commaDelimit $ map variable os
	ps' = case ps of
		[] -> "~"
		_ -> commaDelimit $ map variable ps
\end{code}

\begin{code}
class Compositional a where
	compositionalShow :: a -> String
instance Compositional Expression where
	compositionalShow (FunctionCall f es) = "(" ++ variable f ++ compositionalShow es ++ ")"
	compositionalShow (Construction c []) = upperIdent c ++ "\\;()"
	compositionalShow (Construction c es) = upperIdent c ++ "\\;(" ++ (compositionalShow $
		TupleExpr es) ++ ")"
	compositionalShow (Fold (f, x, ys) pes coda) = "(\\lambda^f" ++ variable f ++ "." ++
			compositionalShow coda ++ ")\\;({\\bf fold}\\;(" ++ fxs ++
			".{\\bf fcase}\\;" ++ variable x ++ "\\;" ++ inductivePESShow pes ++ "))" where
		fxs = "\\lambda^f" ++ variable f ++ xs (x:ys)
		xs [] = ""
		xs (y:ys) = ".\\lambda^p" ++ variable y ++ xs ys
	compositionalShow (Case s pes) = keyword "case" ++ "\\;" ++
		inductivePESShow (makeO pes) ++ "\\;" ++ compositionalShow s
	compositionalShow (Peek s pes) = keyword "peek" ++ "\\;" ++
		inductivePESShow (makeP pes) ++ "\\;" ++ compositionalShow s
	compositionalShow (VariableName v) = variable v
	compositionalShow (TupleExpr []) = "()"
	compositionalShow (TupleExpr (e@(VariableName _):es)) = compositionalShow e ++ "\\times" ++
		compositionalShow (TupleExpr es)
	compositionalShow (TupleExpr (e:es)) = "(" ++ compositionalShow e ++ ")\\times" ++
		compositionalShow (TupleExpr es)
	compositionalShow (Where e []) = compositionalShow e
	compositionalShow (Where e (Left (v, d):ws)) = "(\\lambda^o" ++ variable v ++ "." ++
		compositionalShow (Where e ws) ++ ")\\;(" ++ compositionalShow d ++ ")"
	compositionalShow (Where e (Right (v, d):ws)) = "(\\lambda^p" ++ variable v ++ "." ++
		compositionalShow (Where e ws) ++ ")\\;(" ++ compositionalShow d ++ ")"
	compositionalShow (Record pes) = keyword "record" ++ "\\;" ++ coinductivePESShow pes
	compositionalShow (Unfold (g, xs) pes coda) = keyword "unfold" ++ "\\;" ++ gxs ++ coda' where
		gxs = "(\\lambda^{\\bar{f}}" ++ variable g ++ ys xs ++ pes' ++ ")"
		FunctionCall _ args = coda
		coda' = compositionalShow args
		ys [] = "."
		ys (x:xs) = ".\\lambda^p" ++ variable x ++ ys xs
		pes' = coinductivePESShow pes
	compositionalShow (Destruction d (e:es)) = "(" ++ compositionalShow e ++ "\\;" ++ upperIdent d ++
		compositionalShow es ++ ")"
	compositionalShow (ErrorExpr _) = "!!"
instance Compositional [Expression] where
	compositionalShow [] = ""
	compositionalShow xs = "\\;" ++ (delimit "\\;" $ map cs xs) where
		cs e@(VariableName _) = compositionalShow e
		cs e = "(" ++ compositionalShow e ++ ")"
instance Compositional Pattern where
	compositionalShow (ConstructorPattern c ps) = "\\lambda^{\\sf C}" ++ upperIdent c ++ "." ++
		concatMap (\ x -> "\\lozenge." ++ compositionalShow x ++ ".") ps ++
		"\\triangledown."
	compositionalShow (DestructorPattern d xs) = "\\lambda^{\\sf D}" ++ upperIdent d ++ "." ++
		concatMap (\ x -> "\\lambda^p" ++ x ++ ".") xs
	compositionalShow (VariablePattern v "") = "\\lambda^o" ++ variable v
	compositionalShow (VariablePattern "" v) = "\\lambda^p" ++ variable v
	compositionalShow (VariablePattern v1 v2) = "\\lambda^{\\it op}" ++ variable v1
		++ "\\," ++ variable v2

pesShow sep pes = "(" ++ delimit sep (map cs pes) ++ ")" where
	cs (p, e) = compositionalShow p ++ compositionalShow e
inductivePESShow = pesShow "+"
coinductivePESShow = pesShow "\\oplus"

compositionalPrintType _ = return ()
compositionalPrintFunction (f, _, os, ps, e) = putStrLn $ variable f ++ "=" ++ pf os ps e where
	pf [] [] e = compositionalShow e
	pf [] (p:ps) e = "\\lambda^p" ++ variable p ++ "." ++ pf [] ps e
	pf (o:os) ps e = "\\lambda^o" ++ variable o ++ "." ++ pf os ps e
\end{code}

\subsection{Helper functions}

\begin{code}
casePeek k n t pes = branches header (n + 1) pes where
	header = keyword k ++ "~" ++ latexShow (n + 2) t ++ "~" ++ keyword "of"

br start end header n pes = header ++ "~" ++ start ++ "}\\\\\n"
		++ delimit ";}\\\\\n" pes' ++ "~" ++ end where
	pes' = map (\ (p, e) -> newLine n ++
		latexShow (n + 1) p ++ "." ++ latexShow n e) pes
branches = br "\\{" "\\}"
cobranches = br "(" ")"
showWhere n (Left (v, e)) = variable v ++ ":=" ++ latexShow n e
showWhere n (Right (v, e)) = variable v ++ "=" ++ latexShow n e

makeO :: [PatternExpr] -> [PatternExpr]
makeO = map (\ (p, e) -> (mO p, e)) where
	mO (ConstructorPattern c ps) = ConstructorPattern c $ map mO ps
	mO (VariablePattern v _) = VariablePattern v ""
makeP :: [PatternExpr] -> [PatternExpr]
makeP = map (\ (p, e) -> (mP p, e)) where
	mP (ConstructorPattern c ps) = ConstructorPattern c $ map mP ps
	mP (VariablePattern _ v) = VariablePattern "" v

variable = showSymbol v where
	v s = case concat $ map r s of
		"" -> "-"
		s'@[_] -> s'
		s' -> "{\\it " ++ s' ++ "}"
	r '_' = ""
	r x = [x]
upperIdent = showSymbol (\ c -> "{\\sf " ++ c ++ "}")

showSymbol style s = case findSubscript s of
	(_, Nothing) -> style s
	(s, Just i) -> style s ++ "_{" ++ show i ++ "}"

findSubscript :: String -> (String, Maybe Integer)
findSubscript x	| isDigit zc = (concat $ reverse $ map reverse zs, Just $ read z)
		| otherwise = (x, Nothing) where
	(z@(zc:_):zs) = groupBy (\ x y -> isDigit x == isDigit y) $ reverse x

keyword k = "{\\bf " ++ k ++ "}"

newLine n = "\\" ++ zs ++ "{" where
	takeI = take . fromInteger
	zs = takeI (n + 1) $ repeat 'z'
\end{code}
