\chapter{Pattern matching}	\label{patternmatching}

A Pola program can have a complex pattern such as:
\[\begin{array}{ll}
\multicolumn{2}{l}{{\bf peek}~f(x, t)~{\bf of}~\{}\\
&(a, {\sf Cons}(-,-)).e_1;\\
&({\sf Cons}(x,{\sf Nil}),y).e_2;\\
&({\sf Cons}({\sf Cons}(x,a),-),{\sf Nil}).e_3;\\
&z.e_4~\}
\end{array}\]

The implementation needs these patterns to be expanded out into ``simple'' patterns, such as:
\[\begin{array}{lllll}
\multicolumn{5}{l}{{\bf peek}~t_1~{\bf of}~(x_1,x_2).{\bf peek}~x_1~{\bf of}~\{}\\
~&\multicolumn{4}{l}{{\sf Nil}.{\bf peek}~x_2~{\bf of}~\{}\\
&~&\multicolumn{3}{l}{{\sf Nil}.e_4~{\bf where}~z:=t_1;}\\
&&\multicolumn{3}{l}{{\sf Cons}(x_5,x_6).e_1~{\bf where}~a:=x_1~\};}\\
&\multicolumn{4}{l}{{\sf Cons}(x_3,x_4).{\bf peek}~x_2~{\bf of}~\{}\\
&&\multicolumn{3}{l}{{\sf Nil}.{\bf peek}~x_3~{\bf of}~\{}\\
&&~&\multicolumn{2}{l}{{\sf Nil}.e_2~{\bf where}~y:=x_2~{\bf where}~x:=x_3;}\\
&&&\multicolumn{2}{l}{{\sf Cons}(x_9,x_{10}).e_3~{\bf where}~\{x:=x_9;a:=x_{10}\}~\};}\\
&&\multicolumn{3}{l}{{\sf Cons}(x_7,x_8).e_1~{\bf where}~a:=x_1~\}~\}}\\
&\multicolumn{4}{l}{{\bf where}~t_1:=f(x,t)}
\end{array}\]

This module deals with expanding out pattern matches to be of the appropriate form for type
checking and interpretation or compilation.

\section{Preamble}

There is only one symbol exported from this module, the {\it expandPatterns} function.

\begin{code}
module PatternMatching (expandPatterns) where

import Parser
import Utility
import Interpreter (cut)
import Control.Monad (foldM)
import Data.Maybe (fromJust)
import Data.List
\end{code}

\subsection{Data types}	\label{matchtypes}

How we deal with patterns depends greatly on what type of pattern matching we're doing. The
four types of patterns given in section~\ref{parserfunctions} describe the four types of
pattern matching we can do. We describe three of them again here. The
{\it Unknown} type is used for matching variables. Note
that we do not need to deal with destructors since destructor matches aren't ``patterns''.

\begin{code}
data MatchType
	= UnknownMatch
	| ConstructorMatch String
	| TupleMatch Integer
	deriving Eq
\end{code}

\section{Transforming terms}

\subsection{The expandPatterns function}

First we introduce a function to recurse through the structures of terms. This is the main
entry point of the module, but no real work is done here.

The case of a variable pattern is extremely unlikely and only comes about if the programmer has
done something really foolish like {\sf case}~x~{\sf of}~z.z. Still, we must cover it as a special
case of {\it doPEs}.

What to do about folds in the general case? Good question! FIXME.

\begin{code}
expandPatterns :: [TypeDefinition] -> FunctionDefinition -> Value FunctionDefinition
expandPatterns ts (f, cs, os, ps, e) = do
		(e', _) <- ep 0 e
		return (f, cs, os, ps, e')
	where
	ep n (FunctionCall s es) = eps n es $ FunctionCall s
	ep n (Construction s es) = eps n es $ Construction s
	ep n (Destruction s es) = eps n es $ Destruction s
	ep n (Fold f@(f1, _, _) pes (FunctionCall f2 (e:es)))
		| f1 == f2 = doPEs n (\ s p -> Fold f p $ FunctionCall f2 (s:es)) Case Left e pes
		| otherwise = eps n (e:es) $ FunctionCall f2
	ep n (Fold _ _ coda) = ep n coda
	ep n (Unfold f pes coda) = do
		(pes', n1) <- reps n pes
		(coda', n2) <- ep n1 coda
		return (Unfold f pes' coda', n2)
	ep n (Record pes) = do
		(pes', n') <- reps n pes
		return (Record pes', n')
	ep n (Case e pes) = doPEs n Case Case Left e pes
	ep n (Peek e pes) = doPEs n Peek Peek Right e pes
	ep n (TupleExpr es) = eps n es $ TupleExpr
	ep n e = return (e, n)
	eps n es f = do
		(es', n') <- ips n es
		return (f es', n')
	ips n [] = return ([], n)
	ips n (e:es) = do
		(e', n1) <- ep n e
		(es', n2) <- ips n1 es
		return (e':es', n2)
	reps n [] = return ([], n)
	reps n ((p, e):pes) = do
		(e', n1) <- ep n e
		(pes', n2) <- reps n1 pes
		return ((p, e'):pes', n2)
	doPEs n _ _ whereworld subject [(VariablePattern x _, e)] =
		return (makeWhere e [whereworld (x, subject)], n)
	doPEs n construct subjconstruct whereworld subject pes = m where
		m = do
			(subject', n1) <- ep n subject
			let (tempVar, n2) = ("_subject" ++ show n1, n1 + 1)
			(es', n3) <- ips n2 es
			(pes', n4) <- doPatts n3 tempVar es'
			let subj = construct (VariableName tempVar) pes'
			return (makeWhere subj [whereworld (tempVar, subject')], n4)
		errExpr = ErrorExpr "Non-exhaustive patterns"
		(ps, es) = unzip (pes ++ [(VariablePattern "_" "_", errExpr)])
		doPatts n v es = doPatterns n ts v subjconstruct whereworld (classifyPatterns ps)
			[] [ps] es
\end{code}

\section{Expanding patterns}

\subsection{The {\it doPatterns} function}

The {\it doPatterns} function takes a list of variables ({\it vars}) of length $m$, a 2D array of
patterns ({\it vars}), of length $m$ where each column is of length $p$, and a list of expressions
({\it es}), of length $p$.

First we look at the tuple case.

\begin{code}
doPatterns n ts v construct world (TupleMatch i) vars pss es = do
		(n', e) <- strip newPs newEs vars'
		return ([(pat, e)], n') where
	strip = stripOffUnknowns ts construct Left world n1
	var1 = concat $ zipWith (\ i j -> ["_x" ++ show i, "_x" ++ show j])
		[n..n + i - 1] [n..n + i - 1]
	varSingle = dropEven var1 where
		dropEven [] = []
		dropEven (x:_:xs) = x:dropEven xs
	vars' = var1 ++ vars
	pat = TuplePattern $ map (\ v -> VariablePattern v "_") varSingle
	n1 = n + i
	(expPs, newEs) = unzip $ zipWith (s i) (transpose pss) es where
		s _ (TuplePattern vs:ps) e = (vs ++ ps, e)
		s i (VariablePattern w _:ps) e = (takeI i (repeat $ VariablePattern "_" "_") ++ ps,
			makeWhere e [world (w, VariableName v)])
	newPs = transpose expPs
\end{code}

And finally the constructor case, which will end up being the most interesting, will not
do anything.

\begin{code}
doPatterns n ts v construct world (ConstructorMatch c) vars pss es = do
		constructors' <- constructors
		(n1, conspats) <- mapAccumM genPats n constructors'
		(n', es') <- mapAccumM (esForConstructor conspats) n1 conspats
		return (zip conspats es', n') where
	mapAccumM :: (Monad m) => (a -> b -> m (a, c)) -> a -> [b] -> m (a, [c])
	mapAccumM _ x [] = return (x, [])
	mapAccumM m x (y:ys) = do
		(x1, y') <- m x y
		(x', ys') <- mapAccumM m x1 ys
		return (x', y':ys')
	constructors = case find findInType ts of
			Just (Inductive _ conses) -> return $ map (\ (s, a, _) -> (s, lengthI a)) conses
			Nothing -> fail $ "Non-existent constructor " ++ c
		where
		findInType (Inductive _ conses) = case find (\ (s, _, _) -> s == c) conses of
			Just _ -> True
			Nothing -> False
		findInType _ = False
	genPats :: Integer -> (String, Integer) -> Value (Integer, Pattern)
	genPats n (c, l) = return (n + 2 * l,
		ConstructorPattern c $
		zipWith (\ i j -> VariablePattern ("_x" ++ show i) ("_x" ++ show j)) [n..n + l - 1]
		[n + l..n + 2 * l - 1])
	esForConstructor conspats n (ConstructorPattern c vs) =
			strip newPs newEs vars' where
		newPs :: [[Pattern]]
		(expPs, newEs) = unzip $ map expandPs goodPsEs where
			expandPs (ConstructorPattern _ vs:ps, e) = (vs ++ ps, e)
			expandPs (VariablePattern w _:ps, e) = (take (length vs)
				(repeat $ VariablePattern "_" "_") ++ ps, makeWhere e
				[world (w, VariableName v)])
		newPs = transpose expPs
		Just (ConstructorPattern _ vars1) = find (\ (ConstructorPattern d _) -> d == c)
			conspats
		vars' = concatMap (\ (VariablePattern x y) -> [x, y]) vars1 ++ vars
		goodPsEs :: [([Pattern], Expression)]
		goodPsEs = filter rightConstructor $ zip (transpose pss) es where
			rightConstructor (ConstructorPattern d _:_, _) = d == c
			rightConstructor (_, _) = True
		strip = stripOffUnknowns ts construct world world n
\end{code}

\subsection{Deealing with pattern variables}

The {\it stripOffUnknowns} function avoids building a case/peek/fold construct over variable
patterns.

\begin{code}
stripOffUnknowns _ _ _ _ n [] (e:_) [] = return (n, e)
stripOffUnknowns ts construct w world n ps'@(p:ps) es (v1:v2:vs) = case classifyPatterns p of
		UnknownMatch -> strip es' vs
		m -> do
			(pes', n') <- doPatts m vs ps' es
			return (n', construct (VariableName v1) pes')
	where
	strip = stripOffUnknowns ts construct w world n ps
	doPatts = doPatterns n ts v12 construct w
	v12 = case w (v1, ErrorExpr "") of
		Left _ -> v1
		Right _ -> v2
	es' = zipWith m p es
	m (VariablePattern "_" "_") e = e
	m (VariablePattern x "_") e = makeWhere e [world (x, VariableName v1)]
	m (VariablePattern "_" x) e = makeWhere e [Right (x, VariableName v2)]
	m (VariablePattern x y) e = makeWhere e [Left (x, VariableName v1),
		Right (y, VariableName v2)]
\end{code}

\subsection{Classifying patterns}

When reconciling heterogenous patterns---e.g., matching a variable pattern with a constructor
pattern---we would like some partial order.

\begin{code}
instance Ord MatchType where
	_ <= UnknownMatch = True
	UnknownMatch <= _ = False
	ConstructorMatch _ <= ConstructorMatch _ = True
	TupleMatch _ <= TupleMatch _ = True
	a <= b = error "Incompatible pattern types"
\end{code}

Given a set of patterns, we would like to classify them into one of the three groups. Note that
destructor patterns should never arise here.

\begin{code}
classifyPatterns :: [Pattern] -> MatchType
classifyPatterns = head . sort . map classifyPattern
classifyPattern (ConstructorPattern c _) = ConstructorMatch c
classifyPattern (TuplePattern ts) = TupleMatch $ lengthI ts
classifyPattern (VariablePattern _ _) = UnknownMatch
\end{code}

\subsection{Utility functions}

First we have a function to optimize away the {\sf where} clauses if it's just a simple variable
substitution.

\begin{code}
makeWhere :: Expression -> [Either (String, Expression) (String, Expression)] -> Expression
makeWhere e vs = case vs' of
		[] -> e'
		_ -> Where e vs'
	where
	(e', vs') = foldl subVar (e, []) vs
	subVar (e, vs) (Left (x, VariableName y)) = (subst e x y, vs)
	subVar (e, vs) (Right (x, VariableName y)) = (subst e x y, vs)
	subVar (e, vs) v = (e, v:vs)
	subst e x y = cut [(x, VariableName y)] e
\end{code}

\begin{code}
takeI = take . fromInteger
\end{code}
