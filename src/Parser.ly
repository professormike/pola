\chapter{Parsing}	\label{parser}

Before we look at the code for the parser, we mention one brief note about
this section. The code is written in Happy, which is a parser generator for
Haskell. For those familiar with Unix tools, Happy is to Haskell as
\texttt{yacc} is to C.

While Happy does allow for a semi-literate mode of programming, there
are no tools specifically supporting Literate Haskell. Thus, one has
been constructed for this project. Since a Happy file is a combination
of grammar code and Haskell code, a \texttt{awk} script has been
constructed which segregates the Happy code from the Haskell code.
Haskell code goes to the Haskell2LaTeX software while the Happy code is
formatted by the \texttt{awk} script.

This is unfortunate.

\section{Preamble}

\subsection{Exported symbols}

Since the parsing module is the first module which is entered during a
compilation, in necessarily has to create the seed data structures. These
need to exported on to the lambda lifter. The \textit{parse} function is
the major entry point for this module.

> {
> {-# OPTIONS_GHC -XFlexibleInstances -XFlexibleContexts #-}
> module Parser (parse, parseExpression, Expression(..), Type(..),
> 	Constraint(..), TypeDefinition(..), FunctionDefinition,
> 	FunctionDecl, Polynomial(..), ValueSize(..), ConstructorPolyMap(..), Term(..),
> 	Pattern(..), PatternExpr, commaDelimit, delimit, Value(..), valueToMonad)
> where
> 
> import Data.Char
> import Data.List (elemIndex, sortBy, intersperse)
> import Control.Monad (liftM)
> }

First we instruct the parser generator to give the parser the name
\textit{parser}. Note that this is different than the function \textit{parse}
which is exported from this module. See section~\ref{parsefunction} for
more details.

> %name parser

This sets up the data type for tokens. See section~\ref{tokens} for more
details.

> %tokentype { Token }

This sets up the function to handle parse errors. See
section~\ref{errorhandling} for more details.

> %error { parseError }

It makes life much easier in a number of ways if the parser and lexer are monadic.
It allows us better error reporting and hanging on to line numbers.

> %monad { ParseResult }
> %lexer { lexWrapper } { TokenEOF }

\subsection{Tokens}	\label{tokens}

Next we introduce the tokens such that they can be used within the grammar.
For more information on the tokens used in the language, see
section~\ref{lexer}. The use of $\diamondsuit$ indicates that a particular terminal
has attributed data. In the case of {\it ident} and {\it upperident} this attributed
data is a string indicating the name of the identifier; in the case of {\it num} it
is a natural number.

> %token
> 	ident	{ TokenIdent $$ }
> 	upperident	{ TokenUpperIdent $$ }
> 	num	{ TokenNum $$ }
> 	'data'	{ TokenData }
> 	'fold'	{ TokenFold }
> 	'unfold'	{ TokenUnfold }
> 	'in'	{ TokenIn }
> 	'case'	{ TokenCase }
> 	'of'	{ TokenOf }
> 	'peek'	{ TokenPeek }
> 	'where'	{ TokenWhere }
> 	':'	{ TokenTypeConstraint }
> 	'='	{ TokenEq }
> 	'('	{ TokenLParens }
> 	')'	{ TokenRParens }
> 	'['	{ TokenLBrack }
> 	']'	{ TokenRBrack }
> 	'|'	{ TokenAlt }
> 	';'	{ TokenTerm }
> 	','	{ TokenComma }
> 	'{'	{ TokenLBrace }
> 	'}'	{ TokenRBrace }
> 	'->'	{ TokenArrow }
> 	'.'	{ TokenDot }
> 	'<>'	{ TokenSafe }
> 	'><'	{ TokenDangerous }
> 
> %%

\section{Grammar}

Now we introduce the grammar. The starting non-terminal of a program
is \textit{Program}, which allows top-level declarations. Notice that this
is the only level where we allow function declarations, as opposed to only
allowing function definitions. This is eventually going to hook into the
module system.

> Program
> 	: TypeDefinition Program	{ let (t, f, d) = $2 in ($1:t, f, d) }
> 	| FunctionDefinition Program	{ let (t, f, d) = $2 in (t, $1:f, d) }
> 	| FunctionDeclaration Program	{ let (t, f, d) = $2 in (t, f, $1:d) }
> 	|				{ ([], [], []) }

We allow both inductive type definitions and coinductive type definitions.

> TypeDefinition
> 	: 'data' FullTypeName '->' ident '=' Constructors ';'
> 					{ Inductive $2 $ map (fixConstructor $2 $4) $6 }
> 	| 'data' ident '->' FullTypeName '=' Destructors ';'
> 					{ Coinductive $4 $ map (fixDestructor $4 $2) $6 }

The full typename is only used when declaring a new data type. In this case
parentheses aren't not necessary, because it's unambiguous that we are
introducing only one typename. Also, qualified parameters to the type are
disallowed: \textit{only} parametric types are allowed.

> FullTypeName
> 	: upperident '(' TypeParameters ')'	{ ($1, $3) }
> 	| upperident				{ ($1, []) }

A {\it FullNamedType} differs from a {\it FullTypeName} in that parameters can be
concrete.

> FullNamedType
> 	: upperident '(' FullTypes ')'		{ ($1, $3) }
> 	| upperident				{ ($1, []) }

A type parameter is just a lower-case identifier.

> TypeParameters
> 	: ident ',' TypeParameters	{ $1:$3 }
> 	| ident			{ [$1] }

The \textit{Constructors} non-terminal is only used when declaring a new
data type.

> Constructors
> 	: Constructor				{ [$1] }
> 	| Constructor '|' Constructors		{ $1:$3 }
> Constructor
> 	: upperident ':' FullTypesList '->' ident
> 						{ ($1, $3, $5) }

As is the \textit{Destructors} non-terminal.

> Destructors
> 	: Destructor				{ [$1] }
> 	| Destructor '|' Destructors		{ $1:$3 }
> Destructor
> 	: upperident ':' FullTypes '->' FullTypes
> 						{ ($1, $3, $5) }

A full type is used in type signatures. Typically this will just be a
\textit{FullNamedType}, but we required syntax for tuples and also we allow
convenience syntax for arrays and lists.

> FullType
> 	: '(' FullTypesList ')'		{ TupleType $2 }
> 	| FullNamedType			{ let (n, a) = $1 in NamedType n a }
> 	| ident				{ PolyType $1 }

Comma-separated lists of types are used when describing tuples.

> FullTypes
> 	: FullType ',' FullTypes	{ $1:$3 }
> 	| FullType			{ [$1] }
> FullTypesList
> 	: FullTypes		{ $1 }
> 	|			{ [] }

A function is defined by its name, any constraints (such as type constraints)
explicitly placed upon it, its list of o-parameters and p-parameters, and its resultant
expression.

> FunctionDefinition
> 	: ident Constraints '=' MParameterList '|' MParameterList '.' Expression ';'
> 					{ ($1, $2, $4, $6, $8) }

A function declaration is just the same except that has no parameters or
body. It really should have a type constraint; otherwise it won't be of
any use. This is used for bringing in functions from other modules or
languages.

> FunctionDeclaration
>	: ident Constraints	{ ($1, $2) }
> Constraints
> 	: ':' ArrowType		{ [TypeConstraint $2] }
> 	|			{ [] }

The arrow type is only used in type constraints since higher-order functions
are currently not allowed.

> ArrowType
> 	: SFullTypesList '|' SFullTypesList '->' SFullType	{ ArrowType ($1, $3) $5 }

Since a function declaration may need to include safety annotations, we provide syntax
for those here.

> SFullTypesList
> 	: SFullTypes	{ $1 }
> 	|		{ [] }
> SFullTypes
> 	: SFullType			{ [$1] }
> 	| SFullType ',' SFullTypes	{ $1:$3 }

> SFullType
> 	: '(' SFullTypesList ')' Safety	{ $4 $ TupleType $2 }
> 	| SFullTypeName			{ let (n, sf, a) = $1 in sf $ NamedType n a }
> 	| ident	Safety			{ $2 $ PolyType $1 }

> SFullTypeName
> 	: upperident Safety			{ ($1, $2, []) }
> 	| upperident Safety '(' SFullTypes ')'	{ ($1, $2, $4) }

Safety annotations.

> Safety
> 	: '<>'			{ SafeType }
> 	| '><'			{ DangerousType }
> 	|			{ id }

Parameter lists are used in function definitions. Types of the parameters are
inferred and possibly constrained, but not given explicitly. Hence, they are
just a white-space separated list of identifiers.

> MParameterList
> 	: ParameterList			{ $1 }
> 	|				{ [] }
> ParameterList
> 	: ident ',' ParameterList	{% assertPureParamList $1 $3 }
> 	| ident				{ [$1] }

Finally, the expression grammar.

> Expression
> 	: Expression 'where' Binding		{ Where $1 [$3] }
> 	| Expression 'where' '{' Bindings '}'	{ Where $1 $4 }
> 	| BigExpression				{ $1 }

A binding is a mapping between an identifier and an expression, used in {\tt where} clauses. We
distinguish between an opponent binding and a player binding by the use of a {\tt :=} (opponent
binding) versus {\tt =} (player binding).

> Bindings
> 	: Binding ';' Bindings	{ $1:$3 }
> 	|			{ [] }
> Binding
> 	: ident ':' '=' BigExpression	{ Left ($1, $4) }
> 	| ident '=' BigExpression	{ Right ($1, $3) }

> BigExpression
> 	: 'fold' ident '(' ParameterList ')' 'of' PattExprs 'in' BigExpression
> 			{ Fold ($2, head $4, tail $4) $7 $9 }
> 	| 'unfold' ident '(' MParameterList ')' 'of' Records 'in' BigExpression
> 			{ Unfold ($2, $4) $7 $9 }
> 	| 'case' Expression 'of' PattExprs
> 				{% do
> 					pes <- assertCasePatt $4
> 					return $ Case $2 pes }
> 	| 'peek' Expression 'of' PattExprs
> 				{% do
> 					pes <- assertCasePatt $4
> 					return $ Peek $2 pes }
> 	| upperident '[' Expression ']'	{ Destruction $1 [$3] }
> 	| upperident '[' Expression ']' '(' ArgumentList ')'
> 					{ Destruction $1 ($3:$6) }
> 	| FunctionExpr		{ $1 }

A function expression is just an application of a function to a number of arguments.

> FunctionExpr
> 	: ident '(' MArgumentList ')'	{ FunctionCall $1 $3 }
> 	| upperident '(' ArgumentList ')'	{ Construction $1 $3 }
> 	| Records			{ Record $1 }
> 	| ArgumentExpr			{ $1 }

The argument list is a list of expressions to be given as arguments to a
function or construction.

> MArgumentList
> 	: ArgumentList			{ $1 }
> 	|				{ [] }
> ArgumentList
> 	: Expression ',' ArgumentList	{ $1:$3 }
> 	| Expression			{ [$1] }

The bottom level of expressions is either a structured composition of
expressions, as in a tuple or list, a primitive, or a variable. Note that
we disallow unary tuples.

> ArgumentExpr
> 	: ident				{ VariableName $1 }
>	| upperident			{ Construction $1 [] }
> 	| num				{ makeNum $1 }
> 	| '(' ')'			{ TupleExpr [] }
> 	| '(' Expression ',' ArgumentList ')'	{ TupleExpr ($2:$4) }
> 	| '(' Expression ')'		{ $2 }
> 	| '[' MArgumentList ']'		{ makeList $2 }

Next we move to pattern expressions, which are very simple in their construction: patterns
paired with expressions.

> PattExprs
> 	: '{' PEs '}'			{ $2 }
> 	| Pattern '.' BigExpression	{ [($1, $3)] }
> PEs
> 	: PE				{ [$1] }
> 	| PE ';' PEs			{ $1:$3 }
> PE
> 	: Pattern '.' Expression	{ ($1, $3) }

For patterns we enumerate all pattern types except destructor patterns. Since destructor
patterns can only be used in record or unfold expressions, they need not be syntactically
supported here.

> Pattern
> 	: '(' ')'			{ TuplePattern [] }
> 	| '(' Pattern ',' FCommaPatterns ')'	{ TuplePattern ($2:$4) }
> 	| upperident			{ ConstructorPattern $1 [] }
> 	| upperident '(' CommaPatterns ')'	{ ConstructorPattern $1 $3 }
> 	| ident				{ VariablePattern $1 "_" }
> 	| ident '|' ident		{ VariablePattern $1 $3 }

> FCommaPatterns
> 	: Pattern			{ [$1] }
> 	| Pattern ',' FCommaPatterns	{ $1:$3 }
> CommaPatterns
> 	:				{ [] }
> 	| FCommaPatterns		{ $1 }

Record expressions used in unfolds and records.

> Records
> 	: '(' RecordPattExprs ')'	{ $2 }
> RecordPattExprs
> 	: RecordPattExpr		{ [$1] }
> 	| RecordPattExpr ';' RecordPattExprs	{ $1:$3 }

Records are different from inductive patterns in that they bind only p-identifiers, never
o-identifiers.

> RecordPattExpr
> 	: upperident ':' Expression	{ (DestructorPattern $1 [], $3) }
> 	| upperident ':' ParameterList '.' Expression
> 					{ (DestructorPattern $1 $3, $5) }

\section{Supporting functions}	\label{parserfunctions}

Now that the grammar has been described, we need only define the functions
used by the grammar to complete the parsing module.

> {

First we introduce a monadic type for keeping line number state.

> data ParseResult a
> 	= S (String -> (Integer, Integer) -> Value a)
> instance Functor ParseResult where
> 	fmap = liftM
> instance Applicative ParseResult where
>	pure v = S $ \ _ _ -> Ok v
>	(<*>) = ap
> instance Monad ParseResult where
> 	S f >>= m = S $ \ s l -> case f s l of
> 		Ok v -> let S g = m v in g s l
> 		Fail err -> Fail err
> 	return = pure
> 	fail s = S $ \ _ _ -> Fail s

The monadic function for finding the current position in the lexer.

> currentPosition :: ParseResult (Integer, Integer)
> currentPosition = S $ \ s p -> Ok p

Next we introduce the various types which can be used in type constraints.

> data Type a
> 	= TupleType [Type a]
> 	| NamedType String [Type a]
> 	| PolyType a
> 	| ArrowType ([Type a], [Type a]) (Type a)
> 	| SafeType (Type a)
> 	| DangerousType (Type a)
> 	deriving Eq

In order to produce type constraints in the way that the parser
expects, we must provide our own \textit{show} function.

> instance Show (Type String) where
> 	show (TupleType ts) = "(" ++ commaDelimit (map show ts) ++ ")"
> 	show (NamedType s []) = s
> 	show (NamedType s ts) = s ++ "(" ++ commaDelimit (map show ts) ++ ")"
> 	show (PolyType s) = s
> 	show (ArrowType (a, b) c) = commaDelimit (map show a) ++ " | " ++
> 		commaDelimit (map show b) ++ " -> " ++ show c
> 	show (SafeType t) = showSafety "<>" t
> 	show (DangerousType t) = showSafety "><" t

Safety can be marked to show the safety requirements on particular types.

> showSafety mark t@(NamedType s ts) = show $ NamedType (s ++ mark) ts
> showSafety mark t = show t ++ mark

\subsection{Types and functions dealing with constraints}

We need polynomials and sizes to deal with bound inference. These are more properly defined in
section~\ref{bounds}, but because we have to deal with function declarations in the front end,
they are initially defined here.

> data Constraint
> 	= TypeConstraint (Type String)
> 	| TimeConstraint Polynomial
> 	| SizeConstraint ValueSize
> instance Show Constraint where
> 	show (TypeConstraint t) = ":: " ++ show t
> 	show (TimeConstraint p) = ":time: " ++ show p
> 	show (SizeConstraint s) = ":size: " ++ show s

The polynomial type is used for representing time constraints over some variables.

> data Polynomial
> 	= Poly [String] [Term]
> data Term
> 	= PolyTerm Integer [(String, Integer)]
> instance Show Polynomial where
> 	show (Poly _ terms) = s $ filter (\ (PolyTerm n _) -> n /= 0) terms' where
> 		terms' = sortBy higherDegree terms
> 		higherDegree (PolyTerm d1 xs) (PolyTerm d2 ys) = case compare (d $ ys) (d $ xs) of
> 			EQ -> compare d2 d1
> 			c -> c
> 		d = map snd
> 		s [] = "0"
> 		s [x] = show x
> 		s (x:xs@(PolyTerm yc yt:xs1)) = show x ++ y' where
> 			y'	| yc < 0 = " - " ++ s (PolyTerm (-yc) yt:xs1)
> 				| otherwise = " + " ++ s xs
> instance Show Term where
> 	show (PolyTerm coeff []) = show coeff
> 	show (PolyTerm coeff factors) = coeff' ++ delimit " " (map showFactor factors) where
> 		coeff'	| coeff == 1 = ""
> 			| otherwise = show coeff
> 		showFactor (x, 1) = x
> 		showFactor (x, n) = x ++ "^" ++ show n

HUGE FIXME UP THERE ON y'.True

For dealing with sizes, we either have a straight inductive size, corresponding to the numbers of
each constructor and the sizes of associated type parameters, or we have a coinductive value in
which case we need to be representing in terms of lazy ``potential'' cost and size.

The {\sf FoldSize} should {\it never} be visible to the user. The {\sf UnfoldSize} should only
be valid in the context of a larger {\sf CoinductiveSize}.

> data ValueSize
> 	= InductiveSize ConstructorPolyMap
> 	| CoinductiveSize [String] [(String, Polynomial)] [(String, [String], ValueSize)]
> 	| TupleSize [ValueSize]
> 	| SimpleSize Polynomial
> instance Show ValueSize where
> 	show (InductiveSize conses) = show conses
> 	show (CoinductiveSize rc tm sz) = vars ++ "Lazy time: <" ++ delimit " | " (map showT tm) ++
> 			">, Lazy size: <" ++ delimit " | " (map showZ sz) ++ ">" where
> 		showT (d, p) = d ++ ":" ++ show p
> 		showZ (d, [], z) = d ++ ":" ++ show z
> 		showZ (d, xs, z) = d ++ "(" ++ commaDelimit xs ++ "):" ++ show z
> 		vars = case rc of
> 			[] -> ""
> 			_ -> "\\ " ++ commaDelimit (map show rc) ++ " . "
> 	show (TupleSize sizes) = "(" ++ commaDelimit (map show sizes) ++ ")"
> 	show (SimpleSize p) = show p
> data ConstructorPolyMap = Polies [(String, Polynomial, [ValueSize])]
> instance Show ConstructorPolyMap where
> 	show (Polies ps) = "<" ++ delimit " | " (map showSP ps) ++ ">" where
> 		showSP (s, p, ds) = s ++ ":" ++ show p ++ case ds of
> 			[] -> ""
> 			_ -> ", <" ++ commaDelimit (map show ds) ++ ">"

\subsection{Error handling}	\label{errorhandling}

In the interest of graceful handling of errors, we need a monad to capture the
idea of a parse succeeding or failing.

> data Value a
> 	= Ok a
> 	| Fail String
> instance Functor Value where
> 	fmap = liftM
> instance Applicative Value where
> 	pure = Ok
> 	(<*>) = ap
> instance Monad Value where
> 	Ok a >>= m = m a
> 	Fail s >>= _ = Fail s
> 	return = pure
> 	fail = Fail

We may want to transform it to some other monad.

> valueToMonad (Ok x) = return x
> valueToMonad (Fail x) = fail x

Handling a parser error then involves constructing a failed object.

> syntaxError :: String -> ParseResult a
> syntaxError msg = do
> 	(l, c) <- currentPosition
> 	fail $ show l ++ ":" ++ show c ++ ":" ++ msg
> parseError :: Token -> ParseResult a
> parseError TokenEOF = syntaxError "Unexpected end-of-file"
> parseError t = syntaxError $ "Unexpected token: " ++ show t

\subsection{Expression data types and functions}

We need a structure for the various types of patterns. Note that patterns may
be heterogeneous within a {\tt case} or {\tt fold}. That is, a single {\tt case}
could have a variable pattern and constructor pattern.

> data Pattern
> 	= VariablePattern String String
> 	| ConstructorPattern String [Pattern]
> 	| DestructorPattern String [String]
> 	| TuplePattern [Pattern]
> 	deriving Eq
> instance Show Pattern where
> 	show (VariablePattern p "_") = p
> 	show (VariablePattern p q) = p ++ "|" ++ q
> 	show (ConstructorPattern s []) = s
> 	show (ConstructorPattern s ps) = s ++ "(" ++ commaDelimit (map show ps) ++ ")"
> 	show (DestructorPattern s vs) = s ++ " : " ++ commaDelimit vs
> 	show (TuplePattern ps) = "(" ++ commaDelimit (map show ps) ++ ")"

The taxonomy of expressions should follow clearly from the syntax of the
language, as at this point we are only constructing the parse tree without
any attention to semantics.

> type PatternExpr = (Pattern, Expression)
> data Expression
> 	= FunctionCall String [Expression]
> 	| Construction String [Expression]
> 	| Destruction String [Expression]
> 	| Fold (String, String, [String]) [PatternExpr] Expression
> 	| Unfold (String, [String]) [PatternExpr] Expression
> 	| Case Expression [PatternExpr]
> 	| Peek Expression [PatternExpr]
> 	| Record [PatternExpr]
> 	| VariableName String
> 	| TupleExpr [Expression]
> 	| Where Expression [Either (String, Expression) (String, Expression)]
> 	| ErrorExpr String
> 	deriving Eq

And the code to show expressions:

> instance Show Expression where
> 	show (FunctionCall s es) = s ++ "(" ++ (commaDelimit . map show) es ++ ")"
> 	show (Construction "Zero" _) = "0"
> 	show (Construction "Succ" [c]) = case toNum c of
> 			Just x -> show $ x + 1
> 			Nothing -> "Succ(" ++ show c ++ ")"
> 		where
> 		toNum (Construction "Zero" _) = return 0
> 		toNum (Construction "Succ" [c]) = do
> 			c' <- toNum c
> 			return $ c' + 1
> 		toNum _ = Nothing
> 	show (Construction "Nil" _) = "[]"
> 	show (Construction "Cons" [h, t]) = "[" ++ show h ++ l t where
> 		l (Construction "Cons" [h, t]) = ", " ++ show h ++ l t
> 		l _ = "]"
> 	show (Construction s []) = s
> 	show (Construction s es) = show (FunctionCall s es)
> 	show (Destruction s [e]) = s ++ "[" ++ show e ++ "]"
> 	show (Destruction s (e:es)) = s ++ "[" ++ show e ++ "](" ++
> 		(commaDelimit . map show) es ++ ")"
> 	show (Fold (f, x, ys) ((p, e):_) c) = "fold " ++ f ++ "(" ++
> 		commaDelimit (x:ys) ++ ") as { " ++ show p ++ "." ++ show e ++ "; ... } in " ++
> 		show c
> 	show (Unfold (f, ys) ds coda) = "unfold " ++ f ++ "(" ++ commaDelimit ys ++ ") as " ++
> 		show (Record ds) ++ " in " ++ show coda
> 	show (Case s [(p, _)]) = "case " ++ show s ++ " of " ++ show p ++ ". ..."
> 	show (Case s ((p, _):_)) = "case " ++ show s ++ " of { " ++ show p ++ ". ...; ... }"
> 	show (Peek s [(p, _)]) = "peek " ++ show s ++ " of " ++ show p ++ ". ..."
> 	show (Peek s ((p, _):_)) = "peek " ++ show s ++ " of { " ++ show p ++ ". ...; ... }"
> 	show (Record ((DestructorPattern d [], _):_)) = "( " ++ d ++ " : ...; ... )"
> 	show (Record ((DestructorPattern d ps, _):_)) = "( " ++ d ++ " : " ++
> 		commaDelimit ps ++ " . ...; ... )"
> 	show (VariableName s) = s
> 	show (TupleExpr es) = "(" ++ (commaDelimit . map show) es ++ ")"
> 	show (Where e xs) = show e ++ " where " ++ s xs where
> 		s [] = "{ }"
> 		s [x] = s1 x
> 		s (x:_) = "{" ++ s1 x ++ "; ... }"
> 		s1 (Left (x, e)) = x ++ " := " ++ show e
> 		s1 (Right (x, e)) = x ++ " = " ++ show e
> 	show (ErrorExpr s) = "!!! " ++ s ++ " !!!"

\subsection{Lexer supporting functions}

The list of tokens must be listed explicitly, and was described in detail in
section~\ref{tokens}.

> data Token = TokenEq | TokenLParens | TokenRParens | TokenLBrack |
> 	TokenRBrack | TokenIdent String |
> 	TokenUpperIdent String | TokenData | TokenAlt | TokenTerm | TokenComma | 
> 	TokenLBrace | TokenRBrace | TokenArrow | TokenDot | TokenTypeConstraint |
> 	TokenFold | TokenUnfold | TokenIn | TokenCase | TokenWhere |
> 	TokenOf | TokenNum Integer | TokenPeek | TokenSafe | TokenDangerous | TokenEOF

When printing error messages, it is useful to have human-readable forms of the
tokens.

> instance Show Token where
> 	show TokenEq = "="
> 	show TokenLParens = "("
> 	show TokenRParens = ")"
> 	show TokenLBrack = "["
> 	show TokenRBrack = "]"
> 	show (TokenIdent s) = s
> 	show (TokenUpperIdent s) = s
> 	show TokenData = "data"
> 	show TokenAlt = "|"
> 	show TokenTerm = ";"
> 	show TokenComma = ","
> 	show TokenLBrace = "{"
> 	show TokenRBrace = "}"
> 	show TokenArrow = "->"
> 	show TokenDot = "."
> 	show TokenTypeConstraint = ":"
> 	show TokenFold = "fold"
> 	show TokenUnfold = "unfold"
> 	show TokenIn = "in"
> 	show TokenCase = "case"
> 	show TokenOf = "of"
> 	show TokenPeek = "peek"
> 	show TokenWhere = "where"
> 	show (TokenNum i) = show i
> 	show TokenSafe = "<>"
> 	show TokenDangerous = "><"

\subsection{Convenience types and functions}

Finally we make convenience types for commonly-used data.

> data TypeDefinition
> 	= Inductive (String, [String]) [(String, [Type String], Type String)]
> 	| Coinductive (String, [String]) [(String, [Type String], [Type String])]
> type FunctionDefinition = (String, [Constraint], [String], [String], Expression)
> type FunctionDecl = (String, [Constraint])

The \textit{assertPureParamList} function ensures that a parameter name is not used
multiple times in the same parameter list. The variable name \texttt{\_}
(a single underscore) is special. From the lexer, it comes to us at the
empty string, and we should allow an unbounded number of these unnamed
variables.

> assertPureParamList :: String -> [String] -> ParseResult [String]
> assertPureParamList "_" ps = return $ "_":ps
> assertPureParamList p ps
> 	| elem p ps = syntaxError $ "Parameter " ++ p ++ " is multiply defined"
> 	| otherwise = return $ p:ps

The \textit{assertCasePatt} function ensures the p-variable is not bound in a set pattern.

> assertCasePatt = mapM a where
> 	a (p, e) = do
> 		(p', _) <- a2 [] p
> 		return (p', e)
> 	a2 vs v@(VariablePattern w "_")	| elem w vs = syntaxError $ "Bound variable " ++ w ++
> 						" is multiply defined"
> 					| w == "_" = return (v, vs)
> 					| otherwise = return (v, w:vs)
> 	a2 _ (VariablePattern x _) = syntaxError $ "Cannot bind variable " ++ x ++ " in this manner"
> 	a2 vs (TuplePattern ps) = a3 vs ps $ TuplePattern
> 	a2 vs (ConstructorPattern c ps) = a3 vs ps $ ConstructorPattern c
> 	a3 vs ps f = do
> 		(ps', vs') <- a4 vs ps
> 		return (f ps', vs')
> 	a4 vs [] = return ([], vs)
> 	a4 vs (p:ps) = do
> 		(p', vs1) <- a2 vs p
> 		(ps', vs2) <- a4 vs1 ps
> 		return (p':ps', vs2)

For making inductive and coinductive types $C$, we group together any variables of type $C$.

> fixConstructor :: (String, [String]) -> String -> (String, [Type String], String) ->
> 	(String, [Type String], Type String)
> fixConstructor (tp, tpparms) c (cns, attrs, d)
> 	| c == d = (cns, map repl attrs, tp')
> 	| otherwise = error $ "Bad definition of constructor " ++ cns where
> 	tp' = NamedType tp $ map PolyType tpparms
> 	repl (PolyType x)	| x == c = tp'
> 				| otherwise = PolyType x
> 	repl (TupleType ts) = TupleType $ map repl ts
> 	repl tp = tp

> fixDestructor :: (String, [String]) -> String -> (String, [Type String], [Type String]) ->
> 	(String, [Type String], [Type String])
> fixDestructor (tp, tpparms) d (des, cs@(PolyType d':_), rs)
> 	| d' == d = (des, map repl cs, map repl rs)
> 	| otherwise = error $ "Bad definition of destructor " ++ des where
> 	repl (PolyType x)	| x == d = NamedType tp $ map PolyType tpparms
> 				| otherwise = PolyType x
> 	repl (TupleType ts) = TupleType $ map repl ts
> 	repl tp = tp
> fixDestructor _ _ (des, _, _) = error $ "Bad definition of destructor " ++ des

The final function handles the printing of multiple strings in a
comma-delimited fashion. Given strings
like \texttt{foo}, \texttt{bar} and \texttt{honey}, \textit{commaDelimit}
produces the string \verb*8foo, bar, honey8. The \textit{atEndCommaDelimit}
differs in that if the input list is non-empty, the output is prepended by
an extra \verb*8, 8, which is useful if used in a context where the beginning
of a comma-delimited list is already extant.

> commaDelimit = delimit ", "
> delimit :: String -> [String] -> String
> delimit x = concat . intersperse x

We have a function to automatically construct natural numbers.

> makeNum :: Integer -> Expression
> makeNum 0 = Construction "Zero" []
> makeNum n = Construction "Succ" [makeNum (n - 1)]

And a function to automatically construct lists.

> makeList :: [Expression] -> Expression
> makeList [] = Construction "Nil" []
> makeList (x:xs) = Construction "Cons" [x, makeList xs]

\section{The lexer}	\label{lexer}

> lexWrapper cont = S context where
> 	context s p = f s' p' where
> 		S f = cont t
> 		(t, s', p') = lexer s p

> lexer :: String -> (Integer, Integer) -> (Token, String, (Integer, Integer))

First we must consider line comments. As soon as we see two hypens, we
ignore everything until the next newline.

> lexer ('-':'-':cs) (l, _) = lexer (dropWhile ('\n' /=) cs) (l + 1, 0)

Next we consider block comments. These may be nested one inside another, so
we must keep track of how many block comment introductions we see.

> lexer ('{':'-':cs) (l, c) = uncurry lexer $ findCommentEnd cs 1 (l, c + 2) where
> 	findCommentEnd s 0 p = (s, p)
> 	findCommentEnd ('{':'-':cs) n (l, c) = findCommentEnd cs (n + 1) (l, c + 2)
> 	findCommentEnd ('-':'}':cs) n (l, c) = findCommentEnd cs (n - 1) (l, c + 2)
> 	findCommentEnd (c:cs) n p = findCommentEnd cs n $ posWhiteSpace p c
> 	findCommentEnd [] _ _ = error "Unterminated comment; hit end-of-file"

We handle the end-of-file situation.

> lexer "" p = (TokenEOF, "", p)

Since most of the lexing never changes the line number, we now break down into
another function to automatically handle updating the column counter.

> lexer s@(sc:ss) p@(l, c)
> 		| isSpace sc = lexer ss $ posWhiteSpace p sc
>		| otherwise = (t, s', (l, c + lengthI s - lengthI s'))
> 	where
> 	(t, s') = lexC s
> 	lengthI = toInteger . length

The punctuation keywords, such as various parentheses, braces and delimeters,
are quite easy to take care of through pattern matching.

> lexC ('(':cs) = (TokenLParens, cs)
> lexC (')':cs) = (TokenRParens, cs)
> lexC ('[':cs) = (TokenLBrack, cs)
> lexC (']':cs) = (TokenRBrack, cs)
> lexC ('{':cs) = (TokenLBrace, cs)
> lexC ('}':cs) = (TokenRBrace, cs)
> lexC (';':cs) = (TokenTerm, cs)
> lexC ('@':cs) = (TokenTerm, cs)
> lexC (',':cs) = (TokenComma, cs)
> lexC (':':cs) = (TokenTypeConstraint, cs)
> lexC ('_':cs) = (TokenIdent "_", cs)
> lexC ('=':cs) = (TokenEq, cs)
> lexC ('-':'>':cs) = (TokenArrow, cs)
> lexC ('.':cs) = (TokenDot, cs)
> lexC ('|':cs) = (TokenAlt, cs)
> lexC ('<':'>':cs) = (TokenSafe, cs)
> lexC ('>':'<':cs) = (TokenDangerous, cs)

For further lexing, we cannot match fixed strings anymore and must match
by the type of the first character. We use the auxiliary function
\textit{lexCT} for this, as described in subsection~\ref{lexct}

> lexC s@(c:_) = lexCT (classify c) s where
> 	classify c	| isLower c = IdentChar
> 			| isUpper c = UpperIdentChar
> 			| isPunct c = PunctChar
> 			| isDigit c = NumChar
> 			| isSpace c = SpaceChar
> 			| otherwise = ErrorChar

\subsection{Updating the current position according to whitespace}

We often need to update the position counter (e.g., line number) with respect
to whitespace.

> posWhiteSpace (l, _) '\n' = (l + 1, 0)
> posWhiteSpace (l, c) '\t' = (l, c + 8)
> posWhiteSpace (l, c) _ = (l, c + 1)

And a convenience function to do this update on a string.

> pos :: (Integer, Integer) -> String -> (Integer, Integer)
> pos = foldl posWhiteSpace

And a convenience function to add a number of columns on to a position.

> addC (l, c) i = (l, c + i)

\subsection{Lexing by character type}	\label{lexct}

The \textit{lexCT} function lexes not on the exact character, but on the
type of character. For this to work, we need to introduce the data type
representing character types.

> data CharClass = IdentChar | UpperIdentChar | PunctChar | NumChar |
> 	SpaceChar | ErrorChar

We look at identifiers. Identifiers starting with lowercase letters
need special attention because they could be keywords. We have to make
a special case for vertical bar due to its use in delimiting identifiers.

> lexCT IdentChar s = (keyword i, ts) where
> 	(i, ts) = breakUpBy (\ x -> isValidLetter x || isPunct x && x /= '|') s
> 	keyword "data" = TokenData
> 	keyword "fold" = TokenFold
> 	keyword "unfold" = TokenUnfold
> 	keyword "in" = TokenIn
> 	keyword "case" = TokenCase
> 	keyword "of" = TokenOf
> 	keyword "as" = TokenOf
> 	keyword "peek" = TokenPeek
> 	keyword "where" = TokenWhere
> 	keyword id = TokenIdent id

Tokenizing identifiers that begin with an uppercase letter are easier because
they cannot be keywords.

> lexCT UpperIdentChar s = (TokenUpperIdent t, ts) where
> 	(t, ts) = breakUpBy isValidLetter s

And then we look at integer literals.

> lexCT NumChar s = (TokenNum $ read i, ts) where
> 	(i, ts) = breakUpBy isDigit s

\subsection{Grouping together character types}

Commonly we want to group together characters of a certain type as one token,
and then lex the rest of the input string. The \textit{breakUpBy} function
does allows us to do this.

> breakUpBy :: (Char -> Bool) -> String -> (String, String)
> breakUpBy f s = (takeWhile f s, dropWhile f s)

When grouping together identifiers, we want to include the underscore
character as being valid. Hence, we provide the \textit{isValidLetter} predicate
for tokenizing identifiers via the \textit{breakUpBy} function.

> isValidLetter c = isAlphaNum c || c == '_'

We also need a predicate to group characters by what we would consider to be
a valid punctuation token.

> isPunct c = elem c "=<>|&+-*/%^$:'"

\section{The complete parser}	\label{parsefunction}

Finally there is the globally exported functions, \textit{parse} and
\textit{parseExpression}, which are nothing but the parser and lexer
combined. We seed the monadic parser with the initial data of row 1,
column 0. The first character read will then be at row 1, column 1.

> parse :: String -> Value ([TypeDefinition], [FunctionDefinition], [FunctionDecl])
> parse s = f s (1, 0) where
> 	S f = parser

The \textit{parseExpression} function is only used in interaction mode.

> parseExpression :: String -> Value (FunctionDefinition, Expression)
> parseExpression x = do
> 	(_, [f@("expr", [], [], [], e)], _) <- parse $ "expr = | . " ++ x ++ ";"
> 	return (f, e)
> }
