\chapter{Main}

The main module only glues together the other components.

\section{Preamble}

\begin{code}
module Main (main)
where

import Parser
import LaTeXPrint
import TypeInference
import Interpreter
import PatternMatching
import Bounds
import System.IO
import System.Environment (getArgs)
import Data.List (find)
\end{code}

\section{Main function}

The main entry point, of course, is {\it main}, which Haskell mandates be an IO monad. What we
do in {\it main} is, using the filename from the command line, load the given Pola source file
from the filesystem. We parse and typecheck it and end up with a global environment which contains
all the functions and datatypes found within that source file. We then enter a REPL-style loop
where we continuously prompt for expressions to evaluate. We parse and typecheck those and then
invoke the interpreter to evaluate.

\begin{code}
main = do
		inputs <- getArgs
		case inputs of
			["--latex", x] -> do
				(typeDefns, defns, _) <- compileWithFilename x
				mapM_ latexPrintType typeDefns
				mapM_ latexPrintFunction defns
			["--compositional", x] -> do
				(typeDefns, defns, _) <- compileWithFilename x
				mapM_ compositionalPrintType typeDefns
				mapM_ compositionalPrintFunction defns
			[x] -> repl x
			_ -> fail "Usage: pola <input-filename.pola>"
	where
	repl x = do
		(typeDefns, defns, env) <- compileWithFilename x
		withPrompt "> " () $ \ input -> do
			case readExpr input typeDefns env of
				Ok (tp, e) -> do
					putStrLn $ showDecl tp
					let e' = interpret defns e
					print e'
					putStrLn ""
				Fail s -> putStrLn $ "*** " ++ s
	readExpr input typeDefns env = do
		(f, e) <- parseExpression input
		f' <- expandPatterns typeDefns f
		tp <- typeFunction f' env
		return (tp, e)
	compileWithFilename fname = do
		inputFile <- openFile fname ReadMode
		r <- compileFile fname inputFile
		hClose inputFile
		return r
\end{code}

The {\it withPrompt} function is a monad which takes in another monad, $m$, as an argument. It
executes $m$ each time the user types in a line, until the user types in {\tt :q}, at which point
it quits.

\begin{code}
withPrompt :: String -> a -> (String -> IO a) -> IO a
withPrompt s r m = do
	putStr s
	hFlush stdout
	inp <- getLine
	if inp == ":q"
		then return r
		else do
			r' <- m inp
			withPrompt s r' m
\end{code}

The {\it showDecl} function prints the type of a symbol to the screen.

\begin{code}
showDecl :: FunctionDecl -> String
showDecl (s, ts) = s ++ s1 ts where
	s1 [] = ""
	s1 [c] = " " ++ show c
	s1 (c:cs) = " " ++ show c ++ ",\n" ++ s2 cs
	s2 [c] = "\t" ++ show c
	s2 (c:cs) = "\t" ++ show c ++ ",\n" ++ s2 cs
\end{code}

\section{Dealing with source files}

The {\it compileFile} function takes a filename and the contents of that file and return the
new symbols to add to the environment.

\begin{code}
compileFile filename input = do
	code <- hGetContents input
	(typeDefns, decls, defns, env) <- valueToMonad $ generateCode code
	mapM_ (putStrLn . showDecl) decls
	return (typeDefns, defns, env)
\end{code}

The {\it generateCode} function, once a compiler is written, may actually generate code. For
the time being, it just takes the new symbols loaded and adds them to the global environment.
To do this, it parses, expands out complex patterns (see chapter~\ref{patternmatching}) and
then infers and checks typing.

\begin{code}
generateCode :: String ->
	Value ([TypeDefinition], [FunctionDecl], [FunctionDefinition], FlatContext)
generateCode s = do
		(typeDefns, functionDefns, functionDecls) <- parse s
		functionDefns' <- mapM (expandPatterns $ initialTypes ++ typeDefns) functionDefns
		let parseTree' = (typeDefns, functionDefns', functionDecls)
		(functionDecls, env') <- initialEnvironment $ getNewDecl parseTree'
		let boundDecls = foldl (\ ds f -> inferBounds env' ds f:ds) [] $ zip functionDecls functionDefns'
		let functionDecls' = map (mergeDecls boundDecls) functionDecls
		return (typeDefns, functionDecls', functionDefns', env')
	where
	getNewDecl (dataTypes, functionDefns, functionDecls) = do
		mapM_ symbolsFromType dataTypes
		mapM_ addDeclaration functionDecls
		mapM addFunction functionDefns
	mergeDecls ds2 ((f, cs), _) = case find (\ (g, _) -> g == f) ds2 of
		Just (_, cs1) -> (f, cs ++ cs1)
		Nothing -> (f, cs)
\end{code}
