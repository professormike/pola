\chapter{Type Inference}	\label{typing}

The typing system is a fairly simple Hindley-Milner type inference system. It receives a parse tree
from the parsing module (see chapter~\ref{parser}) and must then:
\begin{enumerate}
\item infer correct types for expressions;
\item ensure types are consistent and conform to any explicit type constraints the programmer has
given.
\end{enumerate}

The typing system produces a list of function declarations.

\section{Preamble}

\subsection{Exported symbols}

There are four major symbols exported by this module. \textit{symbolsFromType} produces a
context (symbol table) from a data type definition; \textit{addFunction} augments a
context with the definition of a function, after checking the type of the function for
consistency; \textit{addDeclaration} blindly adds the type of a function, without the body of that
function, to the environment; and \textit{initialEnvironment} provides a starting environment with
the definitions of basic types (e.g., natural numbers, booleans and lists) and simple operations.

The {\it typeFunction} function does nothing but call {\it addFunction} and pull out the type,
but we define it here to nicely hide away our monad.

Note that the type inference module does \textbf{not} export any data types.

\begin{code}
{-# OPTIONS_GHC -XTypeSynonymInstances -XFlexibleInstances -XFlexibleContexts #-}
module TypeInference (symbolsFromType, addFunction, addDeclaration, 
	initialTypes, initialEnvironment,
	typeFunction, Context(..), FlatContext(..), SymbolType(..), genType)
where

import Parser
import Utility
import Data.Maybe
import Data.List
import Control.Monad
\end{code}

\subsection{Commonly used types}

Although the module does not export any data types, we do use
particular data types in certain combinations frequently.

The \textit{TempType} type is a rich type, as inherited from the parser. The goal is to allow
\textit{every} symbol in the parse tree to be annotated with a type. If the type has been annotated
explicitly by the programmer, then polymorphic types are strings, representing the type given by
the programmer; otherwise they are integers, representing polymorphic types that need yet to be
unified by the type inference algorithm.

\begin{code}
type TempType = Type (Either String Integer)
\end{code}

For error reporting purposes, it is sometimes necessary to print out a {\it TempType}.

\begin{code}
instance Show TempType where
	show (TupleType ts) = "(" ++ commaDelimit (map show ts) ++ ")"
	show (NamedType s []) = s
	show (NamedType s ts) = s ++ "(" ++ commaDelimit (map show ts) ++ ")"
	show (PolyType (Left s)) = s
	show (PolyType (Right x)) = "!T" ++ show x
	show (ArrowType (a, b) c) = commaDelimit (map show a) ++ " | " ++
		commaDelimit (map show b) ++ " -> " ++ show c
	show (SafeType t) = showSafety "<>" t
	show (DangerousType t) = showSafety "><" t

showSafety mark (NamedType s ts) = show $ NamedType (s ++ mark) ts
showSafety mark t = show t ++ mark
\end{code}

Equation expressions are either existentially qualified or universally qualified.

\begin{code}
data EquationExpr
	= QF EquationExpr [Integer] [Integer] [(Integer, TempType, [Expression])]
	| Empty
\end{code}

\subsubsection{Contexts}

A context is a mapping between symbols (strings) and their types. For function and variable
symbols, this is all we need. For constructor symbols, we need also to know the ordinal value of
the constructor (e.g., \texttt{Nil} has constructor ordinal value 0, \texttt{Cons} has constructor
ordinal value 1). Hence, for function symbols, the second part of the association will be
\textit{Simple}; for constructor symbols, it will be the ordinal value of the constructor.

\begin{code}
data FlatContext = FlatContext [(String, (TempType, SymbolType))] deriving Show
\end{code}

We also need a bunched context. A bunched context is a tree of alternating levels of Cartesian
products (``semicolon'', where we have to make a choice of one or the rest) and tensor products
(``comma'', where we are allowed to make use of everything). E.g., the context $a,b,(c;d)$
says that we are allowed to consume $a$, $b$ and then we have the choice of consuming either
$c$ or $d$. The context $(a,b);(c,d)$ says we have to choose either $a$ and $b$ or else $c$ and
$d$.

The {\sf Bool} field describes whether a variable has been duplicated.

\begin{code}
data GroupingDiscipline = Semicolon | Comma deriving Eq
data BunchedContext
	= EmptyContext
	| SingleSymbol String TempType Bool
	| ContextNode GroupingDiscipline [BunchedContext]
\end{code}

FIXME: Debugging code:

\begin{code}
instance Show BunchedContext where
	show EmptyContext = "0"
	show (SingleSymbol s _ d) = s ++ d' where
		d'	| d = "*"
			| otherwise = ""
	show (ContextNode _ []) = "()"
	show (ContextNode _ [x]) = "(" ++ show x ++ ")"
	show (ContextNode d xs) = "(" ++ p xs ++ ")" where
		p [x] = show x
		p (x:xs) = show x ++ d' ++ p xs
		d' = case d of
			Semicolon -> "; "
			Comma -> ", "
\end{code}

To define intersection on bunched contexts we need to define equality on these trees. We do it in
the obvious way with the exception that we avoid whether a symbol has been duplicated or not (and
we also ignore symbol types since there will only be one copy of a symbol in the tree anyway). I.e.,
we define equality on the {\it structure} of the tree with the only content of the tree being
checked being the symbol names.

We have to consider that some of the branches have been reordered, however, as we really have a
``mobile'' instead of a ``tree''.

\begin{code}
instance Eq BunchedContext where
	EmptyContext == EmptyContext = True
	SingleSymbol v _ _ == SingleSymbol w _ _ = w == v
	ContextNode m [] == ContextNode n [] = m == n
	ContextNode m (x:xs) == ContextNode n ys = x == y' && ContextNode m xs == ContextNode n ys'
		where
		y':ys' = sortBy eqX ys
		eqX y z = case (x == y, x == z) of
			(True, True) -> EQ
			(True, _) -> LT
			_ -> GT
	_ == _ = False
\end{code}

Finally we make a {\sf Context} class to allow a general way to look up symbols.

\begin{code}
class Context a where
	lookupSymbol :: String -> a -> Maybe (TempType, SymbolType)
instance Context FlatContext where
	lookupSymbol x (FlatContext vs) = lookup x vs
instance Context BunchedContext where
	lookupSymbol _ EmptyContext = Nothing
	lookupSymbol x (SingleSymbol v tp d)
		| x == v = Just (tp, if d
			then Duplicated
			else Simple)
		| otherwise = Nothing
	lookupSymbol v (ContextNode _ xs) = l xs where
		l [] = Nothing
		l (x:xs) = case lookupSymbol v x of
			Nothing -> l xs
			z -> z
\end{code}

\subsection{Utility functions}

Because we use types in very particular ways a lot in the type inference module, we have some
utility functions to make dealing with them easier.

The {\it zipWithAssertM\_} function does the same over monads.

\begin{code}
zipWithAssertM_ _ _ [] [] = return ()
zipWithAssertM_ ex m (x:xs) (y:ys) = do
	m x y
	zipWithAssertM_ ex m xs ys
zipWithAssertM_ ex _ _ _ = fail $ "Wrong number of arguments in " ++ showExprs ex
\end{code}

During the process of type inference, we use integers (specifically natural numbers) to stand for
type variables. For the sake of brevity, we use the function \textit{t}  to create a type variable.

\begin{code}
t :: Integer -> Type (Either String Integer)
t = PolyType . Right
\end{code}

We introduce the \textit{transformType} function. Given a function, \textit{f}, describing a
substitution that needs to be performed (substituting one type for another type), the
\textit{transformType} function recursively substitutes all sub-types.

\begin{code}
transformType :: (a -> c -> (Type b, c)) -> c -> Type a -> (Type b, c)
transformType f c = t c where
	t c (TupleType ts) = (TupleType ts', c') where (ts', c') = st c ts
	t d (ArrowType (a, b) c) = (ArrowType (a', b') c', d') where
		(a', d1) = st d a
		(b', d2) = st d1 b
		([c'], d') = st d2 [c]
	t c (NamedType s ts) = (NamedType s ts', c') where
		(ts', c') = st c ts
	t c (PolyType s) = f s c
	t c (SafeType u) = (SafeType u', c') where ([u'], c') = st c [u]
	t c (DangerousType u) = (DangerousType u', c') where ([u'], c') = st c [u]
	st c = foldl (\ (ts, c) tp -> let (tp', c') = t c tp
		in (ts ++ [tp'], c')) ([], c)
\end{code}

The higher-order \textit{transformType} function is used by the \textit{genType} function. A type
from the parsing module will have type parameters store only as strings. As we required type
parameters to be strings \textit{or} type variables, we have to generalize that type.

\begin{code}
genType :: Type String -> TempType
genType = fst . transformType (\ s _ -> (PolyType $ Left s, ())) ()
\end{code}

\section{The unification monad}

The unification is a state monad that keeps track of all the various contexts and
type equations.

\begin{code}
data Ctxt = Ctxt { oEnv, gEnv :: FlatContext, pEnv :: BunchedContext, eqs :: EquationExpr,
	newVar :: Integer, expressionDictionary :: [(Expression, Integer)] }
data Unification a = U (Ctxt -> Either (a, Ctxt) String)
instance Functor Unification where
	fmap = liftM
instance Applicative Unification where
	pure x = U $ \ s -> Left (x, s)
	(<*>) = ap
instance Monad Unification where
	return = pure
	U m >>= f = U $ \ r -> case m r of
		Left (x, s) -> let U f' = f x in
			f' s
		Right x -> Right x
	fail x = U $ \ _ -> Right x
\end{code}

\subsection{Changing the environment}

We have four convenience functions to get contexts.

\begin{code}
getOEnv :: Unification FlatContext
getOEnv = U $ \ c@(Ctxt { oEnv = x }) -> Left (x, c)
getGEnv = U $ \ c@(Ctxt { gEnv = x }) -> Left (x, c)
getPEnv :: Unification BunchedContext
getPEnv = U $ \ c@(Ctxt { pEnv = x }) -> Left (x, c)
\end{code}

And similarly we have four convenience functions to set contexts.

\begin{code}
setOEnv :: FlatContext -> Unification ()
setOEnv x = U $ \ c -> Left ((), c { oEnv = x })
setGEnv x = U $ \ c -> Left ((), c { gEnv = x })
setPEnv :: BunchedContext -> Unification ()
setPEnv x = U $ \ c -> Left ((), c { pEnv = x })
\end{code}

And for clearing contexts.

\begin{code}
clearOEnv :: Unification FlatContext
clearOEnv = do
	e <- getOEnv
	setOEnv $ FlatContext []
	return e
clearPEnv :: Unification BunchedContext
clearPEnv = do
	e <- getPEnv
	setPEnv EmptyContext
	return e
\end{code}

We need convenience versions for adding things too. For scoping reasons we add the new
additions onto the front of the list.

\begin{code}
addOEnv (FlatContext x) = do
	y'@(FlatContext y) <- getOEnv
	setOEnv $ FlatContext $ x ++ y
	return y'
addGEnv (FlatContext x) = do
	y'@(FlatContext y) <- getGEnv
	setGEnv $ FlatContext $ x ++ y
	return y'
addPEnv EmptyContext = getPEnv
addPEnv s@(SingleSymbol _ _ _) = do
	y <- getPEnv
	setPEnv $ case shadow s y of
		EmptyContext -> s
		ContextNode Comma xs -> ContextNode Comma (s:xs)
		y -> ContextNode Comma [s, y]
	return y
addPEnv (ContextNode _ []) = getPEnv
addPEnv (ContextNode _ [x]) = addPEnv x
addPEnv c@(ContextNode m xs) = do
	y <- getPEnv
	setPEnv $ case shadow c y of
		EmptyContext -> c
		ContextNode n ys -> if m == n
			then ContextNode m (xs ++ ys)
			else ContextNode n (c:ys)
		s -> ContextNode m $ case m of
			Comma -> (s:xs)
			Semicolon -> [s, c]
	return y
semicolon :: BunchedContext -> BunchedContext -> BunchedContext
semicolon x y = case (x, shadow x y) of
	(EmptyContext, x) -> x
	(x, EmptyContext) -> x
	(s@(SingleSymbol _ _ _), ContextNode Semicolon ys) -> ContextNode Semicolon (s:ys)
	(ContextNode Semicolon xs, ContextNode Semicolon ys) -> ContextNode Semicolon (xs ++ ys)
	(ContextNode Semicolon xs, y) -> ContextNode Semicolon (y:xs)
	(x, y) -> ContextNode Semicolon [x, y]
\end{code}

With bunched contexts it's especially important to remove ``shadowed'' variables as the contexts
may get reordered.

\begin{code}
shadow EmptyContext x = x
shadow (ContextNode _ xs) x = foldr shadow x xs
shadow _ EmptyContext = EmptyContext
shadow (SingleSymbol v _ _) s@(SingleSymbol w _ _)	| v == w = EmptyContext
							| otherwise = s
shadow s@(SingleSymbol v _ _) (ContextNode m xs) = case filter notEmpty $ map (shadow s) xs of
		[] -> EmptyContext
		[x] -> x
		xs' -> ContextNode m xs'
	where
	notEmpty EmptyContext = False
	notEmpty _ = True
\end{code}

We would also like to ``destroy'' (consume) variables in our context, too, to prevent duplication.
At the top-level we make a distinction between whether we have a semicolon and are forced to make
a ``choice'' or whether we have a comma and are allowed to consume the rest of the tree.

\begin{code}
destroyVarInEnv s@(SingleSymbol v tp _) w
	| v == w = SingleSymbol v tp True
destroyVarInEnv (ContextNode m ys) v = ContextNode m $ case m of
		Semicolon -> x':map (makeDuplicated (\ _ -> True)) xs
		Comma -> x':xs
	where
	x' = makeDuplicated (\ (SingleSymbol w _ _) -> w == v) x
	x:xs = sortBy (\ x y -> contains v x `compare` contains v y) ys
	contains v EmptyContext = GT
	contains v (SingleSymbol w _ _)	| v == w = LT
					| otherwise = GT
	contains _ (ContextNode _ []) = GT
	contains v (ContextNode _ (x:xs))
		| contains v x == LT = LT
		| otherwise = contains v $ ContextNode Comma xs
	makeDuplicated f v@(SingleSymbol w tp _)
		| f v = SingleSymbol w tp True
		| otherwise = v
	makeDuplicated f (ContextNode m xs) = ContextNode m $ map (makeDuplicated f) xs

destroyPEnv x = do
	env <- getPEnv
	setPEnv $ destroyVarInEnv env x
intersectPEnvs [] = return ()
intersectPEnvs xs = setPEnv $ intersectContexts xs
\end{code}

Intersection is relatively easy owing to the fact that we keep the structure of the tree intact
when consuming variables, with the exception that subtrees may be reordered. Note that in the
{\sf SingleSymbol} case we do a {\it max} on $d_1$ and $d_2$ to ensure that if the variable has
been consumed in one, it will be considered consumed.

\begin{code}
intersectContext EmptyContext EmptyContext = EmptyContext
intersectContext s@(SingleSymbol v tp d1) t@(SingleSymbol _ _ d2)
	| s == t = SingleSymbol v tp (max d1 d2)
intersectContext c@(ContextNode m xs) d@(ContextNode _ ys)
		| c == d = ContextNode m $ i xs ys where
	i [] [] = []
	i (x:xs) ys = x':i xs ys' where
		y':ys' = sortBy eqX ys
		eqX y z = case (x == y, x == z) of
			(True, True) -> EQ
			(True, _) -> LT
			_ -> GT
		x' = intersectContext x y'
intersectContext a1 a2 = error $ show (a1, a2)

intersectContexts [x] = x
intersectContexts (x:xs) = intersectContext x $ intersectContexts xs
\end{code}

When doing {\sf peek}s we must also define the difference between an original context and a modified
(where some variables have been consumed) context. Because we need to fill the hole where the
modification came, we return a function that describes what to fill the hole with. The use of
{\it matchUp} in the {\sf ContextNode} case is to deal with the fact that while {\it xs} and
{\it ys} should be structurally equivalent, they may have been reordered, i.e., they should be
treated as an unordered list.

\begin{code}
bunchedContextDifference :: BunchedContext -> BunchedContext ->
	(BunchedContext -> BunchedContext, BunchedContext)
bunchedContextDifference EmptyContext EmptyContext = (id, EmptyContext)
bunchedContextDifference s@(SingleSymbol x1 tp1 d1) t@(SingleSymbol x2 tp2 d2)
	| x1 == x2 && tp1 == tp2 = if d1 == d2
		then (addContext t, EmptyContext)
		else (addContext t, s)
bunchedContextDifference (ContextNode d1 xs) t@(ContextNode d2 ys)
	| d1 == d2 = case ys1 of
		[] -> (addContext t, EmptyContext)
		[(x, y)] -> let (f, y') = bunchedContextDifference x y in
			(\ y -> addContext x' $ f y, y')
		_ -> (addContext x', ContextNode Comma ys')
	where
	xs1 = matchUpWith xs ys
	matchUpWith xs [] = xs
	matchUpWith xs (y:ys) = case removeBy (sameStructure y) xs of
		Nothing -> matchUpWith xs ys
		Just (x', xs') -> x':matchUpWith xs' ys
	(xs', ys1) = partitionWith eqB xs1 ys
	ys' = map fst ys1
	x' = ContextNode Comma (xs' ++ map snd ys1)
	partitionWith _ [] [] = ([], [])
	partitionWith f (x:xs) (y:ys)	| f x y = (y:xs', ys')
					| otherwise = (xs', (x, y):ys')
		where (xs', ys') = partitionWith f xs ys
	eqB :: BunchedContext -> BunchedContext -> Bool
	eqB EmptyContext EmptyContext = True
	eqB (SingleSymbol x1 tp1 d1) (SingleSymbol x2 tp2 d2)
		| x1 == x2 && tp1 == tp2 = d1 == d2
	eqB (ContextNode d1 xs) (ContextNode d2 ys)
		| d1 == d2 = and $ zipWith eqB xs ys
	sameStructure EmptyContext EmptyContext = True
	sameStructure (SingleSymbol x1 _ _) (SingleSymbol x2 _ _) = x1 == x2
	sameStructure (ContextNode d1 xs) (ContextNode d2 ys) =
			d1 == d2 && (and $ zipWith sameStructure xs' ys) where
		xs' = matchUpWith xs ys
	sameStructure _ _ = False
\end{code}

\begin{code}
addContext t x = case x of
	EmptyContext -> t
	ContextNode _ [] -> t
	ContextNode _ [y] -> ContextNode Comma [shadow y t, y]
	ContextNode Comma xs -> ContextNode Comma (shadow x t:xs)
	_ -> ContextNode Comma [shadow x t, x]
\end{code}

\subsection{Temporarily changing context}

\begin{code}
withoutContext clear restore getNewState m = do
	oldState <- clear
	m' <- m
	newState <- getNewState oldState
	restore newState
	return m'
\end{code}

Sometimes we want to type terms without some context.

\begin{code}
withoutPEnv :: Unification a -> Unification a
withoutPEnv = withoutContext clearPEnv return setPEnv
\end{code}

Sometimes we want to type terms with some temporary context.

\begin{code}
withOEnv :: FlatContext -> Unification a -> Unification a
withOEnv env = withoutContext (addOEnv env) setOEnv return
withPEnv :: BunchedContext -> Unification a -> Unification a
withPEnv env = withoutContext (addPEnv env) setPEnv markPEnv
withREnv f ftp = withoutContext (addGEnv $ FlatContext [(f, (ftp, Simple))]) setGEnv return
\end{code}

The {\it withOnlySafeVariables} function is used to remove variables which could be
universally-typed. This is used for type inference of {\it unfold} and record constructs
where we need to ensure that no universally-typed variables are used.

\begin{code}
withOnlySafeVariables = withoutContext dangerousPEnv return setPEnv where
	dangerousPEnv = do
		oldPEnv <- getPEnv
		setPEnv $ removeDangerous oldPEnv
		return oldPEnv
	removeDangerous EmptyContext = EmptyContext
	removeDangerous (SingleSymbol _ (DangerousType _) _) = EmptyContext
	removeDangerous s@(SingleSymbol _ _ _) = s
	removeDangerous (ContextNode grouping ctxts) = ContextNode grouping $ map removeDangerous ctxts
\end{code}

The {\it markPEnv} function takes an original context plus a modified context. The modified context
has some variables not in the original context but has also marked some of the variables in the
original context as being duplicated. Our task is to return the original context while marking
everything that should be as duplicated.

\begin{code}
markPEnv oldState = do
		newState <- getPEnv
		return $ d (l newState) oldState where
	d _ EmptyContext = EmptyContext
	d vars (SingleSymbol v t _) = SingleSymbol v t $ fromJust $ lookup (v, t) vars
	d vars (ContextNode disc cs) = ContextNode disc $ map (d vars) cs
	l EmptyContext = []
	l (SingleSymbol v t d) = [((v, t), d)]
	l (ContextNode _ cs) = do
		c <- cs
		l c
\end{code}

\subsection{Creating fresh type variables}

A big advantage to putting unification is a monad is to opaquely handle the creation of
fresh type variables.

\begin{code}
freshTypeVariable :: Unification Integer
freshTypeVariable = U $ \ c@(Ctxt { newVar = n }) -> Left (n, c { newVar = n + 1 })
\end{code}

In reality we will likely want a number of fresh type variables at one time. We introduce
the utility function {\it takeM} for this purpose.

\begin{code}
takeM :: (Monad m, Enum n, Bounded n) => n -> m a -> m [a]
takeM n m = replicateM (fromEnum n) m
\end{code}

\subsection{Dealing with equations}

Unification proper---i.e., solving type equations---will likely have to be done in detail in
another section, but here we introduce the ``lighter'' function dealing with equations.

\begin{code}
getEquations = U $ \ c@(Ctxt { eqs = x }) -> Left (x, c)
\end{code}

\begin{code}
addUniversal :: Integer -> [(Integer, TempType, [Expression])] -> Unification ()
addUniversal v eqs = U $ \ c@(Ctxt { eqs = e }) -> Left ((), c { eqs = QF e [] [v] eqs })
addExistential v eqs = U $ \ c@(Ctxt { eqs = e }) -> Left ((), c { eqs = QF e [v] [] eqs })
\end{code}

\begin{code}
resetEquations = U $ \ c -> Left ((), let c1 = c { eqs = Empty} in c1 { newVar = 0 })
\end{code}

We allow the ability to add an equation which does not need to be quantified.

\begin{code}
addEquation :: (Integer, TempType, [Expression]) -> Unification ()
addEquation eq = U $ \ c@(Ctxt { eqs = QF z ex un eqs }) ->
	Left ((), c { eqs = QF z ex un (eq:eqs) })
\end{code}

For convenience, we allow the ability to add many equations at once.

\begin{code}
addExistentials [] eqs = mapM_ addEquation eqs
addExistentials [v] eqs = addExistential v eqs
addExistentials (v:vs) eqs = do
	addExistential v []
	addExistentials vs eqs
\end{code}

The expression dictionary is used to carry typing information about particular expressions or
sub-expressions out of the type inference module.

\begin{code}
getExprDictionary = U $ \ c@(Ctxt { expressionDictionary = ed }) -> Left (ed, c)
addExprWithTypeVar ex tp = U $ \ c@(Ctxt { expressionDictionary = ed }) ->
	Left ((), c { expressionDictionary = (ex, tp):ed })
\end{code}

\section{Adding declarations to the environment}

A symbol in the environment can be one of three valid types. The fourth type, {\sf Duplicated},
helps us provide good error message. The fifth type, {\sf Recursive}, lets us know if a function
is recursive or not, which is improtant to know when enforcing peek restrictions.

\begin{code}
data SymbolType
	= Simple
	| Constructor
	| Destructor
	| Duplicated
	deriving (Eq, Show)
\end{code}

Given a function declaration---i.e., a function with a type but no body---with name $s$ and type
$t$, we simply add it that mapping to the environment. It does not have a constructor ordinal value
since it is a function symbol.

\begin{code}
addDeclaration :: FunctionDecl -> Unification ()
addDeclaration (s, [TypeConstraint t]) = do
	addGEnv $ FlatContext [(s, (genType t, Simple))]
	return ()
\end{code}

\section{Adding constructors to the environment}

The \textit{symbolsFromType} function takes a type definition---i.e., a type name and a list of
constructors---and adds them to the environment. The constructor ordinal values assigned to each
constructor are just taken from the order in which they're given.

This function needs to be changed. It should not allow redefining types nor redefining
constructors.

\begin{code}
symbolsFromType :: TypeDefinition -> Unification ()
symbolsFromType (Inductive (tp, parms) cons) = do
		addGEnv $ FlatContext $ map addCons cons
		return ()
	where
	retTp = NamedType tp (map mkT parms)
	mkT p = PolyType (Left p)
	addCons (c, os, r) = (c, (cnsTp, Constructor)) where
		cnsTp = ArrowType ([], map genType os) retTp
symbolsFromType (Coinductive (tp, parms) dests) = do
		addGEnv $ FlatContext $ map addDest dests
		return ()
	where
	addDest (d, os, qs) = (d, (dstTp, Destructor)) where
		dstTp = ArrowType ([], map genType os) $
			makeRetTp $ map genType qs
		makeRetTp [x] = x
		makeRetTp x = TupleType x
\end{code}

\section{Adding functions to the environment}

The functions seen later in this chapter do the heavy lifting of the typing system. However, we
need functions to tie of the loose ends. The \textit{addFunction} function takes a definition of a
function, types it, then returns the new environment, the transformed function (annotated with type
information), and a function declaration.

\begin{code}
addFunction :: FunctionDefinition -> Unification (FunctionDecl,
	Expression -> Maybe (Type (Either String Integer)))
addFunction f@(fnme, _, _, _, _) = do
		clearOEnv
		clearPEnv
		resetEquations
		typeFun f
		gEnv <- getGEnv
		let ftp = fst $ fromJust $ lookupSymbol fnme gEnv
		dictionary <- unify
		expressionDictionary <- getExprDictionary
		return ((fnme, [TypeConstraint $ makeTypeConcrete ftp]),
			lookupExpr expressionDictionary dictionary)
	where
	makeTypeConcrete t = fst $ transformType (\ (Left x) () -> (PolyType x, ())) () t
	lookupExpr exprDict typeDict e = do
		t <- lookup e exprDict
		return $ cleanReturnType $ typeDict t
\end{code}

\subsection{Typing functions}

The {\it typeFunction} function returns the type of a function without any monad nonsense.

\begin{code}
typeFunction :: FunctionDefinition -> FlatContext -> Value FunctionDecl
typeFunction f env = do
	decl <- initialEnvironment $ do
		addGEnv env
		(f', _) <- addFunction f
		return f'
	return $ fst decl
\end{code}

\section{The initial environment}

The initial environment contains primitive data types and functions.

For the natural numbers, we introduce the constructors \texttt{Zero} and
\texttt{Succ} as well as the usual arithmetic and relational operators.
For the booleans, we introduce \texttt{True} and \texttt{False} as well
as the usual logical operations.

\begin{code}
initialTypes :: [TypeDefinition]
initialTypes = [Inductive ("Nat", []) [("Zero", [], nat), ("Succ", [nat], nat)],
		Inductive ("Bool", []) [("False", [], bool), ("True", [], bool)],
		Inductive ("List", ["a"]) [("Nil", [], lsta), ("Cons", [a, lsta], lsta)]] where
	nat = NamedType "Nat" []
	bool = NamedType "Bool" []
	a = PolyType "a"
	lsta = NamedType "List" [a]
\end{code}

There are no predefined functions on lists, just their constructors.

A note about the convenience variables and functions defined in there. \textit{ms} is an abstract
function which constructs a symbol out of arguments and a return value. It is used by \textit{fn}
(to construct functions) and \textit{c} (to construct constructors). \textit{nat}, \textit{bool}
and \textit{lst} are variables which construct types. The variable \textit{a} stands for some
polymorphic type.

\begin{code}
initialEnvironment :: (Monad m) => Unification a -> m (a, FlatContext)
initialEnvironment (U f) = case f $ Ctxt { oEnv = FlatContext [], pEnv = EmptyContext,
			gEnv = FlatContext $ natCs ++ listCs ++ boolCs, eqs = Empty, newVar = 0, expressionDictionary = [] } of
		Left (x, Ctxt { gEnv = env }) -> return $ (x, env)
		Right x -> fail x
	where
	c i ps r = (ArrowType ps r, Constructor)
	nat = NamedType "Nat" []
	bool = NamedType "Bool" []
	a = PolyType (Left "a")
	lst a = NamedType "List" [a]
	listCs = [("Nil", c 0 ([], []) (lst a)), ("Cons", c 1 ([], [a, lst a]) (lst a))]
	boolCs = [("False", c 0 ([], []) bool), ("True", c 1 ([], []) bool)]
	natCs = [("Zero", c 0 ([], []) nat), ("Succ", c 1 ([], [nat]) nat)]
\end{code}

\section{Typing functions}

We have seen that adding type information to the environment from data type definitions is quite
easy. The other major way to add type information, by typing a function, is considerably more
involved. This process consumes all of the type inference as it encompasses typing expressions.

\subsection{The \textit{typeFun} function}	\label{typefunction}

The major entry point into typing is the \textit{typeFun} function. Given an environment of
already typed symbols and a definition of a function, the \textit{typeFun} function creates
a list of type equations that need to be unified. In addition, it transforms the function definition
from one involving the parse tree to one that can be used by the lambda lifter.

We note the convention of passing around an integer, $n$, when generating type equations. Type
variables in the typing system are represented at integers. The integer $n$ represents the next
free type variable which can be created.

When typing a function, we immediately create a number of new type variables. The variable $n$
itself stands for the type of the function, which would turn out to be an arrow type. \textit{retN}
($n+1$) stands for the return type of the function: equivalently, this is the type of the expression
that the function evaluates to when all of its parameters are in context. \textit{argN} ($n+2$) is
the type of the first argument; $\mathit{argN}+1$ is the type of the second argument, and so on.
The \textit{env1} environment is the incoming environment, \textit{env}, taken in union with the
function's parameters. I.e., we add the parameters to the environment.

The \textit{eq1} type equation defines the type variable $n$, i.e., the type of the function as
a whole. This equation states that the type of the function must be an arrow type between its
parameters and its return value.

The real work is done by the \textit{typeExpression} function (see section~\ref{typeexpr}). We
simply pass it our expression, our augmented environment and our initial type equation. If the
programmer has added explicit type constraints to the function definition, we add type equations
to reflect those constraints, but otherwise the type equations to unify come from typing the
expression.

After the type equations have been generated, we set up the \textit{typeLookup} function, which uses
the \textit{typeDictionary} function (see subsection~\ref{typedictionary}) to map between type
variables and their concrete types, as unified.

\begin{code}
typeFun (f, constraints, os, ps, e) = do
	fVar <- freshTypeVariable
	eVar <- freshTypeVariable
	oVars <- takeM (length os) freshTypeVariable
	pVars <- takeM (length ps) freshTypeVariable
	let oVars' = map t oVars
	let pVars' = map t pVars
	addExistentials (fVar:eVar:oVars ++ pVars)
		[(fVar, ArrowType (oVars', pVars') (t eVar), [e])]
	case constraints of
		[] -> return ()
		[TypeConstraint t] -> mapM_ addEquation $ (eVar, rtpC, [e]):osEq ++ psEq where
			ArrowType (osC, psC) rtpC = genType t
			osEq = zip3 oVars osC $ repeat [e]
			psEq = zip3 pVars psC $ repeat [e]
	addOEnv $ FlatContext $ zipWith (\ v t -> (v, (t, Simple))) os oVars'
	addPEnv $ ContextNode Comma $ zipWith (\ v t -> SingleSymbol v t False) ps pVars'
	typeExpression [] e eVar
	dictionary <- unify
	let fType = cleanReturnType $ dictionary fVar
	fType' <- case constraints of
		[] -> crunchPolies fType
		[TypeConstraint t] -> matchConstraint f t fType
	let allStrings = [ b ++ [a] | b <- "":allStrings, a <- ['a'..'z'] ]
	let genStringPoly x = PolyType $ allStrings !! fromInteger x
	let tp' = genType $ makeTypeConcrete genStringPoly PolyType fType'
	addGEnv $ FlatContext [(f, (tp', Simple))]
\end{code}

We now look at the utility functions used solely for \textit{typeFunction}. The
\textit{crunchPolies} function for purely cosmetic purposes. Occassionally after unifying, we might
find that a symbol has type, e.g., $c\rightarrow(a\rightarrow f)$. We would like to do an
$\alpha$-renaming (or $\alpha$-reduction) so that the type signature starts at the letter $a$ and
works sequentially towards $z$. I.e., the example above would be ``crunched'' into $a\rightarrow
(b\rightarrow c)$.

\begin{code}
crunchPolies :: (Monad m) => TempType -> m TempType
crunchPolies t = matchTypes Nothing t t
\end{code}

The {\it cleanReturnType} function cleans ``safeness'' out of return types. An input parameter
may have been forced safe and then made the return value, but this does not mean the concept
of safeness has any meaning as a return type. Only danger has meaning in return types.

\begin{code}
cleanReturnType (ArrowType params (SafeType t)) = ArrowType params t
cleanReturnType t = t
\end{code}

In the case that an explicit type constraint has been given, we would like not to ``crunch'' the
parametric types in a type signature. In that case, we try to give a type signature which matches
\textit{exactly} the constraint given.

\begin{code}
matchConstraint :: (Monad m) => String -> Type String -> TempType -> m TempType
matchConstraint f t1 t2 = matchTypes (Just f) (genType t1) t2
\end{code}

The \textit{makeTypeConcrete} function takes a type with type variables and converts into a
``concrete'' type without type variables. Type parameters take the place of unbound type variables.

\begin{code}
makeTypeConcrete :: (Integer -> Type a) -> (String -> Type a)
	-> TempType -> Type a
makeTypeConcrete f g (PolyType (Left s)) = g s
makeTypeConcrete f g (PolyType (Right s)) = f s
makeTypeConcrete f g (TupleType ts) = TupleType
	(map (makeTypeConcrete f g) ts)
makeTypeConcrete f g (NamedType s ts) = NamedType s (map
	(makeTypeConcrete f g) ts)
makeTypeConcrete f g (ArrowType (a, b) c) = ArrowType
	(map (makeTypeConcrete f g) a, map (makeTypeConcrete f g) b)
	(makeTypeConcrete f g c)
makeTypeConcrete f g (SafeType t) = SafeType $ makeTypeConcrete f g t
makeTypeConcrete f g (DangerousType t) = DangerousType $ makeTypeConcrete f g t
\end{code}

\subsection{Unifying and constructing the type dictionary}	\label{typedictionary}

The {\it unify} function is the entry point for unification, though it does very little. The
real work is done by the {\it u} function, which both unifies and simultaneously looks up
entries in the dictionary.

\begin{code}
unify :: Unification (Integer -> TempType)
unify = do
	eqs <- getEquations
	eqs' <- u eqs
	let chase n = case lookup3 n eqs' of
		Just (PolyType (Right x), _) -> chase x
		Just (t, _) -> t
		Nothing -> PolyType $ Right n
	return chase
\end{code}

The {\it u} function starts at the innermost quantification in the unification expression.
Eventually, it will remove all quantifications.

\begin{code}
u :: EquationExpr -> Unification [(Integer, TempType, [Expression])]
u (QF z exsts (univ:univs) eqs) = case findWith univ eqs of
	Just ((_, tp, e), eqs') -> if tp == (DangerousType $ PolyType $ Right univ)
		then u $ QF (addUniv z univ) exsts univs eqs'
		else fail $ "Type " ++ show tp ++ " not universally quantified " ++
			"in expression " ++ showExprs e
	Nothing -> u $ QF (addUniv z univ) exsts univs eqs
u (QF z (exst:exsts) [] eqs) = case findWith exst eqs of
	Just (eq, eqs1) -> do
		(eq', eqs2) <- sub eq eqs1
		eqs' <- u $ QF z exsts [] $! eqs2
		return $ eq':eqs'
	Nothing -> u $ QF (addExst z exst) exsts [] eqs
u (QF Empty [] [] eqs) = return eqs
u (QF z [] [] eqs) = u $ addEqs z eqs
u Empty = return []
\end{code}

\begin{code}
findWith var eqs = do
		i <- findIndex (\ (v, _, _) -> v == var) eqs
		return (eqs !! i, sortBy startsWithVar $ take i eqs ++ drop (i + 1) eqs)
	where
	startsWithVar (v, _, _) (w, _, _) = compare (w == var) (v == var)
\end{code}

\begin{code}
addUniv (QF z ex un eqs) v = QF z ex (v:un) eqs
addUniv Empty _ = Empty
addExst (QF z ex un eqs) v = QF z (v:ex) un eqs
addExst Empty _ = Empty
addEqs (QF z ex un eqs) eqs2 = QF z ex un (eqs ++ eqs2)
\end{code}

The occurs check is necessary to guarantee that symbols have only finite type. Well-typed
expressions must have only finite type. We guarantee this by ensuring that no type variable ever
references itself. We have to make special exceptions for {\sf SafeType} and {\sf DangerousType}
as they shouldn't otherwise factor into whether a type variable is considered to occur. The
{\it sub} function below guarantees that $X=X$ variables are removed, so we don't need to give
them special consideration here.

\begin{code}
occurs (_, SafeType (PolyType _)) = False
occurs (_, DangerousType (PolyType _)) = False
occurs (i, tp) = snd $ transformType (\ p v -> (PolyType p, v || f p)) False tp where
	f (Left _) = False
	f (Right x) = i == x
\end{code}

The \textit{sub} function takes a type variable substitution and applies it through all the
remaining equations in the list. This can often create new equations. For instance, consider
substituting the equation $X=\alpha\rightarrow Y$ into the equation $X=Y\rightarrow\alpha$.
Because we introduce structure into the second equation, which is not allowed, we must break it up
into the new equations $Y=\alpha$ and $Y=\alpha$ (the same equation repeated). Since we
are created new equations like this, it is necessary to perform the occurs check again.

\begin{code}
sub eq [] = return (eq, [])
sub eq@(i, tp1, expr1) eqs'@((x, tp2, expr2):eqs)
	| tp1 == PolyType (Right i) = return (eq, eqs')
	| i == x = do
		(tp1', eqs') <- breakUp expr1 expr2 tp1 tp2
		sub (i, tp1', expr1) (eqs' ++ eqs)
	| tp2' == PolyType (Right x) = sub eq eqs
	| occurs (x, tp2') = unificationError expr1 expr2 "Occurs check fails"
	| otherwise = do
		(eq', eqs') <- sub eq eqs
		return (eq', (x, tp2', expr2):eqs')
	where
	tp2' = fst $ transformType (\ p () -> (f p, ())) () tp2
	f tp2@(Right x)	| x == i = tp1
			| otherwise = PolyType tp2
	f tp = PolyType tp
\end{code}

The \textit{breakUp} function breaks up a structured equation into new equations, as described
above. Note that we are able to match safe and dangerous types against
their non-safety-marked cognate types.

\begin{code}
breakUp ex1 ex2 (SafeType t) (SafeType u) = do
	(t', eqs) <- breakUp ex1 ex2 t u
	return (SafeType t', eqs)
breakUp ex1 ex2 (DangerousType t) (DangerousType u) = do
	(t', eqs) <- breakUp ex1 ex2 t u
	return (DangerousType t', eqs)
breakUp ex1 ex2 t1@(SafeType _) t2@(DangerousType _) = unificationError ex1 ex2 $
	"Safe type '" ++ show t1 ++ "' cannot match with dangerous type '" ++ show t2 ++ "'"
breakUp ex1 ex2 t2@(DangerousType _) t1@(SafeType _) = unificationError ex1 ex2 $
	"Safe type '" ++ show t1 ++ "' cannot match with dangerous type '" ++ show t2 ++ "'"
breakUp ex1 ex2 t1 (SafeType t2) = do
	(t1', eqs) <- breakUp ex1 ex2 t1 t2
	return (SafeType t1', eqs)
breakUp ex1 ex2 t1 (DangerousType t2) = do
	(t1', eqs) <- breakUp ex1 ex2 t1 t2
	return (DangerousType t1, eqs)
breakUp ex1 ex2 t1@(SafeType _) t2 = breakUp ex1 ex2 t1 (SafeType t2)
breakUp ex1 ex2 t1@(DangerousType _) t2 = breakUp ex1 ex2 t1 (DangerousType t2)
breakUp _ ex t@(PolyType (Right x)) y = return (t, [(x, y, ex)])
breakUp _ ex y (PolyType (Right x)) = return (y, [(x, y, ex)])
breakUp ex1 ex2 t@(PolyType (Left x)) (PolyType (Left y))
	| x == y = return (t, [])
	| otherwise = unificationError ex1 ex2 $ "Cannot match type '" ++ x ++ "' with type '" ++
		y ++ "'"
breakUp ex1 ex2 (TupleType ts) (TupleType us) = do
	(ts', eqs) <- breakUpMany ex1 ex2 ts us
	return (TupleType ts', eqs)
breakUp ex1 ex2 t1@(NamedType s ts) t2@(NamedType r us)
	| s == r = do
		(ts', eqs) <- breakUpMany ex1 ex2 ts us
		return (NamedType s ts', eqs)
	| otherwise = unificationError ex1 ex2 $ "Cannot match type '" ++ show t1 ++
		"' with type '" ++ show t2 ++ "'"
breakUp ex1 ex2 (ArrowType (a, b) x) (ArrowType (c, d) y) = do
	(a', ac) <- breakUpMany ex1 ex2 a c
	(b', bd) <- breakUpMany ex1 ex2 b d
	(x', xy) <- breakUp ex1 ex2 x y
	return (ArrowType (a', b') x', ac ++ bd ++ xy)
breakUp ex1 ex2 t1 t2 = unificationError ex1 ex2 $ "Cannot match type '" ++ show t1 ++
	"' with type '" ++ show t2 ++ "'"
breakUpMany _ _ [] [] = return ([], [])
breakUpMany ex1 ex2 (x:xs) (y:ys) = do
	(x', eqs1) <- breakUp ex1 ex2 x y
	(xs', eqs2) <- breakUpMany ex1 ex2 xs ys
	return (x':xs', eqs1 ++ eqs2)
\end{code}

We make a handy function to handle unification errors.

\begin{code}
unificationError expr1 expr2 msg = fail $ "Unification fails: " ++ msg ++ " in expression " ++
	showExprs expr1 ++ "\nand in expression " ++ showExprs expr2
\end{code}

The \textit{matchTypes} function provides a generic way of ensuring that two types, $t_1$ and
$t_2$, match. $t_2$ is the type that is being matched and $t_1$ is the type that it \textit{must}
match. It is not used for unification, but for ensuring constraints.

\begin{code}
matchTypes :: (Monad m) => Maybe String -> TempType -> TempType -> m TempType
matchTypes nme t1 t2 = do
		(t2', _, _) <- m t1 t2 [] nme'
		return t2' where
	nme' = case nme of
		Just _ -> Nothing
		Nothing -> Just 0
	Just f = nme
	m :: (Monad p) => TempType -> TempType -> [(Integer, TempType)] -> Maybe Integer ->
		p (TempType, [(Integer, TempType)], Maybe Integer)
	m (ArrowType (a, b) x) (ArrowType (c, d) y) l n = do
		(c', l1, n1) <- subEs (zip a c) l n
		(d', l2, n2) <- subEs (zip b d) l1 n1
		([y'], l', n') <- subEs [(x, y)] l2 n2
		return (ArrowType (c', d') y', l', n')
	m (TupleType ts) (TupleType us) l n = do
		(us', l', n') <- subEs (zip ts us) l n
		return (TupleType us', l', n')
	m (NamedType s ts) (NamedType r us) l n
		| s == r = do
			(us', l', n') <- subEs (zip ts us) l n
			return (NamedType s us', l', n')
		| otherwise = fail $ "Symbol " ++ f ++ " does not meet " ++
			"constraints. Cannot match inferred '" ++ r ++
			"' against expected '" ++ s ++ "'"
	m (SafeType t) (SafeType u) l n = do
		([t'], l', n') <- subEs [(t, u)] l n
		return (SafeType t', l', n')
	m (DangerousType t) (DangerousType u) l n = do
		([t'], l', n') <- subEs [(t, u)] l n
		return (DangerousType t', l', n')
	m t (PolyType (Right x)) l n = case lookup x l of
		Just u -> if t == u || n /= Nothing
			then return (u, l, n)
			else fail $ "Cannot match symbol '" ++ f ++
				"' with its constraints"
		Nothing -> case n of
			Just n -> let t' = PolyType (Right n)
				in return (t', (x, t'):l, Just (n + 1))
			Nothing -> return (t, (x, t):l, n)
	m _ _ _ _ = fail $ "Cannot match symbol '" ++ f ++ "' with " ++
		"its constraints. Cannot match type '" ++ show t1 ++ "' with constraints '" ++
		show t2 ++ "'"
	subEs es l n = foldM doE ([], l, n) es where
		doE (es, l, n) (e1, e2) = do
			(e', l', n') <- m e1 e2 l n
			return (es ++ [e'], l', n')
\end{code}

\section{Inferring types from expressions}

We have dealt with unification in previous sections. We now deal with generating type equations,
inferring typing information from expressions.

\subsection{The \textit{typeExpression} function}	\label{typeexpr}

The \textit{typeExpression} function provides most of the inference for generating type equations.

\begin{code}
typeExpression :: [Expression] -> Expression -> Integer -> Unification ()
\end{code}

The simplest case is variable lookup. There are actually a number of rules for this.

\[\begin{array}{c}
\infer[A=B]{\Gamma,x:B\mid\Delta\parallel\Sigma\mapsto x:A}{}\\
\\
\infer[A=B]{\Gamma\mid\Delta,x:B\parallel\Sigma\mapsto x:A}{}\\
\\
\infer[\mbox{In dotted box}\rhd A=B]{\Gamma\mid\Delta\parallel\Sigma,x:B\mapsto x:A}{}
\end{array}\]

\begin{code}
typeExpression exprs (VariableName "_") _ = fail $ "Cannot reference anonymous variable in " ++
	showExprs exprs
typeExpression exprs expr@(VariableName v) a = do
		oEnv <- getOEnv
		pEnv <- getPEnv
		let exprs' = expr:exprs
		let withValue = wv exprs'
		case (lookupSymbol v oEnv, lookupSymbol v pEnv) of
			(Just (_, Simple), Just (_, Simple)) -> fail $ "Ambiguous variable reference " ++
				"to variable " ++ v ++ " which could be either opponent or player, in " ++
				showExprs exprs
			(Just (t, Simple), _) -> addEquation (a, t, exprs')
			(_, Just (_, Duplicated)) -> fail $ "Variable " ++ v ++ " has been duplicated "
				++ "in " ++ showExprs exprs
			(Nothing, Just (t, Simple)) -> withValue t
			(Nothing, Nothing) -> fail $ "Variable " ++ v ++ " is not in scope in expression " ++
				showExprs exprs
	where
	wv exprs' t = do
		destroyPEnv v
		addEquation (a, t, exprs')
\end{code}

For a tuple, we recurse on each sub-expression.

\[\infer[\exists B_1,\ldots,B_n.A=B_1\otimes\cdots\otimes B_n]
{\Gamma\mid\Delta_1,\ldots,\Delta_n\parallel\Sigma_1,\ldots,\Sigma_n\mapsto (e_1,\ldots,e_n):A}
{\Gamma\mid\Delta_1\parallel\Sigma_1\mapsto e_1:B_1&\cdots&
\Gamma\mid\Delta_n\parallel\Sigma_n\mapsto e_n:B_n}\]

\begin{code}
typeExpression exprs expr@(TupleExpr es) a = do
	vars <- takeM (length es) freshTypeVariable
	let exprs' = expr:exprs
	addExistentials vars [(a, TupleType $ map (PolyType . Right) vars, exprs')]
	zipWithAssertM_ exprs' (typeExpression exprs') es vars
\end{code}

This is similar to what is done for a construction, with one complicated. Even though the
constructor {\sf C} has type $\mid \beta_1,\ldots,\beta_n\mapsto\alpha$, we want to create
fresh type variables for any type parameters in that type signature, hence the use of
$\beta'_i$ and $\alpha'$. See section~\ref{generalizeType} for more detail.

\[
\infer[\begin{array}{r}({\sf C}:~\mid \beta_1,\ldots,\beta_n\mapsto\alpha),\\
	\exists B_1,\ldots,B_n\end{array}
	.
	\begin{array}{l}A=\alpha',\\
	B_1=\beta'_1,\ldots,B_n=\beta'_n\end{array}]
{\Gamma\mid\Delta_1,\ldots,\Delta_n\parallel\Sigma_1,\ldots,\Sigma_n\mapsto
	{\sf C}(e_1,\ldots,e_n):t}
{\Gamma\mid\Delta_1\parallel\Sigma_1\mapsto e_1:B_1&\cdots&
\Gamma\mid\Delta_n\parallel\Sigma_n\mapsto e_n:B_n}
\]

\begin{code}
typeExpression exprs expr@(Construction c es) a = do
	gEnv <- getGEnv
	vars <- takeM (length es) freshTypeVariable
	let exprs' = expr:exprs
	case lookupSymbol c gEnv of
		Just (t, Constructor) -> do
			ArrowType ([], beta') alpha' <- generalizeType t
			addExistentials vars $
				(a, alpha', exprs'):zip3 vars beta' (map (:exprs') es)
			zipWithAssertM_ exprs' (typeExpression exprs') es vars
		_ -> fail $ "No such constructor " ++ c ++ " in expression " ++
			showExprs exprs'
\end{code}

Function calls are nearly identical to constructors except that we have the possibility of
arguments in the opponent world. In this case, we temporarily take out the player context,
$\Delta$, as well as the peek environment, $\Sigma$.

\[
\infer[\begin{array}{r}(f:~\beta_1,\ldots,\beta_m\mid\beta_{m+1},\ldots,\beta_n\mapsto\alpha),\\
	\forall 1\leq i\leq m,\forall m<j\leq n,\exists B_1,\ldots,B_n\end{array}
	.
	\begin{array}{l}A=\alpha',\\
	B_1=\beta'_1,\ldots,B_n=\beta'_n\end{array}]
{\Gamma\mid\Delta_{m+1},\ldots,\Delta_n\parallel\Sigma_1,\ldots,\Sigma_n\mapsto
	f(o_1,\ldots,o_m,p_{m+1},\ldots,p_m):t}
{\Gamma\mid~\parallel~\mapsto o_i:B_i&\cdots&
\Gamma\mid\Delta_j\parallel\Sigma_j\mapsto e_n:B_n}
\]

\begin{code}
typeExpression exprs expr@(FunctionCall f es) a = do
		gEnv <- getGEnv
		let exprs' = expr:exprs
		let wf t = withF t gEnv expr exprs'
		case lookupSymbol f gEnv of
			Just (t, Simple) -> wf t
			_ -> fail $ "No such function " ++ f ++ " in expression " ++ showExprs exprs'
	where
	withF t gEnv expr exprs' = do
		ArrowType (os, ps) r <- generalizeType t
		let olength = length os
		let (eos, eps) = splitAt olength es
		oVars <- takeM olength freshTypeVariable
		pVars <- takeM (length ps) freshTypeVariable
		addEquation (a, r, exprs)
		addExistentials oVars $ zip3 oVars os (map (:exprs') eos)
		addExistentials pVars $ zip3 pVars ps (map (:exprs') eps)
		withoutPEnv $
			zipWithAssertM_ exprs' (typeExpression exprs') eos oVars
		zipWithAssertM_ exprs' (typeExpression exprs') eps pVars
\end{code}

The simplest type of {\bf case} is when we want to project from a tuple.

\[
\infer[\exists B,C_1,\ldots,C_n.B=C_1\otimes\cdots\otimes C_n]
{\Gamma\mid\Delta_1,\Delta_2\parallel\Sigma_1,\Sigma_2\mapsto
	{\bf case}~t~{\bf of}~(y_1,\ldots,y_n).e:A}
{\Gamma\mid\Delta_1\parallel\Sigma_1\mapsto t:B&
\Gamma,y_1:C_1,\ldots,y_n:C_n\mid\Delta_2\parallel\Sigma_2\mapsto e:A}
\]

\begin{code}
typeExpression exprs expr@(Case tm [(TuplePattern ys, e)]) a = do
	b <- freshTypeVariable
	c <- takeM (length ys) freshTypeVariable
	let exprs' = expr:exprs
	addExistentials (b:c) [(b, TupleType $ map t c, exprs')]
	withoutPEnv $ typeExpression exprs' tm b
	addOEnv $ FlatContext $ zipWith (\ (VariablePattern v _) p -> (v, (t p, Simple))) ys c
	typeExpression exprs' e a
\end{code}

Cases are more complicated in that they need to add variables to the context. Note that by the time
we have reached typing, any general patterns will have been expanded out such that we only have
to deal with patterns that are simple constructors with variables to bind.

\[
fix this
\]

\begin{code}
typeExpression exprs expr@(Case tm pes) a = do
		b <- freshTypeVariable
		addExistential b []
		let exprs' = expr:exprs
		withoutPEnv $ typeExpression exprs' tm b
		mapM_ (doPE exprs' installOEnv makeFlatSymbol ignore3 bottom b a) pes
	where
	installOEnv env _ = withOEnv (FlatContext env)
	makeFlatSymbol (VariablePattern v _) tp = (v, (t tp, Simple))
	ignore3 _ _ _ _ = ()
	bottom = error "This should never happen"
\end{code}

Peeks are similar to cases. The difference is that we now involve the peek environment, $\Sigma$.
First we consider a {\bf peek} over a tuple pattern, and we are allowed to duplicate the player
environment, $\Delta$.

\[
fix this
\]

\begin{code}
typeExpression exprs expr@(Peek tm [(TuplePattern y, e)]) a = do
	b <- freshTypeVariable
	c <- takeM (length y) freshTypeVariable
	let exprs' = expr:exprs
	addExistentials (b:c) [(b, TupleType $ map t c, exprs')]
	(unchangedPEnv, changedPEnv) <- savingPEnv exprs' tm b
	forceSafe exprs' changedPEnv
	let buildYEnv (VariablePattern v _) tp = SingleSymbol v (t tp) False
	let yEnv = ContextNode Comma $ zipWith buildYEnv y c
	setPEnv $ unchangedPEnv $ semicolon changedPEnv yEnv
	typeExpression exprs' e a
\end{code}

Next, we consider a {\bf peek} over constructor patterns.

\[
fix this
\]

\begin{code}
typeExpression exprs expr@(Peek tm pes) a = do
		b <- freshTypeVariable
		addExistential b []
		let exprs' = expr:exprs
		contextDiff <- savingPEnv exprs' tm b
		let buildPEnv = bPEnv exprs' contextDiff
		let makeBunchedSymbol (VariablePattern v _) tp = SingleSymbol v (t tp) False
		let ignore3 _ _ _ _ = ()
		let bottom = error "This should never happen"
		penvs <- mapM (doPE exprs' buildPEnv makeBunchedSymbol ignore3 bottom b a) pes
		intersectPEnvs penvs where
	bPEnv exprs envs@(unchangedPEnv, changedPEnv) yEnv _ m = do
		oldPEnv <- getPEnv
		forceSafe exprs changedPEnv
		withoutContext (setSemiPEnv envs yEnv) setPEnv markPEnv m
		newPEnv <- getPEnv
		setPEnv oldPEnv
		return newPEnv
	setSemiPEnv (unchangedPEnv, changedPEnv) yEnv = do
		oldPEnv <- getPEnv
		setPEnv $ unchangedPEnv $ semicolon changedPEnv $ ContextNode Comma yEnv
		return oldPEnv
\end{code}

Folds are also much like cases, except we have annoying universal types to consider.

\[
FIXME
\]

\begin{code}
typeExpression exprs expr@(Fold (f, _, bs) pes coda) a = do
		b <- freshTypeVariable
		c <- takeM (length bs) freshTypeVariable
		addExprWithTypeVar expr a
		e <- freshTypeVariable
		addExistentials (b:c) []
		let exprs' = expr:exprs
		let fOutsideType = ArrowType ([t b], map t c) (t a)
		withREnv f fOutsideType $ typeExpression exprs' coda a
		addUniversal e []
		let fType = ArrowType ([], map t (e:c)) (t a)
		penvs <- withREnv f fType $
			mapM (doPE exprs' (withEnv $ bEnv c) makeOEnv makePEnv e b a) pes
		intersectPEnvs penvs where
	makeOEnv (VariablePattern v _) tp = (v, (t tp, Simple))
	makePEnv _ (VariablePattern _ "_") _ _ = EmptyContext
	makePEnv (alpha, e) (VariablePattern _ v) tp beta
			| alpha == beta = SingleSymbol v e' False
			| otherwise = EmptyContext where
		e' = DangerousType $ t e
	withEnv bEnv oEnv pvEnv m = do
			withoutContext save restore mark m'
			getPEnv
		where
		save = do
			o <- addOEnv $ FlatContext oEnv
			p <- addPEnv pEnv
			return (o, p)
		restore (o, p) = do
			setOEnv o
			setPEnv p
		mark (o, p) = do
			p' <- markPEnv p
			return (o, p')
		m' = do
			m
			getPEnv
		pEnv = ContextNode Comma $ filter notEmpty pvEnv ++ bEnv
		notEmpty EmptyContext = False
		notEmpty _ = True
	bEnv c = zipWith (\ v tp -> SingleSymbol v (t tp) False) bs c
\end{code}

We now turn our attention to terms dealing with coinductive values. First is destruction.

\[
\infer[\begin{array}{r}
	({\sf D}:~\mid\alpha,\beta_1,\ldots,\beta_n\mapsto \gamma),\\
	\exists B,C_1,\ldots,C_n\end{array}
	.
	\begin{array}{l}
	B=\alpha',A=\gamma'\\
	C_1=\beta'_1,\ldots,C_n=\beta'_n\end{array}]
{\Gamma\mid\Delta_0,\ldots,\Delta_n\parallel\Sigma_0,\ldots,\Sigma_n\mapsto
	{\sf D}[x](y_1,\ldots,y_n):A}
{\Gamma\mid\Delta_0\parallel\Sigma_0\mapsto x:B&
\Gamma\mid\Delta_i\parallel\Sigma_i\mapsto y_i:C_i}
\]

\begin{code}
typeExpression exprs expr@(Destruction d (x:ys)) a = do
	gEnv <- getGEnv
	(ArrowType ([], (alpha':beta')) gamma') <- case lookupSymbol d gEnv of
		Just (dtp, Destructor) -> generalizeType dtp
		_ -> fail $ "No such destructor " ++ d ++ " in expression " ++
			showExprs (expr:exprs)
	b <- freshTypeVariable
	c <- takeM (length ys) freshTypeVariable
	let exprs' = expr:exprs
	let eqns = (b, alpha', x:exprs'):(a, gamma', exprs'):(zip3 c beta' $ map (:exprs') ys)
	addExistentials (b:c) eqns
	typeExpression exprs' x b
	zipWithAssertM_ exprs' (typeExpression exprs') ys c
\end{code}

Records are type as follows:

\[
\infer[\begin{array}{r}
	({\sf D}_i:~\mid\alpha_i,\beta_{i,1},\ldots,\beta_{i,k_i}\mapsto\gamma_i),\\
	\exists B_1,\ldots,B_n,C_1,\ldots,C_n\end{array}
	.
	\begin{array}{l}
	A=\alpha'_1,\ldots,A=\alpha'_n,\\
	\tilde{B}_1=\tilde{\beta'}_1,\ldots,\tilde{B}_n=\tilde{\beta'}_n,\\
	C_1=\gamma'_1,\ldots,C_n=\gamma'_n\end{array}]
{\Gamma\mid\Delta_1,\ldots,\Delta_n\parallel\Sigma_1,\ldots,\Sigma_n\mapsto
	\left({\sf D}_i:\tilde{y}_i.e_i\right):A}
{\Gamma\mid\Delta_i,\tilde{y}_i:\tilde{B}_i\parallel\Sigma_i\mapsto e_i:C_i}
\]

There is a complication with records (as well as {\bf unfold}s, below), which is that a record
should never be constructed in the presence of a universally-typed variable. Doing so could allow
clever use of a recursive (fold) function in a record to spawn exponential-time behaviour.

The following is an example of a record which could cause exponential-time behaviour if it were
allowed to be built and destructed:

\[\begin{codearray}
\z{g=n\mid ~.{\bf fold}~f(x, b)~{\bf as}~\{}\\
\zz{{\sf Zero}.~(}\\
\zzz{{\sf Eval}:z.b~);}\\
\zz{{\sf Succ}(x_{1}\mid x_{2}).~(}\\
\zzz{{\sf Eval}:z.{\sf Eval}[f(x_{2}, b)](z)~)~\}}\\
\z{{\bf in}~f(n, {\sf True});}
\end{codearray}\]

We use the {\it withOnlySafeVariables} function to ensure it cannot be built.

\begin{code}
typeExpression exprs expr@(Record pes) a = withOnlySafeVariables $ do
		gEnv <- getGEnv
		mapM_ (doPE $ \ d -> lookupSymbol d gEnv) pes where
	doPE lookup (DestructorPattern d vs, e) = do
		(ArrowType ([], alpha':beta') gamma') <- case lookup d of
			Just (ctp, Destructor) -> generalizeType ctp
			_ -> fail $ "No such destructor " ++ d ++ " in expression " ++
				showExprs (expr:exprs)
		b <- takeM (length vs) freshTypeVariable
		c <- freshTypeVariable
		let exprs' = expr:exprs
		let xpr = e:exprs'
		let eqns = (a, alpha', xpr):(c, gamma', xpr):(zip3 b beta' $ repeat xpr)
		addExistentials (c:b) eqns
		let yenv = ContextNode Comma $ zipWith buildYEnv vs b
		pEnv <- getPEnv
		withPEnv yenv $ typeExpression exprs' e c
		setPEnv pEnv
	buildYEnv v tp = SingleSymbol v (t tp) False
\end{code}

As for unfolds, the return type of the function, $g$, introduced in an {\bf unfold}
must be universally quantified. Where $\hat{x}=x$ if $x\neq\alpha'_i$ and $\hat{x}=D$ if
$x=\alpha'_i$. Just like with records, we must disallow dangerous variables when using an {\tt unfold}.

An example of such code which must be disallowed is the infinite list of exponential numbers:

\[\begin{codearray}
\z{{\it exp}=x\mid ~.{\bf fold}~f(x)~{\bf as}~\{}\\
\zz{{\sf Zero}.{\bf unfold}~g(y)~{\bf as}~(}\\
\zzz{{\sf Head}.y;}\\
\zzz{{\sf Tail}.g({\sf Succ}(y))~)}\\
\zz{{\bf in}~g({\sf Zero});}\\
\zz{{\sf Succ}(x_{1}\mid x_{2}).{\bf unfold}~g(y)~{\bf as}~(}\\
\zzz{{\sf Head}.{\sf Head}[{\sf Tail}[y]];}\\
\zzz{{\sf Tail}.g({\sf Tail}[{\sf Tail}[y]])~)}\\
\zz{{\bf in}~g(f(x_{2}))~\}}\\
\z{{\bf in}~f(x);}
\end{codearray}\]

We use the {\tt withAllVariablesDangerous} function to force every variable in the player context to
not be of universal type.

\begin{code}
typeExpression exprs expr@(Unfold (g, x) pes coda) a = withOnlySafeVariables $ do
		b <- takeM (length x) freshTypeVariable
		c <- freshTypeVariable
		addExistentials (c:b) [(c, t a, exprs')]
		let gOutsideType = ArrowType ([], map t b) (t c)
		withREnv g gOutsideType $ typeExpression exprs' coda a
		d <- freshTypeVariable
		addUniversal d []
		let gType = ArrowType ([], map t b) (t d)
		let benv = ContextNode Comma $ zipWith buildBEnv x b
		gEnv <- getGEnv
		withREnv g gType $ mapM_ (withPEnv benv . doPE d gEnv) pes where
	exprs' = expr:exprs
	buildBEnv v tp = SingleSymbol v (t tp) False
	doPE d gEnv (DestructorPattern dst y, e) = do
		ArrowType ([], alpha':beta') gamma' <- case lookupSymbol dst gEnv of
			Just (dtp, Destructor) -> generalizeType dtp
			_ -> fail $ "No such destructor " ++ dst
		let hatGamma = sub (alpha', t d) gamma'
		c <- takeM (length y) freshTypeVariable
		etp <- freshTypeVariable
		let xpr = e:exprs'
		addExistentials (etp:c) $
			(a, alpha', xpr):(etp, hatGamma, xpr):(zip3 c beta' $ repeat xpr)
		pEnv <- getPEnv
		addPEnv $ ContextNode Comma $ zipWith (\ v tp -> SingleSymbol v (t tp) False) y c
		typeExpression exprs' e etp
		setPEnv pEnv
	sub t@(t1, t2) u
		| t1 == u = t2
		| otherwise = sub2 t u
	sub2 t (NamedType s us) = NamedType s $ map (sub t) us
	sub2 t (TupleType us) = TupleType $ map (sub t) us
	sub2 _ u@(PolyType _) = u
\end{code}

Where expressions.

\begin{code}
typeExpression exprs expr@(Where e vs) a = addWhereEnvs vs $ typeExpression exprs' e a where
	exprs' = expr:exprs
	addWhereEnvs [] m = m
	addWhereEnvs (Left (v, tm):vars) m = do
		b <- freshTypeVariable
		addExistential b []
		withoutPEnv $ typeExpression exprs' tm b
		withOEnv (FlatContext [(v, (t b, Simple))]) $ addWhereEnvs vars m
	addWhereEnvs (Right (v, tm):vars) m = do
		b <- freshTypeVariable
		addExistential b []
		(unchangedPEnv, changedPEnv) <- savingPEnv exprs' tm b
		forceSafe exprs' changedPEnv
		oldPEnv <- getPEnv
		setPEnv $ unchangedPEnv $ semicolon changedPEnv $ SingleSymbol v (t b) False
		m' <- addWhereEnvs vars m
		setPEnv oldPEnv
		return m'
\end{code}

Error expressions make no restrictions on typing.

\begin{code}
typeExpression _ (ErrorExpr _) _ = return ()
\end{code}

\subsection{Utility functions}	\label{generalizeType}

The {\it doPE} function attempts to avoid the redundance present among {\sf peek}s, {\sf case}s and
{\sf fold}s. The {\it makeRecEnv} and $e$ variables are not used with {\sf peek}s or {\sf cases}:
they're only used to generate ``recursive'' (of the universal type, $e$) variables that need to be
bound in {\sf fold}s. The {\it makeEnv} is function takes in bound variables names and types and
creates a context: opponent context for {\sf case}s and player context for {\sf peek}s. The
{\it withNewEnvironment} variable is a monad which takes in the newly bound variables and modifies
the environment in some way.

\begin{code}
doPE exprs' withNewEnvironment makeEnv makeRecEnv e b a (ConstructorPattern c vs, tm) = do
		gEnv <- getGEnv
		(ctp@(ArrowType ([], beta) alpha), Constructor) <- liftMaybe
			("No such constructor '" ++ c ++ "'") $ lookupSymbol c gEnv
		ArrowType ([], beta') alpha' <- generalizeType ctp
		vars <- takeM (length vs) freshTypeVariable
		let vsEnv = zipWith makeEnv vs vars
		let recEnv = zipWith3 (makeRecEnv (alpha, e)) vs vars beta
		addExistentials vars $ (b, alpha', xpr):(zip3 vars beta' $ repeat xpr)
		origPEnv <- getPEnv
		modifiedPEnv <- withNewEnvironment vsEnv recEnv $ typeExpression exprs' tm a
		setPEnv origPEnv
		return modifiedPEnv where
	xpr = tm:exprs'
\end{code}

The {\it savingPEnv} monadic function is used for {\sf where} clauses and in the subjects of
{\sf peek}s. It hangs on to the consumed player environment so that a difference can be determined.

\begin{code}
savingPEnv exprs' tm b = do
		pEnv1 <- getPEnv
		typeExpression exprs' tm b
		pEnv2 <- getPEnv
		let (unchanged, changed) = bunchedContextDifference pEnv1 pEnv2
		return (unchanged, takeOutDangerous changed)
	where
	takeOutDangerous (SingleSymbol s t@(DangerousType _) _) = SingleSymbol s t True
	takeOutDangerous (ContextNode t ss) = ContextNode t $ map takeOutDangerous ss
	takeOutDangerous t = t
\end{code}

We have a function to print out nice error messages when something goes wrong.

\begin{code}
showExprs [] = ""
showExprs [e] = show e
showExprs (e:es) = show e ++ "\n\t\tin expression\t" ++ showExprs es
\end{code}

The {\it generalizeType} function takes a type involving named type parameters and creates
fresh type variables for them.

\begin{code}
generalizeType :: TempType -> Unification TempType
generalizeType tp = do
		(_, t') <- gT tp []
		return t'
	where
	gT :: TempType -> [(String, TempType)] -> Unification ([(String, TempType)], TempType)
	gT (PolyType (Left a)) l = case lookup a l of
		Just tp -> return $ (l, tp)
		Nothing -> do
			var <- freshTypeVariable
			addExistential var []
			return ((a, t var):l, t var)
	gT (TupleType ts) l = do
		(l', ts') <- gTmany ts l
		return (l', TupleType ts')
	gT (ArrowType (os, ps) r) l = do
		(l1, os') <- gTmany os l
		(l2, ps') <- gTmany ps l1
		(l', r') <- gT r l2
		return (l', ArrowType (os', ps') r')
	gT (NamedType s ts) l = do
		(l', ts') <- gTmany ts l
		return (l', NamedType s ts')
	gT (SafeType t) l = do
		(l', t') <- gT t l
		return (l', SafeType t')
	gT (DangerousType t) l = do
		(l', t') <- gT t l
		return (l', DangerousType t')
	gT tp l = return (l, tp)
	gTmany [] l = return (l, [])
	gTmany (t:ts) l = do
		(l1, t') <- gT t l
		(l', ts') <- gTmany ts l1
		return (l', t':ts')
\end{code}

The {\it forceSafe} function takes a context and forces all symbols in there to be safe. This is
used in peeks where we reintroduced symbols consumed in the subject of a peek to be safe. If
they're used in the body of a peek then they must be safe.

\begin{code}
forceSafe exprs (SingleSymbol s (PolyType (Right x)) d) = do
	x' <- freshTypeVariable
	addExistentials [x'] [(x, SafeType $ PolyType $ Right x', exprs)]
forceSafe exprs (ContextNode _ ss) = mapM_ (forceSafe exprs) ss
forceSafe _ _ = return ()
\end{code}

