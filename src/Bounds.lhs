\chapter{Calculating bounds}	\label{bounds}

The interpreting follows the operational semantics of the language.

\section{Preamble}

\subsection{Exported symbols}

The only symbol exported by this module is \textit{inferBounds}, which takes a function as input
and bounds that function. Note that that function is not allowed to call other functions, since
that would take too long to implement at this time.

Note that the interpreter module does \textbf{not} export any data types since the Polynomial
type is already defined.

\begin{code}
module Bounds (inferBounds)
where

import Parser
import TypeInference (genType, Context(..), FlatContext(..), SymbolType(..))
import Utility
import Data.List
import Data.Maybe (fromJust)
\end{code}

\section{Polynomial arithmetic}

\subsection{Monomials and polynomials}

First we show how term are equal. We say that two terms are equal if only their coefficients
differ.

\begin{code}
instance Eq Term where
	(PolyTerm _ x) == (PolyTerm _ y) = x == y
\end{code}

And we allow the adding and multiplying of terms.

\begin{code}
instance Num Term where
	(PolyTerm n x) + (PolyTerm m y)	| x == y = PolyTerm (n + m) x
	(PolyTerm n x) * (PolyTerm m y) = PolyTerm (n * m) $ aggregate x y where
		aggregate [] y = y
		aggregate x [] = x
		aggregate (x@(var, pow):xs) ys = x':aggregate xs ys' where
			(x', ys') = case removeBy (\ (var', _) -> var == var') ys of
				Just ((_, pow'), ys') -> ((var, pow + pow'), ys')
				Nothing -> (x, ys)
	abs (PolyTerm n x) = PolyTerm (abs n) x
	negate (PolyTerm n x) = PolyTerm (negate n) x
	signum (PolyTerm n _) = fromInteger $ signum n
	fromInteger n = PolyTerm n []
\end{code}

We must first define equality for polynomials.

\begin{code}
instance Eq Polynomial where
	(Poly _ ts) == (Poly _ us) = ts == us
\end{code}

We allow the polynomial type to be added and multiplied easily.

\begin{code}
instance Num Polynomial where
	(Poly _ []) + q = q
	p + (Poly _ []) = p
	(Poly _ (t:ts)) + (Poly _ us) = Poly [] ts + case removeBy (== t) us of
		Just (u, us') -> Poly [] [t + u] + Poly [] us'
		Nothing -> Poly [] (t:us)
	p - q = p + negate q
	(Poly _ []) * _ = 0
	(Poly _ [t]) * (Poly _ ts) = Poly [] $ map (* t) ts
	(Poly _ (t:ts)) * q = (Poly [] [t]) * q + (Poly [] ts) * q
	abs x = x
	negate (Poly vs ts) = Poly vs $ map negate ts
	signum _ = fromInteger 1
	fromInteger x = Poly [] [fromInteger x]
\end{code}

The flip-side to {\it fromInteger} is adding a polynomial based on a variable name.

\begin{code}
p x = Poly [x] [PolyTerm 1 [(x, 1)]]
\end{code}

The Haskell function {\it max} is defined in terms of a total partial order, which doesn't work
well for polynomials. When we say $\max (p,q)$ over two polynomials $p$ and $q$, the result may
be a polynomial which is neither $p$ nor $q$. We define the {\it maxP} function to handle this.

\begin{code}
instance Maxable Polynomial where
	maxP (Poly _ ts) (Poly _ us) = Poly [] $ m ts us where
		m [] us = us
		m ts [] = ts
		m (t@(PolyTerm c cs):ts) us = case removeBy (\ (PolyTerm d ds) -> ds == cs) us of
			Nothing -> t:m ts us
			Just (PolyTerm d _, us') -> PolyTerm (max c d) cs:m ts us'
\end{code}

\subsection{Lists of polynomials}

We also have to deal with lists of polynomials, which add and multiply in parallel.

\begin{code}
instance Eq ConstructorPolyMap where
	(Polies ps) == (Polies qs) = ps == qs
\end{code}

\begin{code}
instance Num ConstructorPolyMap where
	(Polies ps) + (Polies qs) = Polies $ zipWithP ((+), (+)) ps qs
	(Polies ps) - (Polies qs) = Polies $ zipWithP ((-), (-)) ps qs
	(Polies ps) * (Polies qs) = Polies $ zipWithP ((*), (*)) ps qs
	abs (Polies ps) = Polies $ mapP (\ _ -> abs, \ _ _ -> abs) ps
	negate (Polies ps) = Polies $ mapP (\ _ -> negate, \ _ _ -> negate) ps
	signum _ = fromInteger 1
	fromInteger x = Polies [("", fromInteger x, [])]
\end{code}

And, similarly to with polynomials, we define the {\it maxP} function to determine the maximum
of two lists of polynomials.

\begin{code}
instance Maxable ConstructorPolyMap where
	maxP (Polies []) qs = qs
	maxP (Polies ((b, bp, bv):bs)) (Polies cs) = Polies ((b, bp', bv'):bs') where
		Polies bs' = maxP (Polies bs) (Polies cs')
		(bp', bv', cs') = case removeBy (\ (c, _, _) -> c == b) cs of
			Just ((_, cp, cv), cs') -> (maxP bp cp, maxP bv cv, cs')
			Nothing -> (bp, bv, cs)
\end{code}

\subsection{Value sizes}

We need to be able to add value sizes.

\begin{code}
instance Eq ValueSize where
	(InductiveSize cs) == (InductiveSize ds) = cs == ds
	(CoinductiveSize ds _ e) == (CoinductiveSize fs _ g) = ds == fs && e == g
	(TupleSize ts) == (TupleSize us) = ts == us
	(SimpleSize x) == (SimpleSize y) = x == y
	_ == _ = False
\end{code}

\begin{code}
instance Num ValueSize where
	(InductiveSize cs) + (InductiveSize ds) = InductiveSize (cs + ds)
	(InductiveSize cs) + (SimpleSize x) = InductiveSize (cs `addPoly` x)
	(CoinductiveSize s1 tm1 z1) + (CoinductiveSize s2 tm2 z2)
			| s1 == s2 = CoinductiveSize s1 tm z where
		tm = zipWith addDF tm1 tm2
		addDF (d1, t1) (d2, t2)	| d1 == d2 = (d1, t1 + t2)
		z = zipWith addEG z1 z2
		addEG (d1, xs1, z1) (d2, xs2, z2)
			| d1 == d2 && xs1 == xs2 = (d1, xs1, z1 + z2)
	(SimpleSize x) + (SimpleSize y) = SimpleSize (x + y)
	x@(SimpleSize _) + v = v + x
	(TupleSize ts) + (TupleSize us) = TupleSize $ zipWith (+) ts us
	(TupleSize ts) + x@(SimpleSize _) = TupleSize $ map (+ x) ts
	a + (SimpleSize (Poly _ [PolyTerm 0 _])) = a

	(InductiveSize cs) * (SimpleSize x) = InductiveSize (cs `mulPoly` x)
	(CoinductiveSize s t z) * x'@(SimpleSize x) = CoinductiveSize s t' z' where
		t' = map (\ (s, t) -> (s, t * x)) t
		z' = map (\ (s, xs, z) -> (s, xs, z * x')) z
	(TupleSize ts) * x@(SimpleSize _) = TupleSize $ map (* x) ts
	(SimpleSize x) * (SimpleSize y) = SimpleSize $ x * y
	x@(SimpleSize _) * v = v * x
	abs x = x
	negate (InductiveSize cs) = InductiveSize (negate cs)
	negate (SimpleSize x) = SimpleSize (negate x)
	negate (TupleSize ts) = TupleSize $ map negate ts
	signum _ = fromInteger 1
	fromInteger = SimpleSize . fromInteger
\end{code}

There's a FIXME on {\tt maxP a b}.

\begin{code}
instance Maxable ValueSize where
	maxP (InductiveSize a) (InductiveSize b) = InductiveSize $ maxP a b
	maxP (InductiveSize a) (SimpleSize b) = InductiveSize $ maxPoly a b
	maxP (CoinductiveSize s1 tm1 z1) (CoinductiveSize s2 tm2 z2)
			| s1 == s2 = CoinductiveSize s1 tm z where
		tm = zipWith m1 tm1 tm2
		m1 (d1, t1) (d2, t2)	| d1 == d2 = (d1, maxP t1 t2)
		z = zipWith m2 z1 z2
		m2 (d1, xs1, z1) (d2, xs2, z2)
			| d1 == d2 && xs1 == xs2 = (d1, xs1, maxP z1 z2)
	maxP (TupleSize ts) (TupleSize us)
		| length ts == length us = TupleSize $ zipWith maxP ts us
	maxP (SimpleSize a) (SimpleSize b) = SimpleSize $ maxP a b
	maxP a@(SimpleSize _) b = maxP b a
	maxP a b = a + b
\end{code}

\subsection{Utility functions}

\begin{code}
zipWithP :: (Polynomial -> Polynomial -> Polynomial, ValueSize -> ValueSize -> ValueSize) ->
	[(String, Polynomial, [ValueSize])] ->
	[(String, Polynomial, [ValueSize])] -> [(String, Polynomial, [ValueSize])]
zipWithP f@(f1, f2) ((b, bp, bv):bs) cs = case removeBy (\ (c, _, _) -> c == b) cs of
	Just ((_, cp, cv), cs') -> (b, f1 bp cp, zipWith f2 bv cv):zipWithP f bs cs'
	Nothing -> (b, f1 bp 0, zipWith f2 bv $ repeat 0):cs
zipWithP _ [] [] = []
mapP f@(f1, f2) ((b, bp, bv):bs) = (b, f1 b bp, mapI (f2 b) bv):mapP f bs
mapP _ [] = []
mapI = m 0 where
	m _ _ [] = []
	m i f (x:xs) = f i x:m (i + 1) f xs
\end{code}

\begin{code}
addPoly :: ConstructorPolyMap -> Polynomial -> ConstructorPolyMap
addPoly (Polies ps) p = Polies $ mapP (\ c s -> s + divideSymbolName p c,
	\ c i -> (+ SimpleSize (divideSymbolNameWithI c i p))) ps
mulPoly (Polies ps) p = Polies $ mapP (\ _ s -> s * p, \ _ _ -> (* SimpleSize p)) ps
maxPoly (Polies ps) p = Polies $ mapP (\ c s -> s `maxP` divideSymbolName p c, \ _ _ -> id) ps
divideSymbolName :: Polynomial -> String -> Polynomial
divideSymbolName (Poly ss ts) c = Poly ss $ map f ts where
	f (PolyTerm i fs) = PolyTerm i $ map g fs
	g (s, p) = (s ++ "." ++ c, p)
divideSymbolNameWithI :: (Show a) => String -> a -> Polynomial -> Polynomial
divideSymbolNameWithI c i p = divideSymbolName p (c ++ "." ++ show i)
\end{code}

\subsection{Maxable}

As described above, often we want to find the {\it max} of two entities, such as polynomials,
where the {\it max} is neither of its operands.

\begin{code}
class Maxable a where
	maxP :: a -> a -> a
instance (Maxable a) => Maxable [a] where
	maxP = zipWith maxP
\end{code}

We also need a function for finding the maximum of two polynomials (which may be
greater than each of its operands!).

\begin{code}
maxPs :: (Maxable a, Num a) => [a] -> a
maxPs [] = 0
maxPs [p] = p
maxPs (x:xs) = maxP x $ maxPs xs
\end{code}

\section{Inferring bounds}

The major function is the \textit{inferBounds}.

\begin{code}
inferBounds :: FlatContext -> [FunctionDecl] ->
	((FunctionDecl, Expression -> Maybe (Type (Either String Integer))), FunctionDefinition) ->
	FunctionDecl
inferBounds ctxt@(FlatContext conses) decls (((_, TypeConstraint t:_), ed), (f, cs, os, ps, e)) = (f,
		cs ++ [TimeConstraint $ Poly basis timebound, SizeConstraint sc]) where
	ArrowType (oTypes, pTypes) retType = t
	Poly _ timebound = inferTime (ctxt, ed) env e
	basis = os ++ ps
	fixBasis basis (InductiveSize (Polies cs)) = InductiveSize (Polies $ map fixPoly cs)
		where fixPoly (s, Poly _ p, ss) = (s, Poly basis p, map (fixBasis basis) ss)
	fixBasis basis (CoinductiveSize s t z) = CoinductiveSize s t' z' where
		t' = map (\ (d, Poly _ t) -> (d, Poly basis t)) t
		z' = map (\ (d, ys, z) -> (d, ys, fixBasis (basis ++ ys) z)) z
	fixBasis basis (TupleSize ss) = TupleSize $ map (fixBasis basis) ss
	fixBasis basis (SimpleSize (Poly _ p)) = SimpleSize $ Poly basis p
	sc = fixBasis basis $ inferSize (ctxt, ed) env e
	env = concat (zipWith mkV oTypes os ++ zipWith mkV pTypes ps) ++ map fixDecl decls where
		mkV :: Type String -> String -> [(String, Polynomial, ValueSize)]
		mkV tp x = simple:concatMap makeC indConses where
			simple = makeP x
			makeC (s, (ArrowType (so, sp) _, _)) = makeP (x ++ "." ++ s):
				(map makeP $ take (length $ filter (/= genType retType) $ so ++ sp) $
				repeatI (\ i -> x ++ "." ++ s ++ "." ++ show i))
			makeP x = (x, 1, SimpleSize $ p x)
			indConses = filter isRightConstructor conses
			isRightConstructor (_, (ArrowType _ r, Constructor)) = r == genType retType
			isRightConstructor _ = False
		fixDecl (f, [TimeConstraint p, SizeConstraint s]) = (f, p, s)
\end{code}

\subsection{Function calls}

\begin{code}
funCallAsTimeCost :: Polynomial -> [ValueSize] -> Polynomial
funCallAsTimeCost = evalPolynomial
\end{code}

\begin{code}
funCallAsSizeCost :: ValueSize -> [ValueSize] -> ValueSize
funCallAsSizeCost z es = size z where
	size (TupleSize ts) = TupleSize $ map size ts
	size (InductiveSize (Polies ps)) = InductiveSize $ Polies $ map m ps where
		m (c, t, vs) = (c, evalPolynomial t es, map size vs)
	size (SimpleSize p) = evalValueSize p es
	size (CoinductiveSize s t z) = CoinductiveSize s t' z' where
		t' = map (\ (d, t) -> (d, evalPolynomial t es)) t
		z' = map (\ (d, xs, z) -> (d, xs, funCallAsSizeCost z es)) z
\end{code}

\begin{code}
evalPolynomial :: Polynomial -> [ValueSize] -> Polynomial
evalPolynomial (Poly _ []) _ = 0
evalPolynomial (Poly vs (term:terms)) ps = evalPolynomial (Poly vs terms) ps + evalTerm term where
	evalTerm (PolyTerm coeff factors) = fromInteger coeff * (product $ map evalFactor factors)
	evalFactor (var, power) = getTime var (zip vs ps) ^ power
	getTime var vsps = case break (== '.') var of
		(_, "") -> case lookup var vsps of
			Just (SimpleSize t) -> t
			Nothing -> p var
		(stem, '.':branch) -> chaseBranch stem branch vsps
	chaseBranch s b vsps = case lookup s vsps of
			Just (InductiveSize (Polies cs)) -> getTime b $ concatMap mC cs
			Just (SimpleSize (Poly vs ts)) -> Poly vs $ map doTerm ts
			Just (TupleSize zs) -> getTime b $ mT zs 0
			Nothing -> p (s ++ "." ++ b)
			x -> error $ "Cannot evaluation size bound " ++ show x
		where
		mC (c, s, ss) = (c, SimpleSize s):zipWith (\ s i -> (c ++ "." ++ show i, s)) ss [0..]
		mT [] _ = []
		mT (z:zs) n = (show n, z):mT zs (n + 1)
		doTerm (PolyTerm coeff facts) = PolyTerm coeff $ map doFact facts
		doFact (var, power) = (var ++ "." ++ b, power)
\end{code}

\begin{code}
evalValueSize :: Polynomial -> [ValueSize] -> ValueSize
evalValueSize (Poly _ []) _ = 0
evalValueSize (Poly vs (term:terms)) ps = evalValueSize (Poly vs terms) ps + evalTerm term where
	evalTerm (PolyTerm coeff factors) = fromInteger coeff * (product $ map evalFactor factors)
	evalFactor (var, power) = getSize var (zip vs ps) ^ power
	getSize var vsps = case break (== '.') var of
		(_, "") -> case lookup var vsps of
			Just s -> s
			Nothing -> SimpleSize $ p var
		(stem, '.':branch) -> chaseBranch stem branch vsps
	chaseBranch s b vsps = case lookup s vsps of
			Just (InductiveSize (Polies cs)) -> getSize b $ concatMap mC cs
			Just (SimpleSize (Poly vs ts)) -> SimpleSize $ Poly vs $ map doTerm ts
			_ -> SimpleSize $ p (s ++ "." ++ b)
		where
		mC (c, s, ss) = (c, SimpleSize s):zipWith (\ s i -> (c ++ "." ++ show i, s)) ss [0..]
		doTerm (PolyTerm coeff facts) = PolyTerm coeff $ map doFact facts
		doFact (var, power) = (var ++ "." ++ b, power)
\end{code}

\subsection{Inferring bounds from expressions}

\begin{code}
inferTime _ _ (VariableName _) = 1
inferTime c env (TupleExpr ts) = sum (map (inferTime c env) ts) + 1
inferTime c env (FunctionCall f es) = sum (map (inferTime c env) es) +
		(funCallAsTimeCost f' $ map (inferSize c env) es) where
	f' = fst $ fromJust $ lookup3 f env
inferTime c env (Construction _ cs) = sum (map (inferTime c env) cs) + 1
inferTime c env (Case t pes) = inferTime c env t +
	maxPs (doPEs (inferSize c env t) inferTime c env pes) + 1
inferTime c env (Peek t pes) = inferTime c env (Case t pes)
inferTime ctxt@(conses, _) env (Fold (f, x, ys) pes coda) = inferTime ctxt envOutside coda where
	envOutside = (f, Poly (x:ys) fTime, 0):env
	Poly _ fTime = sum (map branchTime pes) + 1
	envInside = (f, Poly (x:ys) [0], 0):map (\ v -> (v, 1, 0)) ys ++ env
	branchTime (ConstructorPattern c ps, e) = p cName * inferTime ctxt env' e where
		cName = x ++ "." ++ c
		env' = concat (zipWith nr psNotRec [0..]) ++ concatMap r psRec ++ envInside
		(psNotRec1, psRec1) = dualFilter (\ (t, _) -> t == cType) $ zip psTypes ps
		(psNotRec, psRec) = (snd $ unzip psNotRec1, snd $ unzip psRec1)
		Just (ArrowType ([], psTypes) cType, _) = lookupSymbol c conses
		nr pat i = addVars (SimpleSize $ p $ cName ++ "." ++ show i) pat
		r = addVars $ SimpleSize $ p x - 1
		addVars _ (VariablePattern "_" "_") = []
		addVars sz (VariablePattern x "_") = [(x, 1, sz)]
		addVars sz (VariablePattern "_" x) = [(x, 1, sz)]
		addVars sz (VariablePattern x y) = [(x, 1, sz), (y, 1, sz)]
inferTime c env (Record pes) = 1
inferTime c env (Destruction d xs@(x:_)) = xsTime + case inferSize c env x of
		CoinductiveSize _ tm _ -> fromJust $ lookup d tm
		SimpleSize (Poly ss ts) -> Poly ss ts' where
			ts' = map (\ (PolyTerm c fs) -> PolyTerm c $ map
				(\ (v, e) -> (v ++ "[" ++ d ++ "]", e)) fs) ts
		_ -> 0	-- FIXME
	where xsTime = sum $ map (inferTime c env) xs
inferTime c env (Unfold (g, xs) _ coda) = inferTime c env' coda where
	env' = (g, Poly xs [1], 0):env
inferTime c env (Where e vsps) = inferTime c env' e + vsps' where
	(env', vsps') = foldl addVar (env, 0) vsps
	addVar (env, cost) (Left (v, e)) = ((v, 1, inferSize c env e):env,
		inferTime c env e + cost)
	addVar env (Right (v, e)) = addVar env (Left (v, e))
inferTime _ _ (ErrorExpr _) = 0
inferTime _ _ _ = 0
\end{code}

FIXME: describe the inferSize function.

\begin{code}
inferSize _ env (VariableName x) = snd $ fromJust $ lookup3 x env
inferSize c env (Where e vs) = inferSize c (map doVar vs ++ env) e where
	doVar (Left (v, e)) = (v, Poly [v] [1], inferSize c env e)
	doVar (Right (v, e)) = (v, Poly [v] [1], inferSize c env e)
inferSize c env (TupleExpr es) = TupleSize $ map (inferSize c env) es
inferSize ctxt@(FlatContext conses, _) env (Construction c es) = sum $ c':subC where
	c' = InductiveSize $ Polies cs where
		cs = map makeC $ filter matchesC conses
		makeC (d, (ArrowType ([], tps) _, _)) = (d, count, ss) where
			count	| d == c = 1
				| otherwise = 0
			ss	| d == c = map (inferSize ctxt env) esNotRec
				| otherwise = map (\ _ -> 0) tpsNotRec
			tpsNotRec = fst $ break (== cType) tps
		matchesC (_, (ArrowType _ t, Constructor)) = t == cType
		matchesC _ = False
	subC = map (inferSize ctxt env) esRec
	(esNotRec1, esRec1) = break (\ (t, _) -> t == cType) $ zip esTypes es
	(esNotRec, esRec) = (snd $ unzip esNotRec1, snd $ unzip esRec1)
	Just (ArrowType ([], esTypes) cType, _) = lookupSymbol c $ FlatContext conses
inferSize c env (Peek e pes) = inferSize c env $ Case e pes
inferSize c env (Case e pes) = maxPs $ doPEs (inferSize c env e) inferSize c env pes
inferSize c env (FunctionCall f es) = funCallAsSizeCost f' $ map (inferSize c env) es where
	f' = snd $ fromJust $ lookup3 f env
inferSize c env (Record pes) = CoinductiveSize [] tm sz where
	tm = map (\ (DestructorPattern d xs, e) -> (d, inferTime c (env ++ makeVars xs) e)) pes
	sz = map (\ (DestructorPattern d xs, e) -> (d, xs,
		inferSize c (env ++ makeVars xs) e)) pes
\end{code}

Folds are complicated.

\begin{code}
inferSize ctxt@(FlatContext conses, eD) env exp@(Fold (f, x, ys) pes coda) =
		inferSize ctxt envOutside coda where
	envOutside = (f, 0, normalize $ branchesInside + branchesOutside):env
	envFzero = (f, 0, 0)
	envFmax = (f, 0, normalize $ maxPs gamma')
	envYzero = map (\ y -> (y, 1, 0)) ys
	envYy = map (\ y -> (y, 1, SimpleSize $ p y)) ys
	gamma = map (inferBranch $ envFzero:envYy ++ env) pes
	gamma' = map (keepOnly ys) gamma where
		keepOnly ys (SimpleSize p) = SimpleSize $ keepOnlyP ys p
		keepOnly ys (TupleSize ts) = TupleSize $ map (keepOnly ys) ts
		keepOnly ys (InductiveSize (Polies cs)) = InductiveSize $ Polies cs' where
			cs' = map (\ (c, p, z) -> (c, keepOnlyP ys p, map (keepOnly ys) z)) cs
		keepOnly ys (CoinductiveSize vs ss zs) = CoinductiveSize vs ss' zs' where
			ss' = map (\ (s, t) -> (s, 0)) ss
			zs' = map (\ (s, vs, z) -> (s, vs, keepOnly ys z)) zs
		keepOnlyP ys (Poly vs ts) = case filter isY $ map zeroOut ts of
				[] -> 0
				ts' -> Poly vs ts'
			where
			isY (PolyTerm _ []) = False
			isY (PolyTerm 1 _) = True
			zeroOut (PolyTerm _ fs) = PolyTerm 1 $ filter z2 fs
			z2 (y, 1) = elem (takeWhile (/= '.') y) ys
			z2 _ = False
	eta = map (inferBranch $ envFmax:envYzero ++ env) pes
	branchesOutside = funCallAsSizeCost (normalize $ maxPs gamma') (0:map (SimpleSize . p) ys)
	branchesInside = sum $ zipWith consSizeBound rawConsNames eta
	normalize (SimpleSize (Poly _ ts)) = SimpleSize $ Poly (x:ys) ts
	normalize (TupleSize ts) = TupleSize $ map normalize ts
	normalize (InductiveSize (Polies cs)) = InductiveSize $ Polies $
		map (\ (c, Poly _ ts, ss) -> (c, Poly (x:ys) ts, map normalize ss)) cs
	normalize (CoinductiveSize vs ss zs) = CoinductiveSize vs ss' zs' where
		ss' = map (\ (s, Poly _ ts) -> (s, Poly (x:ys) ts)) ss
		zs' = map (\ (s, vs, z) -> (s, vs, normalize z)) zs
	rawConsNames = map (\ (ConstructorPattern c _, _) -> c) pes
	consSizeBound :: String -> ValueSize -> ValueSize
	consSizeBound c z = SimpleSize (p x_c) * z where
		x_c = x ++ "." ++ c
	inferBranch env (ConstructorPattern c vs, e) = inferSize ctxt (vsEnv ++ env) e where
		x_c = x ++ "." ++ c
		vsEnv = concat $ zipWith addVEnv (map (\ i -> x_c ++ "." ++ show i) [0..]) vs
		addVEnv sName (VariablePattern v1 v2) = m v1 ++ m v2 where
			m "_" = []
			m x = [(x, Poly [x] [1], SimpleSize $ p sName)]
\end{code}

FIXME! We should definitely not be defining szPlus1 to just be sz. It should be replacing all
instance of d with d + 1 in sz.

FIXME! The cases that return 0 here should be re-examined.

\begin{code}
inferSize c env (Destruction d (x:xs)) = case inferSize c env x of
	CoinductiveSize s tm sz -> case (elem d s, lookup3 d sz) of
			(True, Nothing) -> CoinductiveSize s tmPlus1 szPlus1
			(False, Just (_, z)) -> funCallAsSizeCost z $ map (inferSize c env) xs
		where
		dPlus1 = SimpleSize $ p d + 1
		tmPlus1 = map (\ (d, Poly vars p) -> (d, funCallAsTimeCost (Poly (d:vars) p)
			[dPlus1])) tm
		szPlus1 = sz
	SimpleSize (Poly _ [PolyTerm 1 [(x, 1)]]) -> SimpleSize $ p x' where
		x' = x ++ "[" ++ d ++ "]" ++ args
		args = case xs of
			[] -> ""
			_ -> "(" ++ commaDelimit (map (show . inferSize c env) xs) ++ ")"
	_ -> 0
inferSize _ _ (ErrorExpr _) = 0
inferSize _ _ _ = 0
\end{code}

The {\it doPEs} function does inference, both time and size, over pattern-expressions, such as those
found in {\sf case} and {\sf peek} constructs. This doesn't deal with {\sf fold}s as those require
special care. The $s$ variable carries the size of the subject and {\it infer} is a function which
does the actual inference (typically bound to {\it inferTime} or {\it inferSize}).

The {\it addToEnv} function adds newly-bound variables to the environment for inference

\begin{code}
doPEs s infer ctxt@(conses, _) env pes = map (\ (p, e) -> infer ctxt (addToEnv s env p) e) pes where
	addToEnv s env (VariablePattern "_" "_") = env
	addToEnv s env (VariablePattern "_" x) = (x, Poly [x] [1], s):env
	addToEnv s env (VariablePattern x "_") = (x, Poly [x] [1], s):env
	addToEnv s env (VariablePattern x y) = (x, Poly [x] [1], s):
		(y, Poly [y] [1], s):env
	addToEnv s env (ConstructorPattern c ps) = foldl addNonRec recPsEnv $
			zip (map snd notRecursivePs) nonRecSizes where
		recPsEnv = foldl (addToEnv s') env $ map snd recursivePs
		addNonRec env (p, sz) = addToEnv sz env p
		uniformSize = case s of
			InductiveSize (Polies p) -> p
			SimpleSize p -> subdivide p
		subdivide (Poly [_] [PolyTerm 1 [(x, 1)]]) = [(c, p (x ++ "." ++ c),
				take (length notRecursivePs) $
				repeatI $ \ i -> SimpleSize $ p (x ++ "." ++ c ++ "." ++ show i))]
		subdivide (Poly [] [PolyTerm 0 _]) = [(c, 0, take (length notRecursivePs) $ repeat 0)]
		s'	| s == 0 = 0
			| otherwise = InductiveSize $ Polies shrunkP
		Just (_, nonRecSizes) = lookup3 c shrunkP
		shrunkP = map shrinkPoly uniformSize
		shrinkPoly (d, n, z) = (d, n', z) where n' = if d == c then n - 1 else n
		(notRecursivePs, recursivePs) = dualFilter (\ (t, _) -> t == cType) $ zip psTypes ps
		Just (ArrowType ([], psTypes) cType, _) = lookupSymbol c conses
	addToEnv s env (TuplePattern ps) = foldl a env $ zip ps s' where
		a env (p, s) = addToEnv s env p
		s' = case s of
			TupleSize ss -> ss
			SimpleSize p -> map (SimpleSize . makeSize p) [0..]
		makeSize (Poly vs ts) i = Poly vs $ map mT ts where
			mT (PolyTerm c fs) = PolyTerm c $ map mF fs
			mF (x, p) = (x ++ "." ++ show i, p)
\end{code}

\begin{code}
repeatI x = r 0 x where
	r i x = x i:r (i + 1) x
\end{code}

\begin{code}
dualFilter _ [] = ([], [])
dualFilter f (x:xs)	| f x = (ys, x:zs)
			| otherwise = (x:ys, zs)
	where (ys, zs) = dualFilter f xs
\end{code}

We have a convenient way to add variables to the environment.

\begin{code}
makeVar x = (x, 1, SimpleSize $ p x)
makeVars = map makeVar
\end{code}
