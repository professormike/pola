\chapter{Utility functions}

These are functions which are used by multiple modules and are not core to the program.

\section{Preamble}

\begin{code}
module Utility (lengthI, removeBy, liftMaybe, lookup3)
where
\end{code}

\section{Defined functions}

\subsection{lengthI}

First off, many Haskell functions use the
bounded \textit{Int} type, while we generally prefer to deal with the arbitrary-length
\textit{Integer} type.

The \textit{lengthI} function is exactly like the \textit{length} function except that it returns
an \textit{Integer} instead of an \textit{Int}.

\begin{code}
lengthI :: [a] -> Integer
lengthI = toInteger . length
\end{code}

\subsection{removeBy}

We sometimes like to find an element in a list and remove it if it's found.
\begin{code}
removeBy f [] = Nothing
removeBy f (x:xs)	| f x = Just (x, xs)
			| otherwise = do
				(x', xs') <- removeBy f xs
				return (x', x:xs')
\end{code}

\subsection{liftMaybe}

Technically speaking this isn't a ``lift''; it's a horizontal move from one monad to another. We
shift from one fail to another fail, or one success to another success.

\begin{code}
liftMaybe :: (Monad m) => String -> Maybe a -> m a
liftMaybe s Nothing = fail s
liftMaybe _ (Just x) = return x
\end{code}

\subsection{lookup3}

This is the standard Haskell {\it lookup} function extended to triples.

\begin{code}
lookup3 _ [] = Nothing
lookup3 x ((a, b, c):as)	| x == a = Just (b, c)
				| otherwise = lookup3 x as
\end{code}
