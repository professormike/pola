Description
===========

Pola is a restricted functional programming language such that every well-typed function is
guaranteed to halt in polynomial time. It was the main focus of my PhD thesis.

Building
========

Try `make`

Expect it to build a binary (called `built/pola`) and pass all of the tests.

Running
=======

You need an existing Pola source file (see the test directory for examples). The Pola
interpreter expects exactly 1 command-line argument, which is the name of a source file
to read from. It will then put you in a REPL-like loop. Use :q to quit.

Dependencies
============

1. `texlive-latex-extra` for building type1cm.sty, which is used to build literate documentation
